<?php
/**
 * Airlines add/edit/delete/view class
 */ 
defined('BASEPATH') OR exit('No direct script access allowed');

class Airline extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data 
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');          
    }    
    
    /**
     * Airline list view
     */     
    public function index(){
        // Access Airline model
        $this->load->model('Airlines');
        $this->load->helper(array('form', 'url'));
        // get all airline data to controller from airline model
        $this->data['allData'] = $this->Airlines->viewAll();
        // load list view template with airline data
        $this->load->template('tmp_view_airline', $this->data); 
    }
    
    /**
     * Add airline
     */
    public function add(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller
        if($this->data['user_type'] == 2){
           redirect('/access');
        }    
        // access airline model
        $this->load->model('Airlines');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // validate airline_name as required field, airline name should be unique.
        $this->form_validation->set_rules('airline_name', 'Airline Name', 'required|is_unique[airline.airline_name]');
        // track from validation 
        if ($this->form_validation->run() == FALSE){
            // if form validation fail add airline page return again with error messages
            $this->load->template('tmp_add_airline', $this->data);
        }else{
            // if form validation success
            // and if press submit button
            if ($this->input->post('submit')==true) {
                // capture data
                $data['airline_name'] = $this->input->post('airline_name');
                $data['airline_description'] = $this->input->post('airline_description');
                // if an image selected
                if (!empty($_FILES["thumb_image"]['name'])) {
                    // resize image and upload that image to relevent folder
                    $thumb_image_new_name = $this->imageUploadResize($_FILES["thumb_image"]['name'], 'thumb_image', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_');
                    // set thumbnail image name to send database array
                    $data['thumb_image'] = $thumb_image_new_name;
                } else {
                    // if image not selected
                    $data['thumb_image'] = "";
                }                
                
                // save data on table
                if($this->Airlines->addAirline($data)){
                    $this->data['msg'] = 'Airline Added';
                    $this->load->template('tmp_add_airline', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to add airline';
                    $this->load->template('tmp_add_airline',  $this->data);
                }
            }                
        }         
    }
    
    /**
     * edit function
     */
    public function edit(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }      
        // access airline model
        $this->load->model('Airlines');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // get selected airline data 
        $this->data['airlineData'] = $this->Airlines->viewAirline($_GET['id']);
        // validate airline field
        $this->form_validation->set_rules('airline_name', 'Airline Name', 'required');
        // track form validation
        if ($this->form_validation->run() == FALSE){    
            // if form validation fail edit airline page return again with error messages
            $this->load->template('tmp_edit_airline', $this->data);
        }else{
            // if form validation success
            // and if press submit button            
            if ($this->input->post('submit')==true) {
                // capture updated data
                $data['airline_name'] = $this->input->post('airline_name');
                $data['airline_description'] = $this->input->post('airline_description');
                // user id capture to send airline model
                $data['id'] = $_GET['id'];
                if (!empty($_FILES["thumb_image"]['name'])) {
                    // if a image uploaded resize and upload to relevent folder
                    $thumb_image_new_name = $this->imageUploadResize($_FILES["thumb_image"]['name'], 'thumb_image', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_');
                    // thumb_image name set to send airline model
                    $data['thumb_image'] = $thumb_image_new_name;
                } else {
                    // if image is not uploaded, then set existing image value to send airline model
                    $data['thumb_image'] = $this->data['airlineData'][0]->thumb_image;
                }                
                // update airline table
                $this->Airlines->airlineUpdate($data);
                $this->data['msg'] = 'Airline Updated';
                // get updated data from airline model
                $this->data['airlineData'] = $this->Airlines->viewAirline($_GET['id']);
                $this->load->template('tmp_edit_airline', $this->data);
            }                
        }         
    }

    /**
     * delete function
     */
    public function delete(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }          
        $this->load->model('Airlines');
        // delete selected airline
        $this->Airlines->delete($_GET['id']);
        // redirect to list view
        redirect('/airline');
    }   
    
    /**
     * image upload function
     * @param type $filename - eg: $_FILES['image_name']['name']
     * @param type $field_name - eg: image_name
     * @param type $width
     * @param type $height
     * @param type $maker - rename image with thumb_ or as required
     * @param string $folder  - set folder name if reqired to change defualt foder 
     * @return string - new image name
     */
    protected function imageUploadResize($filename, $field_name, $width, $height, $maker, $folder = NULL) {
        // default folder name
        if (!isset($folder)) {
            $folder = "airline";
        }
        // thumb image upload settings
        $config['upload_path'] = './uploads/' . $folder . '/';
        $config['allowed_types'] = 'gif|jpg|png';
        // thumb upload settings
        // rename image name, this will allow to upload multiple images with same name
        // time() returns current time timestamp
        $image_new_name = time() . '_' . $filename;
        $config['file_name'] = $image_new_name;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // upload thumb image
        if ($this->upload->do_upload($field_name)) {
            // resize uploaded file
            // config data
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/' . $folder . '/' . $image_new_name;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = FALSE;
            $config['thumb_marker'] = '';
            $config['width'] = $width;
            $config['height'] = $height;
            $config['new_image'] = $maker . $image_new_name;
            $this->load->library('image_lib', $config);
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            // print if want not used
            $error = array('error' => $this->upload->display_errors());
        }
        // return new image name
        return $config['new_image'];
    }    

}