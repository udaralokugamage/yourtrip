<?php
/**
 * login class
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    /**
     * login function
     */
    public function index() {
        // load session library
        $this->load->library('session');
        // load models
        $this->load->model('Users');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load validation library
        $this->load->library('form_validation');
        // validation rules
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('tmp_login_page');
        } else {
            $data = array('email' => $this->input->post('user_email'), 'password' => $this->input->post('user_password'));
            // check user with entered email and password
            $output = $this->Users->userLogin($data);
            if (empty($output)) {
                // if user not available with entered details redirect to login page with error message
                $data['msg'] = "Incorrect e-mail or password";
                $this->load->view('tmp_login_page', $data);
            } else {
                // if user available set session data
                // and redirect to dashboard
                $sessionData = array(
                    'first_name' => $output['first_name'],
                    'last_name' => $output['last_name'],
                    'id' => $output['user_id'],
                    'user_type' => $output['user_type']
                );
                $this->session->set_userdata($sessionData);
                redirect('/dashboard');
            }
        }
    }

    /**
     * forgot password screen
     */
    public function forgot() {
        // load session library
        $this->load->library('session');
        // load model
        $this->load->model('Users');
        // load form and url helpers eg base_url
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // set form validation rules
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('tmp_forgot_password');
        } else {
            $data = array('email' => $this->input->post('user_email'));
            // check entered email in db
            $output = $this->Users->userForgotPassword($data);
            if (empty($output)) {
                // if email not exist send error message to view file
                $data['msg'] = "Incorrect e-mail";
                $this->load->view('tmp_forgot_password', $data);
            } else {
                // generate query string using time and email address
                $queryString = hash('md5', time() . " " . $this->input->post('user_email'), false);
                // add that query string to db
                $this->Users->addQueryString($this->input->post('user_email'), $queryString);
                // call send mail function
                $this->sendConfirmationEmail($this->input->post('user_email'), $queryString);
                $data['msg'] = "Password reset link is sent to your email address";
                $this->load->view('tmp_forgot_password', $data);
            }
        }
    }

    public function reset() {
        // load session library
        $this->load->library('session');
        // load model
        $this->load->model('Users');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // set validation rules
        $this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[6]');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm Password', 'required|matches[new_password]');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('tmp_reset_password');
        } else {
            // check browser url to capture query string
            if (isset($_GET['con'])) {
                // if query string not empty
                if (!empty($_GET['con'])) {
                    // then update db with new password 
                    // and remove query string from db
                    $output = $this->Users->userResetPassword($this->input->post('new_password'), $_GET['con']);
                    if($output == 0){
                        // if update not successfull send following error message
                        // Note: user can change password one time from one request
                        // if user want to change password agian
                        // user has to send another request
                        // if not this error message will appear
                        $data['msg'] = "Reset password link is expired";
                        $this->load->view('tmp_reset_password',$data);
                    }else{
                        redirect('/login?msg=1');
                    }
                }
            }
        }
    }

    private function sendConfirmationEmail($email, $link) {
        // load php mailer
        $this->load->library('Lib_PHPMailer');
        $mail = new PHPMailer;
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = "smtp.gmail.com";
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = 587;
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = "anitakosh2017@gmail.com";
        //Password to use for SMTP authentication
        $mail->Password = 'anita.anita';
        //Set who the message is to be sent from
        $mail->setFrom('anitakosh2017@gmail.com', 'Travel Agency');
        //Set an alternative reply-to address
        $mail->addReplyTo('anitakosh2017@gmail.com', 'Travel Agency');
        //Set who the message is to be sent to
        $mail->addAddress($email, '');
        //Set the subject line
        $mail->Subject = 'Travel Agency: Reset Password';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $body = ' <p>Dear User,</p>
                  <p>
                        Please click the link below to reset your password
                        <br/>
                        <a href="' . base_url() . '/login/reset?con=' . $link . '">Reset Password</a>
                  </p> 
                  <p>
                    Regards, <br/>
                    Travel Agency
                  </p>
                    
                ';
        $mail->msgHTML($body);
        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
        //send the message, check for errors
        if (!$mail->send()) {
            return 0;
        } else {
            return 1;
        }
    }

}
