<?php
/*
 * list all customers
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');         
    }    
    
    public function index(){
        // load customer model
        $this->load->model('Customers');
        // load helper url and forms, eg base_url()
        $this->load->helper(array('form', 'url'));
        // add all customers from booking to customer table,
        // if customer not exist in customer table
        //$this->Customers->customersInsertFromBooking();
        // view all customers
        $this->data['allData'] = $this->Customers->viewAll();
        $this->load->template('tmp_view_customers', $this->data); 
    }
    
    public function status(){
        // load customer model
        $this->load->model('Customers');
        // customer enable disable
        $this->Customers->changeStatus($_GET['st'], $_POST['cus_id']);
    }
    
}