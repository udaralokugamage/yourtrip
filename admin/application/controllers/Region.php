<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');         
    }    
    
    public function index(){
        $this->load->model('Regions');
        $this->load->helper(array('form', 'url'));
        $this->data['allData'] = $this->Regions->viewAll();
        $this->load->template('tmp_view_regions', $this->data); 
    }
    
    public function add(){
        if($this->data['user_type'] == 2){
           redirect('/access');
        }        
        $this->load->model('Regions');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('region_name', 'Region Name', 'required|is_unique[region.r_name]');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_add_region', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['region_name'] = $this->input->post('region_name');
                // save data on table
                if($this->Regions->addRegion($data)){
                    $this->data['msg'] = 'Region Added';
                    $this->load->template('tmp_add_region', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to add region';
                    $this->load->template('tmp_add_region',  $this->data);
                }
            }                
        }         
    }
    
    public function edit(){
        if($this->data['user_type'] == 2){
           redirect('/access');
        }        
        $this->load->model('Regions');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->data['regionData'] = $this->Regions->viewRegion($_GET['id']);

        $this->form_validation->set_rules('region_name', 'Region Name', 'required');

        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_edit_region', $this->data);
        }else{
            if ($this->input->post('submit')==true) {
                $data['region_name'] = $this->input->post('region_name');
                // user id
                $data['id'] = $_GET['id'];
                // save data on table
                $this->Regions->regionUpdate($data);
                $this->data['msg'] = 'Region Updated';
                $this->data['regionData'] = $this->Regions->viewRegion($_GET['id']);
                $this->load->template('tmp_edit_region', $this->data);
            }                
        }         
    }

    public function delete(){
        if($this->data['user_type'] == 2){
           redirect('/access');
        }        
        $this->load->model('Regions');
        $this->Regions->delete($_GET['id']);
        redirect('/region');
    }    

}