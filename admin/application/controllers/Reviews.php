<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('reviews_model','packages'));
    $this->load->helper(array('form', 'url'));
    $this->load->library(array('form_validation', 'session'));

    if (!is_logged_in()) {
        redirect('/');
    }
    // session data
    $this->data['first_name'] = $this->session->userdata('first_name');
    $this->data['last_name'] = $this->session->userdata('last_name');
    // user type and id in session
    $this->data['user_type'] = $this->session->userdata('user_type');
    $this->data['user_id'] = $this->session->userdata('id'); 
  }

  // back

  public function index_back($package_id=1)
  {
    $data['reviews'] = $this->reviews_model->reviews_get($package_id);
    $data['packages'] = $this->packages->viewAll();
    $data['package'] = $this->packages->getPackage($package_id);

    $this->load->template('tmp_admin_reviews', $data);
  }

  public function update_status($review_id, $visible)
  {
    $response = $this->reviews_model->review_update($review_id, $visible);
    $this->load->template('reviews/tmp_admin_reviews', $response);
  }

  public function delete($package_id=1, $review_id) {
    $this->reviews_model->review_delete($review_id);
    $data['reviews'] = $this->reviews_model->reviews_get($package_id);
    $this->load->template('reviews/tmp_admin_reviews', $data);
  }


  // front

  public function index($package_id=1)
  {
    $data['reviews'] = $this->reviews_model->reviews_get($package_id);
    $this->load->view('package', $data);
  }

  public function review_form($package_id, $user_id)
  {
    $data['package_id'] = $package_id;
    $this->load->view('reviews/review_form', $data);
  }

  public function create()
  {
    
    $this->form_validation->set_rules('comment', 'Please Enter Your Comment', 'required');
    $this->form_validation->set_rules('stars', 'Please Rate', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->load->view('reviews/review_form');
    }
    else
    {
      $package_id = $this->reviews_model->review_post();
      redirect('packages/show/'.$package_id);
    }
  }
}