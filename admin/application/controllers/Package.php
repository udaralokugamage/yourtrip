<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        if (!is_logged_in()) {
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');          
    }

    /**
     * package list view
     */
    public function index() {
        // load model
        $this->load->model('Packages');
        // load helpers
        $this->load->helper(array('form', 'url'));
        // get current user id and user type from session
        $createdBy = $this->session->userdata('id');
        $userType = $this->session->userdata('user_type');
        // admin userType is 1
        // admin users can see all packages
        if ($userType == 1) {
            $this->data['allData'] = $this->Packages->viewAll();
        } else {
            // agent users can see only their packages
            $this->data['allData'] = $this->Packages->viewAll($createdBy);
        }
        $this->load->template('tmp_view_package', $this->data);
    }

    /**
     * add package
     */
    public function add() {
        // load models
        $this->load->model('Packages');
        $this->load->model('Regions');
        // get all regions data to region drop down
        $this->data['regions'] = $this->Regions->viewAll();
        // load helper form and url
        $this->load->helper(array('form', 'url'));
        // set form validation
        $this->load->library('form_validation');
        // form validation rules
        $this->form_validation->set_rules('package_name', 'Package Name', 'required');
        $this->form_validation->set_rules('fixed_no_of_days', 'Number of days', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('detailed_itinerary', 'Detailed Itinerary', 'required');
        $this->form_validation->set_rules('price_adult', 'Price per adult', 'required');
        $this->form_validation->set_rules('region_id', 'Region', 'required');
        $this->form_validation->set_rules('country_id', 'Country', 'required');
        // track form validation
        if ($this->form_validation->run() == FALSE) {
            $this->load->template('tmp_add_package', $this->data);
        } else {
            // if user press submit button
            if ($this->input->post('submit') == true) {
                // capture package data
                $data['package_name'] = $this->input->post('package_name');
                $data['fixed_no_of_days'] = $this->input->post('fixed_no_of_days');
                $data['description'] = $this->input->post('description');
                $data['detailed_itinerary'] = $this->input->post('detailed_itinerary');
                $data['price_adult'] = $this->input->post('price_adult');
                $data['price_child'] = $this->input->post('price_child');
                $data['region_id'] = $this->input->post('region_id');
                $data['country_id'] = $this->input->post('country_id');
                // country name
                $this->load->model('Countries');
                $countryData = $this->Countries->viewCountry($data['country_id']);
                $data['country_name'] =$countryData[0]->name;
                // region name
                $this->load->model('Regions');
                $regionData = $this->Regions->viewRegion($data['region_id']);
                $data['region_name'] =$regionData[0]->r_name;
                
                // if thumb image upload
                if (!empty($_FILES["thumb_image"]['name'])) {
                    // generate and upload resized thumb image
                    $thumb_image_new_name = $this->imageUploadResize($_FILES["thumb_image"]['name'], 'thumb_image', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_');
                    $data['thumb_image'] = $thumb_image_new_name;
                } else {
                    $data['thumb_image'] = "";
                }
                // if banner image upload
                if (!empty($_FILES["banner_image"]['name'])) {
                    // banner upload to relevent folder
                    $banner_image_new_name = $this->imageUploadResize($_FILES["banner_image"]['name'], 'banner_image', BANNER_IMAGE_WIDTH, BANNER_IMAGE_HEIGHT, 'banner_');
                    $data['banner_image'] = $banner_image_new_name;
                } else {
                    $data['banner_image'] = "";
                }
                // save data on table
                if ($this->Packages->addPackage($data)) {
                    $this->data['msg'] = 'Package Added';
                    $this->load->template('tmp_add_package', $this->data);
                } else {
                    $this->data['msg'] = 'Failed to add package';
                    $this->load->template('tmp_add_package', $this->data);
                }
            }
        }
    }

    /**
     * edit package
     */
    public function view() {
        $this->load->model('Packages');
        $this->load->model('Countries');
        $this->load->model('Regions');
        $this->data['regions'] = $this->Regions->viewAll();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        // filter package data
        $this->data['packageData'] = $this->Packages->getPackage($_GET['id']);
        if(empty($this->data['packageData'])){
            redirect('/access');
        }
        // get related categories
        $this->data['allCountries'] = $this->Countries->regionCountry($this->data['packageData'][0]->region_id);

        $this->form_validation->set_rules('package_name', 'Package Name', 'required');
        $this->form_validation->set_rules('fixed_no_of_days', 'Number of days', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('detailed_itinerary', 'Detailed Itinerary', 'required');
        $this->form_validation->set_rules('price_adult', 'Price per adult', 'required');
        $this->form_validation->set_rules('region_id', 'Region', 'required');
        $this->form_validation->set_rules('country_id', 'Country', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->template('tmp_edit_package', $this->data);
        } else {
            if ($this->input->post('submit') == true) {
                $data['package_name'] = $this->input->post('package_name');
                $data['fixed_no_of_days'] = $this->input->post('fixed_no_of_days');
                $data['description'] = $this->input->post('description');
                $data['detailed_itinerary'] = $this->input->post('detailed_itinerary');
                $data['price_adult'] = $this->input->post('price_adult');
                $data['price_child'] = $this->input->post('price_child');
                $data['region_id'] = $this->input->post('region_id');
                $data['country_id'] = $this->input->post('country_id');
                // country name
                $this->load->model('Countries');
                $countryData = $this->Countries->viewCountry($data['country_id']);
                $data['country_name'] =$countryData[0]->name;
                // region name
                $this->load->model('Regions');
                $regionData = $this->Regions->viewRegion($data['region_id']);
                $data['region_name'] =$regionData[0]->r_name;
                
                $data['package_id'] = $_GET['id'];
                // allocate image name to data set
                if (!empty($_FILES["thumb_image"]['name'])) {
                    // thumb upload
                    $thumb_image_new_name = $this->imageUploadResize($_FILES["thumb_image"]['name'], 'thumb_image', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_');
                    $data['thumb_image'] = $thumb_image_new_name;
                } else {
                    $data['thumb_image'] = $this->data['packageData'][0]->thumb_image;
                }
                // allocate image name to data set
                if (!empty($_FILES["banner_image"]['name'])) {
                    // banner upload
                    $banner_image_new_name = $this->imageUploadResize($_FILES["banner_image"]['name'], 'banner_image', BANNER_IMAGE_WIDTH, BANNER_IMAGE_HEIGHT, 'banner_');
                    $data['banner_image'] = $banner_image_new_name;
                } else {
                    $data['banner_image'] = $this->data['packageData'][0]->banner_image;
                }
                // save data on table
                if ($this->Packages->updatePackage($data)) {
                    // filter package data
                    $this->data['packageData'] = $this->Packages->getPackage($_GET['id']);
                    // get related countries
                    $this->data['allCountries'] = $this->Countries->regionCountry($this->data['packageData'][0]->region_id);
                    $this->data['msg'] = 'Package Updated';
                    $this->load->template('tmp_edit_package', $this->data);
                } else {
                    // filter package data
                    $this->data['packageData'] = $this->Packages->getPackage($_GET['id']);
                    // get related countries
                    $this->data['allCountries'] = $this->Countries->regionCountry($this->data['packageData'][0]->region_id);
                    $this->data['msg'] = 'Failed to update package';
                    $this->load->template('tmp_edit_package', $this->data);
                }
            }
        }
    }

    /**
     * delete package
     */
    public function delete() {
        $this->load->model('Packages');
        $this->Packages->delete($_GET['id']);
        redirect('/package');
    }

    /**
     * image upload function
     * @param type $filename - eg: $_FILES['image_name']['name']
     * @param type $field_name - eg: image_name
     * @param type $width
     * @param type $height
     * @param type $maker - rename image with thumb_ or as required
     * @param string $folder  - set folder name if reqired to change defualt foder 
     * @return string - new image name
     */
    protected function imageUploadResize($filename, $field_name, $width, $height, $maker, $folder = NULL) {

        if (!isset($folder)) {
            $folder = "packages";
        }

        // image upload settings
        $config['upload_path'] = './uploads/' . $folder . '/';
        $config['allowed_types'] = 'gif|jpg|png';
        // image upload settings
        // rename image name, this will allow to upload multiple images with same name
        // time() returns current time timestamp
        $image_new_name = time() . '_' . $filename;
        $config['file_name'] = $image_new_name;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // upload thumb image
        if ($this->upload->do_upload($field_name)) {
            // resize uploaded file
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/' . $folder . '/' . $image_new_name;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = FALSE;
            $config['thumb_marker'] = '';
            $config['width'] = $width;
            $config['height'] = $height;
            $config['new_image'] = $maker . $image_new_name;
            $this->load->library('image_lib', $config);
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            // print if want
            $error = array('error' => $this->upload->display_errors());
        }
        return $config['new_image'];
    }

    /**
     * generate country dropdown for jquery ajax request
     */
    public function countries() {
        $this->load->model('Countries');
        $allCountries = $this->Countries->regionCountry($_POST['sub_id']);
        $subData = '<label class="control-label">Country:</label>';
        $subData .= '<select class="form-control" id="country_id" name="country_id">';
        if (!empty($allCountries)) {
            $subData .= '<option value="">Select a Country</option>';
        }else{
            $subData .= '<option value="">No Countries</option>';
        }
        if (!empty($allCountries)) {
            foreach ($allCountries as $subcat) {
                $subData .= '<option value="' . $subcat->id . '">' . $subcat->name . '</option>';
            }
        }
        $subData .= '</select>';
        echo $subData;
    }

    /*
     * package gallery
     */
    public function gallery() {
        $this->load->model('Packages');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $createdBy = $this->session->userdata('id');
        $userType = $this->session->userdata('user_type');
        // admin user type is 1, admin can upload images for any gallery
        // therefore admin can view all galleries
        if ($userType == 1) {
            $this->data['allData'] = $this->Packages->viewAll();
        } else {
            // agents can upload images to their packages
            $this->data['allData'] = $this->Packages->viewAll($createdBy);
        }
        if (isset($_GET['id'])) {
            // get all images belogn to packages
            $this->data['packageGallery'] = $this->Packages->viewGallery($createdBy, $_GET['id']);
        }
        // set validation rules
        $this->form_validation->set_rules('package_id', 'Package', 'required');
        if ($this->form_validation->run() == FALSE) {
            if (isset($_GET['id'])) {
                $this->data['packageGallery'] = $this->Packages->viewGallery($createdBy, $_GET['id']);
            }
            $this->load->template('tmp_photos', $this->data);
        } else {
            if ($this->input->post('submit') == true) {
                $data['package_id'] = $this->input->post('package_id');
                $files = $_FILES;
                $cpt = count($_FILES['banners']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['banner']['name'] = $files['banners']['name'][$i];
                    $_FILES['banner']['type'] = $files['banners']['type'][$i];
                    $_FILES['banner']['tmp_name'] = $files['banners']['tmp_name'][$i];
                    $_FILES['banner']['error'] = $files['banners']['error'][$i];
                    $_FILES['banner']['size'] = $files['banners']['size'][$i];

                    if (!empty($_FILES['banner']['name'])) {
                        $thumb_image_new_name = $this->imageUploadResize($_FILES['banner']['name'], 'banner', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_', 'gallery');
                        $data['thumb_image'] = $thumb_image_new_name;
                        // get file name
                        $file_name = explode('thumb_', $thumb_image_new_name);
                        $data['file_name'] = $file_name[1];
                        // add image to gallery
                        $this->Packages->addGallery($data);
                    }
                }
            }
            $this->data['packageGallery'] = $this->Packages->viewGallery($createdBy, $_GET['id']);
            $this->load->template('tmp_photos', $this->data);
        }
    }

    /**
     * gallery image delete
     */
    public function deleteImg() {
        if (isset($_POST['deleteimg'])) {
            $this->load->model('Packages');
            foreach ($_POST['deleteimg'] as $img) {
                $getImageData = $this->Packages->imgData($img);
                $thumb = 'uploads/gallery/' . $getImageData[0]->thumb_image;
                unlink($thumb);
                $imgFull = 'uploads/gallery/' . $getImageData[0]->file_name;
                unlink($imgFull);
                $this->Packages->delImg($img);
            }
        }
        redirect('/package/gallery?id=' . $_GET['id']);
    }
    
    /**
     * display packages to approve
     */
    public function approve(){
        $this->load->model('Packages');
        $this->load->helper(array('form', 'url'));
        $this->data['allData'] = $this->Packages->approvePackages();
        $this->load->template('tmp_approve_packages', $this->data);
    }
    
    /**
     * package status change
     */
    public function status(){
        $this->load->model('Packages');
        $this->Packages->changeStatus($_GET['st'], $_POST['pack_id']);
    }    

}
