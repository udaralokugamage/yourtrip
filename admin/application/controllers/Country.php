<?php
/**
 * Country add/edit/delete/view class
 */ 
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');        
    }    
    
    /**
     * list view
     */
    public function index(){
        // load model
        $this->load->model('Countries');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // get all countries
        $this->data['allData'] = $this->Countries->viewAll();
        // send all data to view file
        $this->load->template('tmp_view_country', $this->data); 
    }
    
    /**
     * data add
     */
    public function add(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }
        // load models
        $this->load->model('Countries');
        $this->load->model('Regions');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load validation library
        $this->load->library('form_validation');
        // get all region data, this is used show region dropdown 
        $this->data['allRegions'] = $this->Regions->viewAll();    
        // validation rules
        $this->form_validation->set_rules('country_name', 'Country Name', 'required|is_unique[country.name]');
        $this->form_validation->set_rules('region_id', 'Region Name', 'required');
        // track form validation
        if ($this->form_validation->run() == FALSE){      
            // if validation fail return to page with error messages
            $this->load->template('tmp_add_country', $this->data);
        }else{
            // if form submit button press
            if ($this->input->post('submit')==true) {
                // capture form data
                $data['country_name'] = $this->input->post('country_name');
                $data['region_id'] = $this->input->post('region_id');
                // save data on table
                if($this->Countries->addCountry($data)){
                    $this->data['msg'] = 'Country Added';
                    $this->load->template('tmp_add_country', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to add country';
                    $this->load->template('tmp_add_country',  $this->data);
                }
            }                
        }         
    }
    
    /**
     * data edit
     */
    public function edit(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }   
        // load models
        $this->load->model('Countries');
        $this->load->model('Regions');
        // load form and url helpers eg base_url()
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // all regions for select drop down
        $this->data['allRegions'] = $this->Regions->viewAll();
        // selected counry data
        $this->data['countryData'] = $this->Countries->viewCountry($_GET['id']);
        // validation rules
        $this->form_validation->set_rules('country_name', 'Country Name', 'required');
        $this->form_validation->set_rules('region_id', 'Region Name', 'required');
        // track validations
        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_edit_country', $this->data);
        }else{
            // if submit button press
            if ($this->input->post('submit')==true) {
                //capture form data
                $data['country_name'] = $this->input->post('country_name');
                $data['region_id'] = $this->input->post('region_id');
                // get country id from url
                $data['id'] = $_GET['id'];
                // update data to table
                $this->Countries->countryUpdate($data);
                $this->data['msg'] = 'Country Updated';
                // get updated data again
                $this->data['countryData'] = $this->Countries->viewCountry($_GET['id']);
                $this->load->template('tmp_edit_country', $this->data);
            }                
        }         
    }

    /**
     * data delete
     */
    public function delete(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller    
        if($this->data['user_type'] == 2){
           redirect('/access');
        }         
        $this->load->model('Countries');
        $this->Countries->delete($_GET['id']);
        redirect('/country');
    }    

}