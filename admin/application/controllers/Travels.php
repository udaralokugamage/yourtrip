<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Travels extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('reviews_model','packages','travels_model'));
    $this->load->helper(array('form', 'url'));
    $this->load->library(array('form_validation', 'session', 'upload'));

    if (!is_logged_in()) {
        redirect('/');
    }
    // session data
    $this->data['first_name'] = $this->session->userdata('first_name');
    $this->data['last_name'] = $this->session->userdata('last_name');
    // user type and id in session
    $this->data['user_type'] = $this->session->userdata('user_type');
    $this->data['user_id'] = $this->session->userdata('id'); 
  }

  // back

  public function new_travel()
  {
    $this->load->template('tmp_admin_travels_new');
  }

  public function edit_travel($travel_id)
  {
    $data['packages'] = $this->travels_model->travel_packages_list_get($travel_id);
    $data['travel'] = $this->travels_model->travel_get($travel_id);
    $data['travel_packages'] = $this->travels_model->travel_packages_get($travel_id);
    $this->load->template('tmp_admin_travels_edit', $data);
  }

  public function all_travel()
  {
    $data['travels'] = $this->travels_model->travels_get();
    $this->load->template('tmp_admin_travels', $data);
  }

  public function new_travel_media($travel_id)
  {
    $data['travel'] = $this->travels_model->travel_get($travel_id);
    $data['travel_images'] = $this->travels_model->travel_pictures_get($travel_id);
    $this->load->template('tmp_admin_travels_media_new', $data);
  }

  public function create_travel($package_id=1)
  {
    $this->form_validation->set_rules('lat', 'Select Latitides', 'required');
    $this->form_validation->set_rules('longt', 'Select Longtitude', 'required');
    $this->form_validation->set_rules('description', 'Enter Description', 'required');
    $this->form_validation->set_rules('title', 'Enter Title', 'required');

    if ($this->form_validation->run() === FALSE)
    {

      $this->load->template('tmp_admin_travels_new');
    }
    else
    {
      $travel_id = $this->travels_model->travel_post();
      redirect('travel/media/new/'.$travel_id);
    }
  }

  public function update_travel()
  {
    $this->travels_model->travel_update();
    redirect('travels');
  }

  public function create_travel_media()
  { 
    $config['upload_path']          = './uploads/travel/';
    $config['allowed_types']        = 'gif|jpg|png|mp4';
    $config['max_width']            = 1024;
    $config['max_height']           = 768;

    // print_r(is_really_writable($config['upload_path']));

    // $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('userfile'))
    {
      $error = array('error' => $this->upload->display_errors());
      print_r($error);
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());
      $filename = $data['upload_data']['file_name'];
      $filetype = $data['upload_data']['file_type'];
      $isimage = ($filetype == "video/mp4") ? false : true ;

      $travel_id = $this->travels_model->travel_media_post($filename, $isimage);
      redirect('travel/media/new/'.$travel_id);
    }
  }

  public function create_travel_package()
  {
    $travel_id = $this->travels_model->travel_package_post();
    redirect('travel/'.$travel_id);
  }

  public function delete_media($media_id, $travel_id) 
  {
    $this->travels_model->media_delete($media_id);
    redirect('travel/media/new/'.$travel_id);
  }

  public function delete_travel_package($travel_package_id, $travel_id) 
  {
    $this->travels_model->travel_package_delete($travel_package_id);
    redirect('travel/'.$travel_id);
  }

  public function delete($travel_id) 
  {
    $this->travels_model->delete($travel_id);
    redirect('travels');
  }

  // front

  public function index($package_id=1)
  {
    $data['reviews'] = $this->reviews_model->reviews_get($package_id);
    $this->load->view('package', $data);
  }

  public function review_form($package_id, $user_id)
  {
    $data['package_id'] = $package_id;
    $this->load->view('reviews/review_form', $data);
  }

  public function create()
  {
    
    $this->form_validation->set_rules('comment', 'Please Enter Your Comment', 'required');
    $this->form_validation->set_rules('stars', 'Please Rate', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->load->view('reviews/review_form');
    }
    else
    {
      $package_id = $this->reviews_model->review_post();
      redirect('packages/show/'.$package_id);
    }
  }
}