<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {

    public $data = array();
    public function __construct() {
        parent::__construct();
        if(!is_logged_in()){
            redirect('/');
        }
        // session data
        $this->data['first_name'] = $this->session->userdata('first_name');
        $this->data['last_name'] = $this->session->userdata('last_name');
        // user type and id in session
        $this->data['user_type'] = $this->session->userdata('user_type');
        $this->data['user_id'] = $this->session->userdata('id');         
    }    
    
    /**
     * list view
     */
    public function index(){
        // load models
        $this->load->model('Hotels');
        // load form and url helpers eg. base_url()
        $this->load->helper(array('form', 'url'));
        // get all hotels
        $this->data['allData'] = $this->Hotels->viewAll();
        $this->load->template('tmp_view_hotels', $this->data); 
    }
    
    /**
     * add hotel
     */
    public function add(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        } 
        // load models
        $this->load->model('Hotels');
        // load form and url helpers eg. base_url()
        $this->load->helper(array('form', 'url'));
        // load form validation library
        $this->load->library('form_validation');
        // required field set
        $this->form_validation->set_rules('hotel_name', 'Hotel Name', 'required|is_unique[hotel.hotel_name]');
        // track validation
        if ($this->form_validation->run() == FALSE){    
            // if validation fail return to page with errors
            $this->load->template('tmp_add_hotel', $this->data);
        }else{
            // validation success,
            // if press submit button
            if ($this->input->post('submit')==true) {
                // capture hotel data
                $data['hotel_name'] = $this->input->post('hotel_name');
                $data['hotel_description'] = $this->input->post('hotel_description');
                // save data on table
                if($this->Hotels->addHotel($data)){
                    $this->data['msg'] = 'Hotel Added';
                    $this->load->template('tmp_add_hotel', $this->data);
                }else{
                    $this->data['msg'] = 'Failed to add hotel';
                    $this->load->template('tmp_add_hotel',  $this->data);
                }
            }                
        }         
    }

    /**
     * edit hotel
     */
    public function edit(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }       
        // load models
        $this->load->model('Hotels');
        // load form and url helpers eg. base_url()
        $this->load->helper(array('form', 'url'));
        // load validation library
        $this->load->library('form_validation');
        // get selected hotel data
        $this->data['hotelData'] = $this->Hotels->viewHotel($_GET['id']);
        // validation rules
        $this->form_validation->set_rules('hotel_name', 'Hotel Name', 'required');
        // track form validation
        if ($this->form_validation->run() == FALSE){              
            $this->load->template('tmp_edit_hotels', $this->data);
        }else{
            // if press submit button
            if ($this->input->post('submit')==true) {
                // capture form data
                $data['hotel_name'] = $this->input->post('hotel_name');
                $data['hotel_description'] = $this->input->post('hotel_description');
                // get hotel id from url
                $data['id'] = $_GET['id'];
                // update hotel table
                $this->Hotels->hotelUpdate($data);
                $this->data['msg'] = 'Hotel Updated';
                $this->data['hotelData'] = $this->Hotels->viewHotel($_GET['id']);
                $this->load->template('tmp_edit_hotels', $this->data);
            }                
        }         
    }

    public function delete(){
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }          
        $this->load->model('Hotels');
        $this->Hotels->delete($_GET['id']);
        redirect('/hotel');
    }   
    
    public function gallery() {
        // user_type 2 means agents, if agent try to access this function,
        // it will be redirected to access controller        
        if($this->data['user_type'] == 2){
           redirect('/access');
        }     
        // load models
        $this->load->model('Hotels');
        // load form and url helpers eg. base_url
        $this->load->helper(array('form', 'url'));
        // add form validation library
        $this->load->library('form_validation');
        // get all hotels for select dropdown
        $this->data['allData'] = $this->Hotels->viewAll();
        // if hotel selected from select dropdown
        // get all images belongs to that hotels
        if (isset($_GET['id'])) {
            $this->data['hotelGallery'] = $this->Hotels->viewGallery($_GET['id']);
        }
        // validation rules
        $this->form_validation->set_rules('hotel_id', 'Hotel', 'required');
        // track form validation
        if ($this->form_validation->run() == FALSE) {
            // check hotel id in url
            if (isset($_GET['id'])) {
                // if hotel selected from select dropdown
                // get all images belongs to that hotels                
                $this->data['hotelGallery'] = $this->Hotels->viewGallery($_GET['id']);
            }
            $this->load->template('tmp_hotel_photos', $this->data);
        } else {
            // if submit button press
            if ($this->input->post('submit') == true) {
                $data['hotel_id'] = $this->input->post('hotel_id');
                // copy $_FILES data to files variable
                $files = $_FILES;
                // get uploaded image count
                $cpt = count($_FILES['banners']['name']);
                // rearrange $_FILES array and send data to resize
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['banner']['name'] = $files['banners']['name'][$i];
                    $_FILES['banner']['type'] = $files['banners']['type'][$i];
                    $_FILES['banner']['tmp_name'] = $files['banners']['tmp_name'][$i];
                    $_FILES['banner']['error'] = $files['banners']['error'][$i];
                    $_FILES['banner']['size'] = $files['banners']['size'][$i];
                    
                    if (!empty($_FILES['banner']['name'])) {
                        // upload actual size image, create resize image and upload
                        $thumb_image_new_name = $this->imageUploadResize($_FILES['banner']['name'], 'banner', THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT, 'thumb_');
                        $data['thumb_image'] = $thumb_image_new_name;
                        // imageUploadResize function returns thumbnail image name.
                        // expload thumb image to get actual size image name
                        $file_name = explode('thumb_', $thumb_image_new_name);
                        $data['file_name'] = $file_name[1];
                        // add image to gallery
                        $this->Hotels->addGallery($data);
                    }
                }
            }
            // get selected hotel images
            $this->data['hotelGallery'] = $this->Hotels->viewGallery($_GET['id']);
            $this->load->template('tmp_hotel_photos', $this->data);
        }
    }

    public function deleteImg() {
        // if checked image delete checkboxes
        if (isset($_POST['deleteimg'])) {
            // load models
            $this->load->model('Hotels');
            // deleteimg[] array and delete thumb image and actual size image
            // deleteimg[] contain image ids
            foreach ($_POST['deleteimg'] as $img) {
                // get images data from id (thumbnail image name and actual image name)
                $getImageData = $this->Hotels->imgData($img);
                // remove thumb image from folder
                $thumb = 'uploads/hotels/' . $getImageData[0]->thumb_image;
                unlink($thumb);
                // remove actual size image from folder
                $imgFull = 'uploads/hotels/' . $getImageData[0]->file_name;
                unlink($imgFull);
                // delete image record from hotel gallery table
                $this->Hotels->delImg($img);
            }
        }
        // redirected current page
        redirect('/hotel/gallery?id=' . $_GET['id']);
    }    
    
    /**
     * image upload function
     * @param type $filename - eg: $_FILES['image_name']['name']
     * @param type $field_name - eg: image_name
     * @param type $width
     * @param type $height
     * @param type $maker - rename image with thumb_ or as required
     * @param string $folder  - set folder name if reqired to change defualt foder 
     * @return string - new image name
     */
    protected function imageUploadResize($filename, $field_name, $width, $height, $maker, $folder = NULL) {
        // default image upload folder
        if (!isset($folder)) {
            $folder = "hotels";
        }

        // thumb image upload settings
        $config['upload_path'] = './uploads/' . $folder . '/';
        $config['allowed_types'] = 'gif|jpg|png';
        // thumb upload settings
        // rename image name, this will allow to upload multiple images with same name
        // time() returns current time timestamp
        $image_new_name = time() . '_' . $filename;
        $config['file_name'] = $image_new_name;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // upload thumb image
        if ($this->upload->do_upload($field_name)) {
            // resize uploaded file
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/' . $folder . '/' . $image_new_name;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = FALSE;
            $config['thumb_marker'] = '';
            $config['width'] = $width;
            $config['height'] = $height;
            $config['new_image'] = $maker . $image_new_name;
            $this->load->library('image_lib', $config);
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            // print if want
            $error = array('error' => $this->upload->display_errors());
        }
        // return new image name
        return $config['new_image'];
    }    
}