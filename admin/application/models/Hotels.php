<?php

class Hotels extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function addHotel($data){
        $data = array
        (
            'hotel_name' => $data['hotel_name'],
            'hotel_description' => $data['hotel_description'],
        );
        if($this->db->insert('hotel',$data)){
            return TRUE;
        }    
        return FALSE;
    }
    
    /**
     * 
     * @param type $id
     * @return type all results
     */
    public function viewHotel($id){
        $this->db->select('*');
        $this->db->from('hotel');
        $this->db->where('hotel_id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    public function viewAll(){
        $query = $this->db->get('hotel');
        return $query->result();       
    }    
    
    /**
     * 
     * @param type $id, remove from db
     */
    public function delete($id){
        $this->db->delete('hotel', array('hotel_id' => $id)); 
    } 
   
    /**
     * 
     * @param array $data
     */
    public function hotelUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'hotel_name' => $data['hotel_name'],
            'hotel_description' => $data['hotel_description'],
        );
        $this->db->where('hotel_id', $id);
        $this->db->update('hotel',$data);          
    }
    
    /**
     * 
     * @param type $hotelId
     * @return type all photos belong to that id
     */
    public function viewGallery($hotelId){
        $querySet = " 
            SELECT hotels_gallery.*
            FROM hotels_gallery
            WHERE hotels_gallery.hotel_id='".$hotelId."' 
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }    
    
    /**
     * 
     * @param array $data
     * @return int 1 success, 0 - not success
     */
    public function addGallery($data){
        $data = array
        (
            'hotel_id' => $data['hotel_id'],
            'file_name' => $data['file_name'],
            'thumb_image' => $data['thumb_image'],
        );
        if($this->db->insert('hotels_gallery',$data)){
            return 1;
        }else{
            return 0;
        }         
    }
    
    /**
     * get image data before delete the image,
     * based on this data remove image from folder
     * @param type $id
     * @return type
     */
    public function imgData($id){
        $querySet = " 
            SELECT hotels_gallery.*
            FROM hotels_gallery
            WHERE hotels_gallery.image_id='".$id."'
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    /**
     * delete image
     * @param type $id 
     */
    public function delImg($id){
        $this->db->delete('hotels_gallery', array('image_id' => $id)); 
    }  
    
}

