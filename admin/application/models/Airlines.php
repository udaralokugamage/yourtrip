<?php

class Airlines extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    /**
     * add airline
     * @param array $data
     * @return boolean
     */
    public function addAirline($data){
        $data = array
        (
            'airline_name' => $data['airline_name'],
            'airline_description' => $data['airline_description'],
            'thumb_image' => $data['thumb_image'],
        );
        if($this->db->insert('airline',$data)){
            return TRUE;
        }    
        return FALSE;
    }
    /*
     * view selected airline, edit mode
     */
    public function viewAirline($id){
        $this->db->select('*');
        $this->db->from('airline');
        $this->db->where('airline_id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    /*
     * view all 
     */
    public function viewAll(){
        $query = $this->db->get('airline');
        return $query->result();       
    }    
    
    /**
     * delete
     * @param type $id
     */
    public function delete($id){
        $this->db->delete('airline', array('airline_id' => $id)); 
    } 
   
    /**
     * update
     * @param array $data
     */
    public function airlineUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'airline_name' => $data['airline_name'],
            'airline_description' => $data['airline_description'],
            'thumb_image' => $data['thumb_image'],
        );
        $this->db->where('airline_id', $id);
        $this->db->update('airline',$data);          
    }
}

