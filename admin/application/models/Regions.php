<?php

class Regions extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    /**
     * add
     * @param array $data
     * @return boolean
     */
    public function addRegion($data){
        $data = array
        (
            'r_name' => $data['region_name'],
        );
        if($this->db->insert('region',$data)){
            return TRUE;
        }    
        return FALSE;
    }

    public function viewAll(){
        $query = $this->db->get('region');
        return $query->result();          
    }
    
    /**
     * view selected
     * @param type $id
     * @return type
     */
    public function viewRegion($id){
        $this->db->select('*');
        $this->db->from('region');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    /**
     * update
     * @param array $data
     */
    public function regionUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'r_name' => $data['region_name'],
        );
        $this->db->where('id', $id);
        $this->db->update('region',$data);          
    }
   
    /**
     * delete
     * @param type $id
     */
    public function delete($id){
        // delete region
        $this->db->delete('region', array('id' => $id));
        // delete countris belong to that region
        $this->db->delete('country', array('region_id' => $id));
        // change status of packages to deleted 
        // just change status not deleted from db(reason it will link to booking)
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('region_id', $id);
        $this->db->update('package',$data);         
    }      
}

