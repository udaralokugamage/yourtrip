<?php

class Travels_model extends CI_Model{

  public function __construct()
  {
    $this->load->database();
    $this->load->helper('url');
  }

  public function travels_get()
  {
    $query = $this->db->get('travel');
    return $query->result_array();
  }

  public function travel_get($travel_id)
  {
    $this->db->where('id', $travel_id);
    $query = $this->db->get('travel');
    return $query->result_array();
  }

  public function travel_packages_get($travel_id)
  {

    $this->db->select('travel_package.id as id, package.package_id as package_id, package.package_name as package_name, travel_package.travel_id as travel_id');
    $this->db->order_by('id', 'DESC');
    $this->db->from('travel_package');
    $this->db->join('package', 'travel_package.package_id = package.package_id');
    $this->db->where('travel_id', $travel_id);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function travel_packages_list_get($travel_id)
  {

    $this->db->select('package.package_id, package.package_name');
    $this->db->from('travel_package');
    $this->db->join('package', 'package.package_id = travel_package.package_id');
    $this->db->where('travel_id', $travel_id);
    $packages = $this->db->get()->result_array();

    $ids = [];
    foreach ($packages as $package) {
      array_push($ids, $package['package_id']);
    }

    if (empty($ids)) {
      $this->db->select('package_id, package_name');
      $this->db->from('package');
      $this->db->where('deleted', false);
      $query = $this->db->get();
      return $query->result_array();
    }

    $this->db->select('package_id, package_name');
    $this->db->from('package');
    $this->db->where_not_in('package_id', $ids);
    $this->db->where('deleted', false);
    $query = $this->db->get();

    return $query->result_array();
  }

  public function travel_pictures_get($travel_id)
  {
    $this->db->where('travel_id', $travel_id);
    $query = $this->db->get('travel_media');
    return $query->result_array();
  }

  public function travel_post()
  {
    $data = array(
      'lat' => $this->input->post('lat'),
      'longt' => $this->input->post('longt'),
      'description' => $this->input->post('description'),
      'title' => $this->input->post('title')
    );

    if ($this->db->insert('travel', $data)) {
      $id = $this->db->insert_id();
      return $id;
    }
  }

  public function travel_media_post($image_name, $is_image)
  {
    
    $data = array(
      'travel_id' => $this->input->post('travel_id'),
      'name' => $image_name,
      'image' => $is_image
    );

    if ($this->db->insert('travel_media', $data)) {

      $this->db->where('id', $this->db->insert_id());
      $query = $this->db->get('travel_media')->result_array();
      return ($query[0]['travel_id']);
    }
  }

  public function travel_package_post()
  {
    
    $data = array(
      'package_id' => $this->input->post('travel_package_id'),
      'travel_id' => $this->input->post('travel_travel_id')
    );

    if ($this->db->insert('travel_package', $data)) {

      $this->db->where('id', $this->db->insert_id());
      $query = $this->db->get('travel_package')->result_array();
      return ($query[0]['travel_id']);
    }
  }

  public function travel_update()
  {
    $data = array(
      'lat' => $this->input->post('lat'),
      'longt' => $this->input->post('longt'),
      'description' => $this->input->post('description'),
      'title' => $this->input->post('title')
    );

    $travel_id = $this->input->post('travel_id');

    $this->db->where('id', $travel_id);
    return $this->db->update('travel', $data);
  }

  public function media_delete($media_id)
  {
    $this->db->where('id', $media_id);
    return $this->db->delete('travel_media');
  }

  public function delete($travel_id)
  {
    $this->db->where('id', $travel_id);
    return $this->db->delete('travel');
  }

  public function travel_package_delete($travel_package_id)
  {
    $this->db->where('id', $travel_package_id);
    return $this->db->delete('travel_package');
  }
}