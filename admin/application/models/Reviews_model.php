<?php

class Reviews_model extends CI_Model{

  public function __construct()
  {
    $this->load->database();
    $this->load->helper('url');
  }

  public function reviews_get($package_id)
  {
    $this->db->select('reviews.id as id, reviews.stars as stars, reviews.comment as comment, customer.fname as fname, customer.lname as lname, visible, package_id');
    $this->db->order_by('id', 'DESC');
    $this->db->from('reviews');
    $this->db->join('customer', 'customer.id = reviews.user_id');
    $this->db->where('package_id', $package_id);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function review_get($review_id)
  {
    $this->db->where('id', $review_id);

    $query = $this->db->get('reviews');

    return $query->result_array();
  }

  public function package_review_get($review_id, $package_id)
  {
    $this->db->where('id', $review_id);
    $this->db->where('package_id', $package_id);

    $query = $this->db->get('reviews');

    return $query->result_array();
  }

  public function review_post()
  {
    $this->load->helper('url');

    $data = array(
      'stars' => $this->input->post('stars'),
      'comment' => $this->input->post('comment'),
      'user_id' => $this->input->post('user_id'),
      'package_id' => $this->input->post('package_id')
    );

    if ($this->db->insert('reviews', $data)) {
      return $data['package_id'];
    }
  }

  public function review_update($review_id, $visible)
  {
    $data = array(
      'visible' => $visible
    );

    $this->db->where('id', $review_id);
    return $this->db->update('reviews', $data);
  }

  public function review_delete($review_id)
  {
    $this->db->where('id', $review_id);
    return $this->db->delete('reviews');
  }

}