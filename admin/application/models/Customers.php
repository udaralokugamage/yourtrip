<?php

class Customers extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    public function viewAll(){
        $query = $this->db->get('customer');
        return $query->result();          
    }
    

    /**
     * when customer enable, disable from view
     * controller will call this function for change status
     * @param type $status
     * @param type $cus_id
     */
    public function changeStatus($status, $cus_id){
        $data = array
        (
            'customer_status' => $status,
        );
        $this->db->where('id', $cus_id);
        $this->db->update('customer',$data);         
    }
    
//    public function customersInsertFromBooking(){
//        $querySet = " 
//            INSERT INTO customer (customer_name, customer_email, customer_status)
//            SELECT CONCAT(booking.cus_fname, ' ', booking.cus_lname), email, 1
//            FROM booking
//            WHERE email NOT IN (SELECT customer_email FROM customer)
//        ";
//        $this->db->query($querySet);
//    }
}