<?php

class Packages extends CI_Model {

    protected  $user_type;
    protected $user_id;    
    public function __construct() {
            parent::__construct();
        // session data
        $this->user_type = $this->session->userdata('user_type');
        $this->user_id = $this->session->userdata('id');              
    }
    
    /**
     * add package
     * @param type $data
     * @return int
     */
    public function addPackage($data){
        
        $created_date = date("Y-m-d H:i:s");
        // get user type
        $userType = $this->session->userdata('user_type');
        /*
         * if admin add package(user type = 1) that package status set as enable
         * if agent add package that package status set as disable, then admin has to approve that package
         */
        if($userType == 1){
            $status = 1;
        }else{
            $status = 0;
        }
        // created user
        $createdBy = $this->session->userdata('id');
        
        $data = array
        (
            'package_name' => $data['package_name'],
            'fixed_no_of_days' => $data['fixed_no_of_days'],
            'description' => $data['description'],
            'detailed_itinerary' => $data['detailed_itinerary'],
            'price_adult' => $data['price_adult'],
            'price_child' => $data['price_child'],
            'thumb_image' => $data['thumb_image'],
            'banner_image' => $data['banner_image'],
            'status' => $status,
            'created_by' => $createdBy,
            'created_date' => $created_date,
            'region_id'=>$data['region_id'],
            'region'=>$data['region_name'],
            'country_id'=>$data['country_id'],
            'country'=>$data['country_name'],
        );
        if($this->db->insert('package',$data)){
            return 1;
        }else{
            return 0;
        }        
    }
    
    /**
     * if user_id set then show that users package only
     * @param type $user_id
     * @return type
     */
    public function viewAll($user_id = NULL){
        $querySet = '
            SELECT package.*, country.region_id, CONCAT(user.first_name," ",user.last_name) AS created_user, 
                (
                    SELECT CONCAT(region.r_name," > ",country.name) 	
                    FROM country
                    JOIN region ON country.region_id = region.id
                    WHERE country.id = package.country_id
                ) AS category_name,
                (case when (status = 1) 
                    THEN
                         "Enable" 
                    ELSE
                         "Disable" 
                    END)
                    as status_text 
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id  
            WHERE package.deleted=0
        ';
        if(!empty($user_id)){
            $querySet .= " AND package.created_by='".$user_id."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();        
    }
    
    /**
     * get package details
     * @param type $packageId
     * @return type
     */
    public function getPackage($packageId){
        $querySet = '
            SELECT package.*, country.id as country_id
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id   
            WHERE package_id="'.$packageId.'"
        ';
        if($this->user_type == 2){
            $querySet .= " AND package.created_by='".$this->user_id."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    /**
     * update
     * @param type $data
     * @return int
     */
    public function updatePackage($data){
        $updated_date = date("Y-m-d H:i:s");
        $id = $data['package_id'];
        $data = array
        (
            'package_name' => $data['package_name'],
            'fixed_no_of_days' => $data['fixed_no_of_days'],
            'description' => $data['description'],
            'detailed_itinerary' => $data['detailed_itinerary'],
            'price_adult' => $data['price_adult'],
            'price_child' => $data['price_child'],
            'thumb_image' => $data['thumb_image'],
            'banner_image' => $data['banner_image'],
            'updated_date' => $updated_date,
            'region_id'=>$data['region_id'],
            'region'=>$data['region_name'],
            'country_id'=>$data['country_id'],
            'country'=>$data['country_name']            
        );
        $this->db->where('package_id', $id);
        if($this->db->update('package',$data)){
            return 1;
        }else{
            return 0;
        }
    }     

    /**
     * just change status not deleted from db(reason it will link to booking)
     * @param type $id
     */
    public function delete($id){
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('package_id', $id);
        $this->db->update('package',$data);          
    }    
 
    /**
     * agents can view his package gallery photos
     * admin can view all package galleries 
     * view package photos
     * @param type $createdBy
     * @param type $packageId
     * @return type
     * 
     */
    public function viewGallery($createdBy, $packageId){
        $querySet = " 
            SELECT gallery.*
            FROM gallery
            JOIN package ON package.package_id = gallery.package_id
            WHERE gallery.package_id='".$packageId."' ";
        $userType = $this->session->userdata('user_type');
        if($userType == 1){
            $querySet .= "";
        }else{
            $querySet .= " AND package.created_by='".$createdBy."'";
        }        

        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    /**
     * add
     * @param array $data
     * @return int
     */
    public function addGallery($data){
        $data = array
        (
            'package_id' => $data['package_id'],
            'file_name' => $data['file_name'],
            'thumb_image' => $data['thumb_image'],
        );
        if($this->db->insert('gallery',$data)){
            return 1;
        }else{
            return 0;
        }         
    }
    
    /**
     * get image data before delete the image,
     * based on this data remove image from folder
     * @param type $id
     * @return type
     */
    public function imgData($id){
        $querySet = " 
            SELECT gallery.*
            FROM gallery
            WHERE gallery.image_id='".$id."'
        ";
        $query = $this->db->query($querySet);
        return $query->result();         
    }
    
    /**
     * delete image
     * @param type $id 
     */    
    public function delImg($id){
        $this->db->delete('gallery', array('image_id' => $id)); 
    }
    
    /**
     * count of agents added packages (not status changed)
     * if status change then status_change_track change to 1
     * then it is remove from pending list
     * @return type
     */
    public function agentPendingPackages(){
        $querySet = " 
            SELECT COUNT(*) as agent_packages
            FROM package
            JOIN user ON user.user_id = package.created_by
            WHERE user.user_type='2'  AND package.status='0' AND package.status_change_track='0' AND deleted=0       
        ";
        $query = $this->db->query($querySet);
        return $query->result();          
    }
    
    /**
     * display packages to approve
     * @return type
     */
    public function approvePackages(){
        $querySet = '
            SELECT package.*, country.region_id, CONCAT(user.first_name," ",user.last_name) AS created_user, 
                (
                    SELECT CONCAT(region.r_name," > ",country.name) 	
                    FROM country
                    JOIN region ON country.region_id = region.id
                    WHERE country.id = package.country_id
                ) AS category_name
            FROM package
            JOIN user ON user.user_id = package.created_by
            JOIN country ON country.id = package.country_id  
            WHERE user.user_type="2" AND deleted=0
            ORDER BY FIELD(status, 0, 1)
        ';
        $query = $this->db->query($querySet);
        return $query->result();        
    }   
  
    /**
     * package status change
     * @param type $status
     * @param type $cus_id
     */
    public function changeStatus($status, $cus_id){
        $data = array
        (
            'status' => $status,
            'status_change_track' => 1
        );
        $this->db->where('package_id', $cus_id);
        $this->db->update('package',$data);         
    }    
}
