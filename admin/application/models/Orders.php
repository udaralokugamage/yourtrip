<?php

class Orders extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
    /**
     * if user type 2 (agents) can view agent bookings belong to his packages
     * admin can view all bookings
     * @param type $userType
     * @param type $userId
     * @param type $limit - use to limit data in dashboard
     * @return type
     */
    public function viewAll($userType, $userId, $limit = NULL){
        $querySet = " 
            SELECT package.*, package.status as pack_status, CONCAT(customer.fname,' ',customer.lname) AS customer_name , customer.email AS customer_email, DATE(booking.date_added) AS orderdate , booking.*
            FROM booking
            JOIN customer ON customer.id = booking.cus_id
            JOIN package ON package.package_id = booking.package_id            
        ";
        
        if($userType == 2){
            $querySet .= " WHERE package.created_by='".$userId."'";
        }
        
        if(isset($limit)){
            $querySet .= " ORDER BY booking.bid DESC LIMIT ".$limit;
        }

        
        $query = $this->db->query($querySet);
        return $query->result();           
    } 
    
    /**
     * update order status
     * @param type $orderId
     * @param type $status
     * @return int
     */
    public function updateOrder($orderId, $status){
        $data = array
        (
            'status' => $status,
        );
        $this->db->where('bid', $orderId);
        if($this->db->update('booking',$data)){
            return 1;
        }else{
            return 0;
        }        
    }
    
    /**
     * agents(user type 2) can view pending bookings belong to his packages
     * admin can view all pending orders
     * @param type $userType
     * @param type $userId
     * @return type
     * booking status 0 = pending
     */
    public function pendingOrders($userType, $userId){
        $querySet = " 
            SELECT COUNT(*) as pending_orders
            FROM booking
        ";
        if($userType == 2){
            $querySet .= "
                JOIN package ON package.package_id = booking.package_id
            ";
        }
        $querySet .= "WHERE booking.status='0'";
        if($userType == 2){
            $querySet .= " AND package.created_by='".$userId."'";
        }
        
        $query = $this->db->query($querySet);
        return $query->result();         
    }
}
?>