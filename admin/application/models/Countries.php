<?php

class Countries extends CI_Model {

    public function __construct() {
            parent::__construct();
    }
    
   /**
    * add
    * @param array $data
    * @return boolean
    */
    public function addCountry($data){
        $data = array
        (
            'name' => $data['country_name'],
            'region_id' => $data['region_id'],
        );
        if($this->db->insert('country',$data)){
            return TRUE;
        }    
        return FALSE;
    }
    
    // view all
    public function viewAll(){
        $querySet = '
            SELECT country.*, country.name as country_name, region.r_name as region_name
            FROM country
            JOIN region ON region.id = country.region_id
        ';
        $query = $this->db->query($querySet);
        return $query->result();           
    }
    
    /**
     * view country
     * @param type $id
     * @return type
     */
    public function viewCountry($id){
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();          
    }
    
    /**
     * update country
     * @param array $data
     */
    public function countryUpdate($data){
        $id = $data['id'];
        $data = array
        (
            'name' => $data['country_name'],
            'region_id' => $data['region_id'],
        );
        $this->db->where('id', $id);
        $this->db->update('country',$data);          
    }
    
    /*
     * when delete country 
     * if a package belong to that country
     * then package status will change to deleted
     * when package delete, not going to remove from system
     * because it may be related to orders
     */
    public function delete($id){
        $this->db->delete('country', array('id' => $id)); 
        // delete package
        $data = array
        (
            'deleted' => 1,
        );
        $this->db->where('country_id', $id);
        $this->db->update('package',$data);         
    }   
    
    /*
     * select countries related to region id
     */
    public function regionCountry($region_id){
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('region_id', $region_id);
        $query = $this->db->get();
        return $query->result();         
    }
}

