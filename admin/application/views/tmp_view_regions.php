<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Regions</h1>
            </div>
        </div>
                <div class="col-lg-12">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Region Name</th>
                                        <?php
                                            /*
                                             * user type one mean admin users
                                             * can show last two columns
                                             */
                                        ?>                                         
                                        <?php if($user_type == 1){ ?>
                                        <th class="sorter-false">&nbsp;</th>
                                        <th class="sorter-false">&nbsp;</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    // if data available 
                                ?>                                      
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php // loop all data ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                        /*
                                         * following condition use to change odd rows and even rows
                                         * to do that, varaible i set to increment by 1 once a loop
                                         */                                         
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><?php echo $data->r_name; ?></td>
                                            <?php // only for admin users can see last 2 columns  ?>
                                            <?php if($user_type == 1){ ?>
                                            <td class="center">
                                                <a href="<?php echo base_url() ?>region/edit?id=<?php echo $data->id; ?>">
                                                  <span class="glyphicon glyphicon-pencil"></span>
                                                </a>                                               
                                            </td>
                                            <td class="center">
                                                    <!-- Modal -->
                                                    <?php // delete dialog box ?>
                                                    <div class="modal fade" id="<?php echo $data->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title" id="myModalLabel">Delete Region</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Are you sure? the record will be permanently deleted
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <button type="button" class="btn btn-danger" onclick="deleteRecord('<?php echo $data->id; ?>')">Delete</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>   
                                                    <?php // when click on this button, delete dialog box will popup ?>
                                                    <a data-toggle="modal" data-target="#<?php echo $data->id; ?>"  style="cursor: pointer;">
                                                      <span class="glyphicon glyphicon-remove"></span>
                                                    </a>   
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                        </div>            
            <!-- /.col-lg-12 -->
                </div>
           </div>
         </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
            /*
         * jquery noConflict remove conflicts among jquery functions
         * for remove conflicts use jQuery. instead of $ mark
         * jquery datatable use to show search, pagination and sort options
         * 
         * targets: [-1,-2] remove sort option from last two colomn
         * if not admin then columnDefs remove,
         * because they can not see last two column
         */        
    jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            <?php if($user_type == 1){ ?>
            columnDefs: [
               { orderable: false, targets: [-1,-2] }
            ]     
            <?php } ?>
        });
        /*
         * when click on delete button
         * that request redirected to delete method in controller(with id)
         */
        function deleteRecord(id){
            window.location = '<?php echo base_url() ?>region/delete?id='+id;
        }          
    </script>