<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit Country</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                       
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post">
                        <div class="form-group required"> 
                           <label class="control-label">Country name:</label> 
                             <input class="form-control" type="text" name="country_name" value="<?php echo $countryData[0]->name ?>" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('country_name'); ?>                       
                        </div>
                       <div class="form-group required"> 
                             <label class="control-label">Region:</label>
                             <?php
                                /**
                                 * get inserted all regions to select drop down
                                 * if allRegions array not empty
                                 * then loop array for create select dropdown
                                 */
                             ?>                     
                             <select name="region_id" class="form-control">
                                 <option value="">Select a Region</option>
                                 <?php if(!empty($allRegions)){ ?>
                                    <?php foreach($allRegions as $region){ ?>
                                        <?php
                                            /*
                                             * this code use to dipslay selected region
                                             * assign empty value to $select variable
                                             * check event db entered retion id in country table == retion id (all region list)
                                             * then assign selected="selected" to $select variable
                                             * add that select value to option element
                                             */
                                            $select = "";
                                            if($countryData[0]->region_id == $region->id){
                                                $select = ' selected="selected" ';
                                            }
                                        ?>
                                        <option <?php echo $select; ?> value="<?php echo $region->id; ?>"><?php echo $region->r_name; ?></option>
                                    <?php } ?>
                                 <?php } ?>
                             </select>
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('region_id'); ?>
                        </div>                         
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
