<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit User</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                      
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post">
                        <div class="form-group"> 
                             <label>Email:</label>
                             <input class="form-control" type="text" readonly="" value="<?php echo $userData[0]->user_email ?>" />
                        </div>                         
                       <div class="form-group"> 
                             <label>User Type:</label>
                             <select class="form-control" name="user_type">
                                 <?php foreach($userTypes as $key=>$type){ ?>
                                    <?php 
                                        /*
                                         * this code use to dipslay selected user type
                                         * assign empty value to $select variable
                                         * check event db entered user type in users table == controller usertype array id
                                         * then assign selected="selected" to $select variable
                                         * add that select value to option element
                                         */                                      
                                        $select = '';
                                        if($userData[0]->user_type == $key){ 
                                            $select = ' selected="selected" ';
                                        }
                                    ?>    
                                    <option <?php echo $select; ?> value="<?php echo $key; ?>"><?php echo $type; ?></option>
                                 <?php } ?>
                             </select>
                        </div> 
                        <div class="form-group required"> 
                             <label class="control-label">First name:</label>
                             <input class="form-control" type="text" name="first_name" value="<?php echo $userData[0]->first_name ?>" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('first_name'); ?>
                        </div>
                        <div class="form-group required"> 
                           <label class="control-label">Last name:</label> 
                             <input class="form-control" type="text" name="last_name" value="<?php echo $userData[0]->last_name ?>" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('last_name'); ?>                       
                        </div>
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
