<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Country</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post">
                       <div class="form-group required"> 
                             <label class="control-label">Country name:</label>
                             <input class="form-control" type="text" name="country_name" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('country_name'); ?>
                        </div>
                       <div class="form-group required"> 
                             <label class="control-label">Region:</label>
                             <?php
                             /**
                              * get inserted all regions to select drop down
                              * if allRegions array not empty
                              * then loop array for create select dropdown
                              */
                             ?>
                             <select name="region_id" class="form-control">
                                 <option value="">Select a Region</option>
                                 <?php if(!empty($allRegions)){ ?>
                                    <?php foreach($allRegions as $region){ ?>
                                        <option value="<?php echo $region->id; ?>"><?php echo $region->r_name; ?></option>
                                    <?php } ?>
                                 <?php } ?>
                             </select>
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('region_id'); ?>
                        </div>                        
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Country"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
