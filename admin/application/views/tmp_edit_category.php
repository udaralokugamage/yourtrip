<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Edit 
                    <?php if(isset($_GET['cat_id'])){ ?>
                    Category
                    <?php } else { ?>
                    Subcategory
                    <?php } ?>
                </h1>
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post">
                       <div class="form-group"> 
                             <label>Category name:</label>
                             <input class="form-control" type="text" name="category_name"
                                    <?php if(isset($_GET['cat_id'])){ ?>
                                        value="<?php echo $catData[0]->category_name ?>"
                                    <?php } else { ?>
                                        value="<?php echo $catData[0]->subcategory_name ?>"
                                    <?php } ?>
                                    />
                             <?php echo form_error('category_name'); ?>
                        </div>
                        <?php if(!isset($_GET['cat_id'])){ ?>
                        <div class="form-group"> 
                            <label>Parent category:</label>
                            <select class="form-control" name="parent_category" class="">
                                <option value="0">--- None ---</option>
                                
                                <?php if(!empty($parentCategories)){ ?>
                                    <?php foreach($parentCategories as $category){ ?>
                                        <?php
                                            $select = '';
                                            if($catData[0]->category_id == $category->category_id){
                                                $select = ' selected="" ';
                                            }
                                        ?>
                                         <option <?php echo $select ?> value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>;
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php echo form_error('category_code'); ?>
                       </div>
                        <?php }else{ ?>
                            <input type="hidden" name="parent_category" value="0"/>
                        <?php } ?>
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update Category"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
