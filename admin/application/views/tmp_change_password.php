<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Change Password</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                  
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post">
                       <div class="form-group required"> 
                             <label class="control-label">Old Password:</label>
                             <input class="form-control" type="password" name="old_password" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('old_password'); ?>
                        </div>
                       <div class="form-group required"> 
                             <label class="control-label">New Password:</label>
                             <input class="form-control" type="password" name="new_password" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('new_password'); ?>
                        </div>                        
                        <div class="form-group required"> 
                             <label class="control-label">Confirm New Password:</label>
                             <input class="form-control" type="password" name="confirm_new_password" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('confirm_new_password'); ?>
                        </div>  
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Update Password"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
