<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="page-header">Reviews On <?php echo $package[0]->package_name; ?> </h2>
      </div>
    </div>

    <div class="col-lg-12">
      <div class="panel-body">
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-lg-4">
            <select name="packages" id="package_selection" class="form-control" onchange="change_package();">
              <option>select package </option>
              <?php foreach ($packages as $package) { ?>
                <option value="<?php echo $package->package_id; ?>"><?php echo $package->package_name; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
          <thead>
            <tr>
              <th>ID</th>
              <th>Comment</th>
              <th>Stars</th>
              <th>User</th>
              <th>Visible</th>
              <th>Delete</th>
            </tr>
          </thead>
          <?php foreach ($reviews as $review) { ?>
            <tr>
              <td><?php echo $review['id'] ?></td>
              <td><?php echo $review['comment'] ?></td>
              <td><?php echo $review['stars'] ?></td>
              <td><?php echo $review['fname'].' '.$review['lname'] ?></td>
              <td>
                <input <?php echo ($review['visible']) ? 'checked' : ''; ?> data-toggle="toggle" type="checkbox" value="<?php echo $review['visible']?>" onchange="change_status(<?php echo $review['id'];?>, <?php echo $review['visible'];?>);" id="<?php echo $review['id']?>_tog">
                <label hidden="true"><?php echo $review['visible']; ?></label>
              </td>
              <td>
                <a data-toggle="modal" data-target="#<?php echo $review['id']; ?>_mod"  style="cursor: pointer;">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>

                <div class="modal fade" id="<?php echo $review['id']; ?>_mod" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete Package</h4>
                      </div>
                      <div class="modal-body">
                        Are you sure? the record will be permanently deleted
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" onclick="deleteRecord(<?php echo $review['id']; ?>, <?php echo $review['package_id']; ?>)">Delete</button>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php } ?>          
          <tbody>
          </tbody>
        </table>
      </div>            
    </div>
  </div>
</div>

<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">

<script>
  jQuery.noConflict();
  jQuery('#dataTables-example').DataTable({
    responsive: true,
    columnDefs: [
      { orderable: false, targets: [-1] }
    ]            
  });
</script>

<script type="text/javascript">
  function change_status(id, visible) {
    var status = (visible == 1 ? 0 : 1);
    var element_id = "#"+id+"_tog";
    var update_url = "<?php echo base_url();?>/reviews/update_status/"+id+"/"+status;
    jQuery.ajax({
      type:"POST",
      url: update_url,
      success: function(res){
        if(res){
          $(element_id).prop('checked', status).change();
        }      
      }
    }); 
  }

  function change_package() {
    var package_id = $("#package_selection").val();
    window.location = "<?php echo base_url() ?>reviews/"+package_id;
  }

  function deleteRecord(id, package_id){
    window.location = "<?php echo base_url() ?>reviews/delete/"+package_id+"/"+id;
  }
</script>

<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>


