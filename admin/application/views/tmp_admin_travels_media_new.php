<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="page-header"> Travelling Media to <?php echo $travel[0]['title']?></h2>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="panel-body">

          <?php echo form_open_multipart('travels/create_travel_media'); ?>
          <form style="margin-bottom: 20px;">
            <div class="form-group">
              <label>Upload Media File</label>
              <input type="file" name="userfile"/>
            </div>
            <input type="input" id="travel_id" name="travel_id" hidden="true" value="<?php echo $travel[0]['id']; ?>" />

            <button type="submit" name="submit" class="btn btn-primary">Submit</button>  
          </form>
        </div>            
      </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
      <div class="col-lg-12">
        <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <?php foreach ($travel_images as $image) { ?>
              <tr>
                <td><?php echo $image['id'] ?></td>
                <td><?php echo $image['name'] ?></td>
                <td>
                  <a data-toggle="modal" data-target="#<?php echo $image['id']; ?>_mod"  style="cursor: pointer;">
                    <span class="glyphicon glyphicon-remove"></span>
                  </a>

                  <div class="modal fade" id="<?php echo $image['id']; ?>_mod" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="myModalLabel">Delete Package</h4>
                        </div>
                        <div class="modal-body">
                          Are you sure? the record will be permanently deleted
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-danger" onclick="deleteRecord(<?php echo $image['id']; ?>, <?php echo $travel[0]['id']; ?>)">Delete</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>            
      </div>
    </div>

  </div>
</div>

<!-- Bootstrap Core JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>

<script>
  jQuery.noConflict();
  jQuery('#dataTables-example').DataTable({
    responsive: true,
    columnDefs: [
      { orderable: false, targets: [-1] }
    ]            
  });

  function deleteRecord(id, travel_id){
    window.location = "<?php echo base_url() ?>travel/media/delete/"+id+"/"+travel_id;
  }
</script>


