<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Approve Packages</h1>
            </div>
        </div>
                <div class="col-lg-12">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Package Name</th>
                                        <th>Region > Country</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    // if approval need packages avaialbe ( if not empty alldata array)
                                ?>
                                <?php if(!empty($allData)){ $i = 1; ?>
                                    <?php // loop all data ?>
                                    <?php foreach($allData as $data){ ?>
                                        <?php 
                                        /*
                                         * following condition use to change odd rows and even rows
                                         * to do that, varaible i set to increment by 1 once a loop
                                         */
                                            if($i%2 == 1){
                                                $cl = "odd";
                                            }else{
                                                $cl = "even";
                                            }
                                        ?>
                                        <tr class="<?php echo $cl ?> gradeX">
                                            <td><?php echo $data->package_name; ?></td>
                                            <td><?php echo $data->category_name; ?></td>
                                            <td><?php echo $data->created_user; ?></td>
                                            <td class="center">
                                                <?php // status 1 or 0 keep hide for sorting purpose  ?>
                                                <span style="display: none;"><?php echo $data->status; ?></span>
                                                <?php 
                                                    /**
                                                     * if status == 1 means package enable 
                                                     * then assign ckecked="" to check box
                                                     */
                                                    if($data->status == 1){ 
                                                        $check = ' checked="" ';
                                                    }else{
                                                        $check = '';
                                                    }
                                                ?>
                                                <input <?php echo $check; ?> type="checkbox" class="statustrack" value="<?php echo $data->package_id; ?>" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
                                            </td>
                                        </tr>
                                    <?php $i++; } ?>
                                <?php } ?>    
                                 </tbody>
                            </table>
                        </div>            
            <!-- /.col-lg-12 -->
                </div>
           </div>
         </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
</div>


<!-- /#page-wrapper -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        /*
         * jquery noConflict remove conflicts among jquery functions
         * for remove conflicts use jQuery. instead of $ mark
         * jquery datatable use to show search, pagination and sort options
         * 
         * "order": [[ 3, "asc" ]] use to sort loading screen data using order status
         * order status 0 means not approved packages
         */
    jQuery.noConflict();
        jQuery('#dataTables-example').DataTable({
            responsive: true,
            columnDefs: [
               { orderable: false, targets: [] }
            ],
            "order": [[ 3, "asc" ]]
        });
        /*
         * change package status usuing jquery ajax request 
         * request send to paclages controller, status method
         * send package id which is want to change status
         */
        jQuery('.statustrack').change(function() {
           var id = $(this).val();
           if($(this).prop('checked')){
                $.post("<?php echo base_url() ?>package/status?st=1", {pack_id: id}, function(result){
                });               
           }else{
                $.post("<?php echo base_url() ?>package/status?st=0", {pack_id: id}, function(result){
                });                 
           }
        });
    </script>
<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>

</script>
