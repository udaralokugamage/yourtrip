<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add User</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                      
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                
                    <form action="" method="post">
                       <div class="form-group"> 
                             <label>User Type:</label>
                             <?php 
                             /*
                              * loop all user types
                              */
                             ?>
                             <select class="form-control" name="user_type">
                                 <?php foreach($userTypes as $key=>$type){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $type; ?></option>
                                 <?php } ?>
                             </select>
                        </div>                        
                       <div class="form-group required"> 
                             <label class="control-label">First name:</label>
                             <input class="form-control" type="text" name="first_name" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('first_name'); ?>
                        </div>
                       <div class="form-group required"> 
                           <label class="control-label">Last name:</label> 
                             <input class="form-control" type="text" name="last_name" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('last_name'); ?>                       
                        </div>
                        <div class="form-group required"> 
                           <label class="control-label">Email:</label> 
                             <input class="form-control" type="text" name="email" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('email'); ?>                       
                        </div>  
                        <div class="form-group required"> 
                           <label class="control-label">Password:</label> 
                           <input class="form-control" autocomplete="off" type="password" name="password" />
                           <?php // if validation fail then display form error ?>
                            <?php echo form_error('password'); ?>                       
                        </div>                                                
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add User"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
