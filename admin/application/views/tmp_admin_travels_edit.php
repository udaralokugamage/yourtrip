<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="page-header"> Add Travelling Place </h2>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="panel-body">

        <?php echo form_open('travels/update_travel'); ?>

        <form style="margin-bottom: 20px;">

          <div class="form-group required">
            <label class="control-label">Title :</label>
            <input class="form-control" name="title" value="<?php echo $travel[0]['title']; ?>">
            <?php echo form_error('title'); ?>
          </div>

          <div class="form-group required">
            <label class="control-label">Description :</label>
            <textarea class="form-control" type="text" name="description"><?php echo $travel[0]['description']; ?></textarea>
            <?php echo form_error('description'); ?>
          </div>

          <div class="form-group required">
            <label class="control-label">Location :</label>
          </div>
          <div id="map" style="width:auto;height:400px;background:yellow;margin-bottom:20px;"></div>
          <!-- hidden location -->
          <input type="input" id="lat" name="lat" hidden="true" value="<?php echo $travel[0]['lat']; ?>" />
          <input type="input" id="longt" name="longt" hidden="true" value="<?php echo $travel[0]['longt']; ?>" />
          <!--  -->

          <!-- hidden id -->
          <input type="input" id="travel_id" name="travel_id" hidden="true" value="<?php echo $travel[0]['id']; ?>" />
          <!--  -->

          <button type="submit" name="submit" class="btn btn-primary">Update</button>  
        </form>
      </div>            
    </div>
    <div class="col-lg-6">
      <div class="panel-body">

        <?php echo form_open('travels/create_travel_package'); ?>

        <form>
          <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-12">
              <div class="form-group">
                <label class="control-label">Packages :</label>
                <select name="packages" id="package_selection" class="form-control" onchange="change_package();">
                  <option>select package </option>
                  <?php foreach ($packages as $package) { ?>
                    <option value="<?php echo $package['package_id']; ?>"><?php echo $package['package_name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>

          <!-- hidden package id -->
          <input type="input" id="travel_package_id" name="travel_package_id" value="1"  hidden="true"/>
          <!--  -->

          <!-- hidden travel id -->
          <input type="input" id="travel_travel_id" name="travel_travel_id"  value="<?php echo $travel[0]['id']; ?>"  hidden="true"/>
          <!--  -->

          <button type="submit" name="submit" class="btn btn-primary">Update</button>

        </form>

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example" style="margin-top: 20px;">
          <thead>
            <tr>
              <th>Package</th>
              <th>Remove</th>
            </tr>
          </thead>
          <?php foreach ($travel_packages as $travel_package) { ?>
            <tr>
              <td><?php echo $travel_package['package_name'] ?></td>
              <td>
                <a data-toggle="modal" data-target="#<?php echo $travel_package['id']; ?>_mod"  style="cursor: pointer;">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>

                <div class="modal fade" id="<?php echo $travel_package['id']; ?>_mod" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete Package</h4>
                      </div>
                      <div class="modal-body">
                        Are you sure? the record will be permanently deleted
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" onclick="deleteRecord(<?php echo $travel_package['id']; ?>, <?php echo $travel[0]['id']; ?>)">Delete</button>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php } ?>          
          <tbody>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>

<script>
function myMap() {
  var mapOptions = {
      center: new google.maps.LatLng(<?php echo $travel[0]['lat']; ?>, <?php echo $travel[0]['longt']; ?>),
      zoom: 10
  }
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var previous_marker = new google.maps.Marker({position: {lat: <?php echo $travel[0]['lat']; ?>, lng: <?php echo $travel[0]['longt']; ?>}, map: map});
  previous_marker.setMap(map);

  google.maps.event.addListener(map, 'click', function( event ){
    previous_marker.setMap(null);
    marker = new google.maps.Marker({position: event.latLng, map: map});
    marker.setMap(map);
    previous_marker = marker;

    $("#lat").val(event.latLng.lat());
    $("#longt").val(event.latLng.lng());
  });
}

function change_package() {
  var package_id = $("#package_selection").val();
  $("#travel_package_id").val(package_id);
}

function deleteRecord(id, travel_id){
  window.location = "<?php echo base_url() ?>travel/package/delete/"+id+"/"+travel_id;
}

</script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGfL40sospUmLPHgT3GqxilpKtohlA5LY&callback=myMap"></script>


<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
