<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="page-header"> Add Travelling Place </h2>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="panel-body">

        <?php echo form_open('travels/create_travel'); ?>

        <form style="margin-bottom: 20px;">

          <div class="form-group required">
            <label class="control-label">Title :</label>
            <input class="form-control" name="title"></textarea>
            <?php echo form_error('title'); ?>
          </div>

          <div class="form-group required">
            <label class="control-label">Description :</label>
            <textarea class="form-control" type="text" name="description"></textarea>
            <?php echo form_error('description'); ?>
          </div>

          <div class="form-group required">
            <label class="control-label">Location :</label>
          </div>
          <div id="map" style="width:auto;height:400px;background:yellow;margin-bottom:20px;"></div>
          <!-- hidden location -->
          <input type="input" id="lat" name="lat" hidden="true" />
          <input type="input" id="longt" name="longt" hidden="true" />
          <!--  -->

          <button type="submit" name="submit" class="btn btn-primary">Next</button>  
        </form>
      </div>            
    </div>
  </div>
</div>

<script>
function myMap() {
  var mapOptions = {
      center: new google.maps.LatLng(51.5, -0.12),
      zoom: 10
  }
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var previous_marker = new google.maps.Marker();

  google.maps.event.addListener(map, 'click', function( event ){
    previous_marker.setMap(null);
    marker = new google.maps.Marker({position: event.latLng, map: map});
    marker.setMap(map);
    previous_marker = marker;

    $("#lat").val(event.latLng.lat());
    $("#longt").val(event.latLng.lng());
  });
}

function chng_package() {
    $("#package-id").val($("#package_selection").val());
  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGfL40sospUmLPHgT3GqxilpKtohlA5LY&callback=myMap"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

<link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>


