<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Category</h1>
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post">
                       <div class="form-group"> 
                             <label>Category name:</label>
                             <input class="form-control" type="text" name="category_name" />
                             <?php echo form_error('category_name'); ?>
                        </div>
                        <div class="form-group"> 
                            <label>Parent category:</label>
                            <select class="form-control" name="parent_category" class="">
                                <option value="0">--- None ---</option>
                                
                                <?php if(!empty($parentCategories)){ ?>
                                    <?php foreach($parentCategories as $category){ ?>
                                         <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php echo form_error('category_code'); ?>
                       </div>
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Category"/>
                        </div>
                   </form>                  
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
