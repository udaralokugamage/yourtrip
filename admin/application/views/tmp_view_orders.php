<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Bookings</h1>
            </div>
            <div class="panel-body">
                <form id="statusChangeFrom" action="" method="post">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <td colspan="7">
                                    <select class="form-control" name="status">
                                        <option value="">Change Order Status</option>
                                        <option value="0">Pending</option>
                                        <option value="2">Cancel</option>
                                        <option value="1">Success</option>
                                    </select> 
                                    <input type="hidden" name="update" value="set"/>
                                    <input id="statusChangeBookings" class="form-control btn-primary" type="button" name="update" value="Change Status"/>
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Order Date</th>
                                <th>Customer</th>
                                <th>Customer Email</th>
                                <th>Package Name</th>
                                <th>Order Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                // if data available 
                            ?>                              
                            <?php if (!empty($allData)) {
                                $i = 1; ?>
                                <?php // loop all data ?>
                                <?php foreach ($allData as $data) { ?>
                                    <?php
                                    /*
                                     * following condition use to change odd rows and even rows
                                     * to do that, varaible i set to increment by 1 once a loop
                                     */                                     
                                    if ($i % 2 == 1) {
                                        $cl = "odd";
                                    } else {
                                        $cl = "even";
                                    }
                                    ?>
                                    <tr class="<?php echo $cl ?> gradeX">
                                        <?php
                                            /*
                                             * checkbox name write as a array 
                                             * beasue then user can change status of more than one packages
                                             */
                                        ?>
                                        <td><input type="checkbox" name="order_status[]" value="<?php echo $data->bid; ?>"/></td>
                                        <td><?php echo $data->orderdate; ?></td>
                                        <td><?php echo $data->customer_name; ?></td>
                                        <td><?php echo $data->customer_email; ?></td>
                                        <td>
                                            <?php
                                                //  when click on this link package detail dialog box popup
                                            ?>
                                            <a data-toggle="modal" data-target="#<?php echo $data->package_id."_".$data->bid; ?>"  style="cursor: pointer;">
                                                <?php echo $data->package_name; ?>
                                            </a> 
                                            <?php 
                                                /*
                                                 * package detail view dialog box 
                                                 * for create unique id for these dialog box - Used package id and booking id
                                                 * line number 75 id and line number 65 data-target
                                                 */
                                            ?>                        
                                            <div class="modal fade" id="<?php echo $data->package_id."_".$data->bid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel">Package Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <strong>Package name:</strong> <?php echo $data->package_name; ?> <br/><br/>
                                                            <strong>Description:</strong> <?php echo $data->description; ?> <br/><br/>
                                                            <strong>Detailed itinerary:</strong> <?php echo $data->detailed_itinerary; ?> <br/><br/>
                                                            <strong>Number of days:</strong> <?php echo $data->fixed_no_of_days; ?> <br/>
                                                            <strong>Region:</strong> <?php echo $data->region; ?> <br/>
                                                            <strong>Country:</strong> <?php echo $data->country; ?> <br/>
                                                            <?php if($data->deleted == 1){ ?>
                                                                <strong>Package status:</strong> Deleted <br/>
                                                            <?php  }else{ ?>
                                                                <?php if($data->pack_status == 1){ ?>
                                                                    <strong>Package status:</strong> Enable <br/>
                                                                <?php } else { ?>
                                                                    <strong>Package status:</strong> Disable 
                                                                 <?php } ?>   
                                                            <?php } ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>                                        
                                        </td>
                                        <td>
                                            <?php
                                            if ($data->status == 1) {
                                                echo "Success";
                                            } else if ($data->status == 0) {
                                                echo "Pending";
                                            } else {
                                                echo "Cancel";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php // dialog box for extra informations ?>
                                            <div class="modal fade" id="<?php echo $data->bid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel">Booking Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Number of adults: <?php echo $data->no_of_adults; ?> <br/>
                                                            Adults total: $<?php echo $data->adults_total; ?> <br/>
                                                            Number of children: <?php echo $data->no_of_children; ?> <br/>
                                                            Children total: $<?php echo $data->child_total; ?> <br/>
                                                            Start Date: <?php echo $data->start_date; ?> <br/>
                                                            Subtotal: $<?php echo $data->sub_total; ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>    
                                            <?php // extra information dialog box will popup when click on this link ?>
                                            <a data-toggle="modal" data-target="#<?php echo $data->bid; ?>"  style="cursor: pointer;">
                                                More Details
                                            </a>                                                 
                                        </td>
                                    </tr>
                                    <?php $i++;
                                } ?>
<?php } ?>    
                        </tbody>
                    </table>
                </form>
            </div>            
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

<!-- /#page-wrapper -->
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    /*
   * jquery noConflict remove conflicts among jquery functions
   * for remove conflicts use jQuery. instead of $ mark
   * jquery datatable use to show search, pagination and sort options
   * 
   * targets: [-1,-2] remove sort option from last two colomn
   */     
    jQuery.noConflict();
    jQuery('#dataTables-example').DataTable({
        responsive: true,
        columnDefs: [
            {orderable: false, targets: [-1, -2]}
        ]
    });
    /*
     * when press status change button (line 21) 
     * as a first step check atlease one checkbox is selected
     * if not give error message
     * otherwise submit form
     */
    jQuery("#statusChangeBookings").click(function(event) {
        checked = jQuery("input[type=checkbox]:checked").length;
        if (checked) {
            jQuery('#statusChangeFrom').submit();
        } else {
            alert("Please select an item");
            return false;
        }
    });


</script>