<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Add Package</h1>
                    <?php
                        /**
                         * if controller set $this->data['msg']
                         * then that will display here
                         * success msg or fail msg
                         */
                    ?>                  
                    <?php if(isset($msg)){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <?php echo $msg; ?>
                            </div>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                       <div class="form-group required"> 
                             <label class="control-label">Package Name:</label>
                             <input class="form-control" type="text" name="package_name" />
                             <?php // if validation fail then display form error ?>
                             <?php echo form_error('package_name'); ?>
                        </div>
                        <div class="form-group required"> 
                            <label class="control-label">Number of days:</label>
                             <input class="form-control" type="text" name="fixed_no_of_days" />
                             <?php // if validation fail then display form error ?>
                            <?php echo form_error('fixed_no_of_days'); ?>
                       </div>                        
                        <div class="form-group required"> 
                            <label class="control-label">Description:</label>
                            <textarea class="form-control" name="description"></textarea>
                            <?php // if validation fail then display form error ?>
                            <?php echo form_error('description'); ?>
                       </div>
                       <div class="form-group required"> 
                           <label class="control-label">Detailed Itinerary:</label> 
                            <textarea class="form-control" name="detailed_itinerary"></textarea> 
                       </div>
                       <div class="form-group required"> 
                           <label class="control-label">Price per adult:</label> 
                           <div class="input-group">
                               <span class="input-group-addon">$</span>                           
                               <input type="text" class="form-control" name="price_adult"/>
                           </div>   
                           <?php // if validation fail then display form error ?>
                           <?php echo form_error('price_adult'); ?>
                       </div>
                       <div class="form-group"> 
                           <label>price per child:</label> 
                           <div class="input-group">
                               <span class="input-group-addon">$</span>                           
                               <input type="text" class="form-control" name="price_child"/>
                           </div>    
                           <?php // if validation fail then display form error ?>
                           <?php echo form_error('price_child'); ?>
                       </div>                        
                       <div class="form-group required"> 
                           <label class="control-label">Region:</label> 
                             <?php
                             /**
                              * get inserted all regions to select drop down
                              * if allRegions array not empty
                              * then loop array for create select dropdown
                              */
                             ?>                           
                           <select class="form-control" id="region_id" name="region_id">
                                <option value="">Select a Region</option>
                                <?php if(!empty($regions)){ ?>
                                    <?php foreach($regions as $region){ ?>
                                         <option value="<?php echo $region->id; ?>"><?php echo $region->r_name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                       </div>
                        <?php
                            /**
                             * based on selected region, country list is loading using jquery ajax request
                             * line numbers 125 to 141 code send ajax request to controller ( package controller,countries method)
                             * region id send to countries method. based on that region id
                             * select all countries belong to that region
                             * while loading country list 
                             * line number 92 will run and show loading image
                             * 
                             * If someone going to select country without selecting a region then
                             * code numbers 143 to 155 will run and display 
                             * "Please select a region first to retrieve its related contries" message
                             */
                        ?>
                        <i id="loading" class="fa fa-spinner fa-spin" style="font-size:24px; color: #337AB7; display: none;"></i>
                        <div class="form-group required" id="subbox"> 
                           <label class="control-label">Country:</label> 
                           <select class="form-control" id="country_id" name="country_id">
                                <option value="">Select a Country</option>
                            </select>
                       </div>                        
                       <div class="form-group">
                           <label>Thumbnail Image<i>(For view all packages page)</i></label>
                           <input type="file" name="thumb_image"/>
                        </div>
                       <div class="form-group">
                           <label>Banner Image<i>(For package detail page)</i></label>
                           <input type="file" name="banner_image"/>
                        </div>                        
                        <div class="form-group"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Add Package"/>
                        </div>
                       
                       
                   </form>                   
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>


<!-- /#page-wrapper -->
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script>
/*
 * track region id change event
 * show loading image (diplay visible)
 * get selected region value from region id
 * post region id via jquery post request to package controller, countries method
 * display contries list on #subbox element
 * loading image hide
 */

$("#region_id").change(function(){
    $('#loading').show();
    var id = $("#region_id").val();
    $.post("<?php echo base_url() ?>package/countries", {sub_id: id}, function(result){
        $("#subbox").html(result);
        $('#loading').hide();
    });
});

/**
 * track click event of country select dropdown
 * get region value from region id
 * if retion value empty(region not selected)
 * then disply alert message
 */
$('body').on('click','#country_id',function(){
    var id = $("#region_id").val();
    if(!id){
       alert('Please select a region first to retrieve its related contries.'); 
    }
    id = "";
});
</script>
