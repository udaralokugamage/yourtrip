<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Manage Photo Galleries</h1>
                <?php
                    /**
                     * if controller set $this->data['msg']
                     * then that will display here
                     * success msg or fail msg
                     */
                ?>                  
                <?php if (isset($msg)) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <?php echo $msg; ?>
                    </div>
                <?php } ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group required col-lg-6"> 
                        <label class="control-label">Package:</label>
                        <i id="loading" class="fa fa-spinner fa-spin" style="font-size:24px; color: #337AB7; display: none;"></i>
                        <select id="package_id" name="package_id" class="form-control" >
                            <option value="">Select a Package</option>
                            <?php if (!empty($allData)) { ?>
                                <?php foreach ($allData as $data) { ?>
                                    <?php
                                    /*
                                     * this code use to dipslay selected package name                                     * assign empty value to $select variable
                                     * when hotel selected from dropdown, 
                                     * using jqury add that package id to query string(to page url)
                                     * then read the page url and read $_GET['id']
                                     * if $_GET['id'] match to data hotel id then it is considered as equal
                                     * add that select value to option element
                                     */                                      
                                    $select = "";
                                    if (isset($_GET['id'])) {
                                        if ($_GET['id'] == $data->package_id) {
                                            $select = ' selected="selected" ';
                                        }
                                    }
                                    ?>
                                    <option <?php echo $select; ?> value="<?php echo $data->package_id; ?>"><?php echo $data->package_name; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php // if validation fail then display form error ?>
                        <?php echo form_error('package_id'); ?>
                    </div>

                    <?php if (isset($_GET['id'])) { ?>
                        <div class="form-group col-lg-12">
                            <label>Upload Photos</label>
                            <?php 
                            /*
                             * input file name as banners[]
                             * this [] use to create array name.
                             * because multiple images will be uploaded once
                             */
                            ?>                            
                            <input type="file" name="banners[]" multiple=""/>
                        </div>
                        <div class="form-group col-lg-12"> 
                            <input class="btn btn-success" type="submit" name="submit" value="Upload"/>
                        </div>                        
                    <?php } ?>
                </form>

                <?php if (isset($_GET['id'])) { ?>
                    <?php if (empty($packageGallery)) { ?>
                        <div class="alert alert-warning col-lg-12">
                            No Photos
                        </div>                            
                    <?php } else { ?>
                        <form id="trashForm" action="<?php echo base_url(); ?>package/deleteImg?id=<?php echo $_GET['id']; ?>" method="post">
                            <div class="form-group col-lg-12"> 
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php // checkbox for select all images ?>
                                        <input type="checkbox" id="checkAll" name="selectAll" value="all"> 
                                        &nbsp;&nbsp;
                                        <?php // when click on this button, delete comfirm dialog box will apear. ?>
                                        <button data-toggle="modal" data-target="#trashbox" type="submit" id="trashbtn" class="btn btn-default btn-sm">
                                            <span class="glyphicon glyphicon-trash"></span> Trash 
                                        </button>  
                                    </div>
                                    <div class="panel-body">
                                        <?php // loap all images ?>
                                        <?php foreach ($packageGallery as $photo) { ?>
                                            <div class="imgborder"> 
                                                <?php 
                                                    /*
                                                     * each image has check box to select that image
                                                     * that checkbox name is deleteimg[]
                                                     * that [] represent array
                                                     * beacuse multiple images can be selected
                                                     */
                                                ?>                                                
                                                <input type="checkbox" name="deleteimg[]" value="<?php echo $photo->image_id; ?>"/> <br/>
                                                <a class="example-image-link" href="<?php echo base_url() . 'uploads/gallery/' . $photo->file_name; ?>" data-lightbox="example-set" data-title="">
                                                    <img class="example-image" src="<?php echo base_url() . 'uploads/gallery/' . $photo->thumb_image; ?>"/> 
                                                </a>
                                            </div>                         
                                        <?php } ?>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    <?php } ?>
                <?php } ?>

            </div>
        </div>
    </div>
</div>  
<!--- trash dialog box --->
<div class="modal fade" id="trashbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Photo(s)</h4>
            </div>
            <div class="modal-body">
               Do you want to delete the selected photo(s)?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" onclick="deleteImges()" >Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>
<script>
    /*
     * when select dropdown changed
     * that selected option id get using jquery val
     * if id is set, then window redirected to same page with that id
     * other wise window will be reloaded again.
     */    
    $("#package_id").change(function () {
        $('#loading').show();
        var id = $("#package_id").val();
        if (id) {
            window.location = '<?php echo base_url(); ?>package/gallery?id=' + id;
        } else {
            window.location = '<?php echo base_url(); ?>package/gallery';
        }
    });
    /*
    * when click on line 80 checkall checkbox, then all input checkboxes will checked or unchecked  
     */    
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    /*
    * when click on trash button if one or more checkboxs are selected then form will submit
    * otherwise form will not submit
    */    
    $("#trashbtn").click(function (event) {
        event.preventDefault();
        checked = $("input[type=checkbox]:checked").length;
        if(checked) {
        }else{
          alert("Please select an item");
          return false;
        }
    });
     // delete confirmation box
     // when click on delete buttion then submit the form.      
    function deleteImges(){
        $('#trashForm').submit();
    }  
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist2/css/lightbox.min.css">
<script src="<?php echo base_url(); ?>assets/dist2/js/lightbox-plus-jquery.min.js"></script>