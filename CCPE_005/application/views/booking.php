


<div style="background-image: url(<?php echo base_url(); ?>assets/uploads/images/Capture1.png);
   width:100%;height: 215px;margin-top:50px" class="jumbotron text-center">
</div>
        	
<div class="container-fluid" style="background-color: #fff">  
<div class="container">          
            
     
              <div class="row ">
               
                  <div class="container" style="border: 1px solid #eee;padding: 10px;border-radius: 6px ;margin-top: 50px;">
                            
                      <div class="row" style="margin: 20px">
                           <div class="form-content">
                                <h4>User Information</h4>                                                                   
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                         <label>Your First Name</label>
                                         <input id="first_name" name="first_name" class="form-control" value="<?php echo $user['fname']; ?>" type="text">       
                                         <input id="user_id" name="user_id" class="form-control" value="<?php echo $user['id']; ?>" type="hidden" >         
                                    </div>
                                    <div class="col-sm-6">
                                         <label>Your Last Name</label>
                                         <input id="last_name" name="last_name" class="form-control" value="<?php echo $user['lname']; ?>" type="text">                                                                     
                                    </div>
                                </div>
                                <br>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                         <label>Your Email</label>
                                         <input id="email" name="email" class="form-control" value="<?php echo $user['email']; ?>" type="text">                                          
                                    </div>
                                    <div class="col-sm-6">
                                         <label>Your Contact No</label>
                                         <input id="contact" name="contact" class="form-control" value="<?php echo $user['contactNumber']; ?>" type="text" >                                                                     
                                    </div>
                                </div>
                           </div>      
                      </div>
                          
                  </div>
                
              </div> 


</div> 
</div>     

<div class="container-fluid" style="background-color: #f1f4f5;margin-bottom: 30px;margin-top: 30px">  
<div class="container"> 
    
              <div class="row" style="margin-top: 50px;">
                  <h4>PACKAGE SUMMARY</h4>
                  <h1 style="font-family: Roboto, sans-serif;text-transform: uppercase; font-size: 24px;color:#000 ;font-weight: 600"><?php echo $packages['package_name']; ?>.</h4>           
              </div>
              <br>
              <div class="row">
                 <br>
                <div class="row form-group">
                    <input name="package_id" class="form-control" value="<?php echo $packages['package_id']; ?>" type="hidden" >   
                                   <div class="col-sm-4">
                                        <label style="font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">Destination Region :</label> <label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"><?php echo $packages['region']; ?></label>
                                                                             
                                   </div>
                                   <div class="col-sm-4">
                                       <label style="font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">Destination Country :</label> <label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"><?php echo $packages['country']; ?></label>                                                                  
                                   </div>
                                   <div class="col-sm-4">
                                       <label style="font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">No of dates :</label> <label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"><?php echo $packages['fixed_no_of_days']; ?> Days</label>                                                                  
                                   </div>
                </div>
                <div class="row form-group">                                  
                                   <div class="col-sm-4">
                                       <label style="font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">Price for single person : </label> <label style="color: #ff4411; font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">$</label><label id="person" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"><?php echo $packages['price_adult']; ?></label>
                                        <br><label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">For : </label> <label id="no_ad" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">&nbsp0&nbsp</label>  <label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">person : $<?php echo $packages['price_adult']; ?>  X  </label> <label id="no_adu" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">&nbsp0&nbsp</label> <label  style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"> = $</label><label id="a_price" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">0</label>                                        
                                   </div>
                                   <div class="col-sm-4">
                                       <label style="font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">Price for one child : </label> <label style="color: #ff4411; font-size: 18px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">$</label><label id="child" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"><?php echo $packages['price_child']; ?></label>                                                                    
                                        <br><label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">For : </label> <label id="no_ch" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">&nbsp0&nbsp</label> <label style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">child : $<?php echo $packages['price_child']; ?>  X  </label> <label id="no_child" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">&nbsp0&nbsp</label> <label  style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;"> = $</label><label id="c_price" style="color: #ff4411; font-size: 16px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">0</label>    
                                   </div>
                                    <div class="col-sm-4">
                                        <label style="color: #ff4411; font-size: 26px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">SUB TOTAL : $</label><label id="sub_tot" style="color: #ff4411; font-size: 26px; font-family: 'Signika', sans-serif; padding-bottom: 10px;">0.00</label>
                                                                                                          
                                   </div>
                </div>
                <br>
              </div> 
 
</div> 
</div> 

<div class="container-fluid" style="background-color: #fff;margin-bottom: 60px">  
    
                  <div class="container" style="border: 1px solid #eee;padding: 10px;border-radius: 6px">
                            
                        <div class="row" style="margin: 20px">
                          
                            <h4>Booking Information</h4>
                                                            
                               <br>
                            
                                 <div class="row form-group">
                                     
                              <?php echo form_open('packages/booking_pack'); ?>                      
                                     <input type="hidden" name="Buid" id="Buid"  value="<?php echo $user['id']; ?>" />
                                     <input type="hidden" name="Bfname" id="Bfname"  value="<?php echo $user['fname']; ?>" />
                                     <input type="hidden" name="Blname" id="Blname"  value="<?php echo $user['lname']; ?>" />
                                     <input type="hidden" name="Bemail" id="Bemail"  value="<?php echo $user['email']; ?>" />
                                     
                                     <input type="hidden" name="Bpid" id="Bpid"  value="<?php echo $packages['package_id']; ?>" />
                                     <input type="hidden" name="Bpname" id="Bpname"  value="<?php echo $packages['package_name']; ?>" />
                                     <input type="hidden" name="Bregion" id="Bregion"  value="<?php echo $packages['region']; ?>" />
                                     <input type="hidden" name="Bcountry" id="Bcountry"  value="<?php echo $packages['country']; ?>" />
                                     <input type="hidden" name="Bdays" id="Bdays"  value="<?php echo $packages['fixed_no_of_days']; ?>" />
                                     <input type="hidden" name="priceC" id="priceC"  value="<?php echo $packages['price_child']; ?>" />
                                     <input type="hidden" name="priceP" id="priceP"  value="<?php echo $packages['price_adult']; ?>" />
                                    
                                     
                                     <input type="hidden" name="Bcontact" id="Bcontact"  value="1234567890" />
                                    <div class="col-sm-4">
                                        <label>Start date</label>
                                        <input id="s_date" name="start_day" class="form-control" type="date" >                                          
                                   </div>
                                   <div class="col-sm-4">
                                        <label>No of Adults</label>
                                        <input id="ad" min="0" name="adults" class="form-control" type="number" >                                          
                                   </div>
                                   <div class="col-sm-4">
                                       <label>No of Children</label>
                                       <input id="ch" min="0" name="Children" class="form-control" type="number" >                                                                     
                                   </div>
                                     <input type="hidden" name="adults_total" id="adults_total"  value="00.00" />
                                     <input type="hidden" name="child_total" id="child_total"  value="00.00" />
                                     <input type="hidden" name="sub_total" id="sub_total"  value="00.00" />
                                     
                                    
                                
                                 </div>
                                 <br>
                                    <div class=" text-right ">
                                    <button style="width: 225px" type="submit"  class="btn btn-primary"> BOOK NOW</button>
                                    </div>
                                 
                                  <?php echo form_close(); ?>   
                        </div>
              <?php echo validation_errors(); ?>
                   </div>
                      
</div>


<!--                             
<script> 
        $(document).ready(function(){
                  
            var ufname = $('#first_name').text().trim();
            var ulname = $('#last_name').text().trim();
            var ucon = $('#contact').text().trim();
            var umail = $('#email').text().trim();
            var uid = $('#user_id').text().trim();
           
            $("#Bfname").val(ufname);
            $("#Blname").val(ulname);
            $("#Bemail").val(umail);
            $("#Bcontact").val(ucon);
            $("#Buid").val(uid);                  
                  
        });
</script>-->

<script>
    
$("#ad").bind('keyup mouseup', function () {
    
    var adults = parseInt($('#ad').val(), 10);
    $('#no_ad').html(adults);
    $('#no_adu').html(adults);
    
    var number_adults = $('#no_ad').text().trim();
        
    var adultsPrice = $('#person').text().trim();
    
    var aa = parseFloat(adultsPrice).toFixed(2);
    var ss = parseFloat(number_adults).toFixed(2);
    var dd = aa * ss;
    var ff = parseFloat(dd).toFixed(2);

    $('#a_price').html(ff);
    
    var APriceTot = $('#a_price').text().trim();
    var CPriceTot = $('#c_price').text().trim();
    var total = parseFloat(APriceTot) + parseFloat(CPriceTot);
    $('#sub_tot').html(total); 
    
    $("#adults_total").val(parseFloat(APriceTot));
    $("#child_total").val(parseFloat(CPriceTot));
    $("#sub_total").val(total);
          
});

$("#ch").bind('keyup mouseup', function () {
    
    var children = parseInt($('#ch').val(), 10);
    $('#no_ch').html(children);
    $('#no_child').html(children);
    
    var number_children = $('#no_ch').text().trim();
    var childrenPrice = $('#child').text().trim();
   
    var qq = parseFloat(childrenPrice).toFixed(2);
    var ww = parseFloat(number_children).toFixed(2);
    var ee = qq * ww;
    var rr = parseFloat(ee).toFixed(2);
    
    $('#c_price').html(rr);
      
    var APriceTot = $('#a_price').text().trim();
    var CPriceTot = $('#c_price').text().trim();
    var total = parseFloat(APriceTot) + parseFloat(CPriceTot);
    $('#sub_tot').html(total); 
    
    $("#adults_total").val(parseFloat(APriceTot));
    $("#child_total").val(parseFloat(CPriceTot));
    $("#sub_total").val(total);
   //alert(total);
     
});


var today = new Date().toISOString().split('T')[0];
document.getElementsByName("start_day")[0].setAttribute('min', today);

</script>

 