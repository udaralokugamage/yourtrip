<html>
	<head>
		<title>Your trip</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom_style.css">
              
                <script src="<?php echo base_url(); ?>dist/sweetalert.min.js"></script>
                <link rel="stylesheet" href="<?php echo base_url(); ?>dist/sweetalert.css">
                
                <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
    @import url('https://fonts.googleapis.com/css?family=Lora');
    @import url('https://fonts.googleapis.com/css?family=Marvel');
    </style>
    <script src="https://use.fontawesome.com/639f40bdc5.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/crawler.js" type="text/javascript" ></script>
       
	</head>
	<body>
	<nav class="navbar navbar-default  navbar-fixed-top" style="background-color:#f2f2f2;font-color:#000;margin-bottom:0px">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>">Your trip</a>
        </div>
<!--        <div id="navbar">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="#">About</a></li>
             <li><a href="<?php echo base_url(); ?>index.php/home/travel">Travel with Us</a></li>
             <li><a href="#">Videos</a></li>
             <?php if ($this->session->userdata('login')){ ?>
		<li><a href="<?php echo base_url(); ?>index.php/home/history">My Bookings</a></li>
	     <?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <?php if ($this->session->userdata('login')){ ?>
        <li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname');?></p></li>
        <li><a href="<?php echo base_url(); ?>index.php/profile">Profile</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/home/logout">Log Out</a></li>
        <?php } else { ?>
        <li><a href="<?php echo base_url(); ?>index.php/login/">Login</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/signup/">Sign up</a></li>
        <?php } ?>
          </ul>
        </div>-->
             <div id="navbar">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
             <li><a href="<?php echo base_url(); ?>index.php/home/about">About</a></li>
             <li><a href="<?php echo base_url(); ?>index.php/home/travel">Travel with Us</a></li>
             <li><a href="#">Videos</a></li>
			 <?php if ($this->session->userdata('login')){ ?>
				<li><a href="<?php echo base_url(); ?>index.php/home/history">My Bookings</a></li>
			 <?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<?php if ($this->session->userdata('login')){ ?>
			<li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname');?></p></li>
			<li><a href="<?php echo base_url(); ?>index.php/profile">Profile</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/home/logout">Log Out</a></li>
			<?php } else { ?>
			<li><a href="<?php echo base_url(); ?>index.php/login/index">Login</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/signup/index">Sign up</a></li>
        <?php } ?>
          </ul>
        </div>
      </div>
    </nav>
    