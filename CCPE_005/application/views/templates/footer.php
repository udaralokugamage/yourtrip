      
  

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<br><hr><br>
<!--footer-->
<footer class="footer1">
<div class="container">

<div class="row"><!-- row -->
            
              
                                               
                <div class="col-lg-4 col-md-4"><!-- widgets column left -->
            
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                    <h4 style="margin-left: 50px" class="title-widget">YourTrip.lk</h4>

                                    <ul>
                                        <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#">  Sign up</a></li>
                                        <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#">  Login</a></li>
                                        <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#">  Packages</a></li>
                                        <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#">  Travel with US</a></li>                                  
                                        <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#">  My Profile</a></li>                                
                                    </ul>

				</li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                                               
                <div class="col-lg-4 col-md-4"><!-- widgets column left -->
            
                    <ul class="list-unstyled clear-margins"><!-- widgets -->

                        <li class="widget-container widget_nav_menu"><!-- widgets list -->

                            <h4 style="margin-left: 50px" class="title-widget">Help</h4>

                            <ul>

                                <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'"  href="#"> About us</a></li>
                                <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#"> Terms of use</a></li>
                                <li><a style="text-decoration: none;color:#444449" onMouseOut="this.style.color='#444449'" onMouseOver="this.style.color='#d9230f'" href="#"> Team</a></li>

                            </ul>

                        </li>

                    </ul>
                              
                </div><!-- widgets column left end -->
                                
                <div class="col-lg-4 col-md-4"><!-- widgets column center -->
                           
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_recent_news"><!-- widgets list -->

                                    <h4 class="title-widget">Contact Detail </h4>

                                    
                                   <a class="twitter-follow-button" href="#">
                                         Follow @YourTrip</a>   <br><br>
                                     
                                    <div class="footerp"> 

<!--                                        <p class="title-median"> GIG Interactive Pvt. Ltd.</p>-->
                                        <p><b>Email id:</b> <a href="mailto:info@yourtrip.com">info@yourtrip.com</a></p>

                                    </div>

                                    <div class="social-icons">
                                        <ul class="nomargin">

                                            <a href="#"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                                            <a href="#"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                                            <a href="#"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
                                            <a href="#"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>

                                        </ul>
                                    </div>
                            </li>
                            
                        </ul>
                    
                </div>

</div>
    
</div>
</footer>


<!--header-->

<div class="footer-bottom">

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <div class="copyright" style="text-align: center">

					© 2017 YourTrip.lk All rights reserved

				</div>

			</div>		

		</div>

	</div>

</div>
  

    </body>
</html>



    <script>
            marqueeInit({
               uniqueid: 'mycrawler2',
               style: {
                        
               },
               inc: 5, //speed - pixel increment for each iteration of this marquee's movement
               mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
               moveatleast: 2, 
               neutral: 1050,
               savedirection: true,
               random: true
            });
    </script>
    
    
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>

  </body>
</html>