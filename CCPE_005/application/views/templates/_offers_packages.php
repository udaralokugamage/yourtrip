
<style>
  @import url('https://fonts.googleapis.com/css?family=Lora');
  @import url('https://fonts.googleapis.com/css?family=Marvel');
  
  @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&subset=latin-ext,vietnamese');   
    
  body{
    font-family: 'Quicksand', sans-serif;
  }
  h4{
    font-weight: 600;  
  }
  p{
    font-size: 12px;
    margin-top: 5px;
  }
  .price{
    font-size: 30px;
      margin: 0 auto;
      color: #333;
      text-align: center;
  }
  .thumbnail{
    opacity:0.80;
    -webkit-transition: all 0.5s; 
    transition: all 0.5s;
  }
  .thumbnail:hover{
    opacity:1.00;
    box-shadow: 0px 0px 10px #cc0000;
  }
  .line{
    margin-bottom: 5px;
  }
  @media screen and (max-width: 770px) {
    .right{
      float:left;
      width: 100%;
    }
  }
  span.thumbnail {
        border: 1px solid #cc0000 !important;
        border-radius: 15px !important;
        -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
  padding: 10px;
    }
    .container h4{margin-top:30px; margin-bottom:15px;}
    button {    margin-top: 6px;
    }
    .right {
        float: right;
        border-bottom: 2px solid #0a5971;
    }
    .btn-info {
        color: #fff;
        background-color: #19b4e2;
        border-color: #19b4e2;
            font-size:13px;
            font-weight:600;
    }
  #recent_packages{
    margin-top: 5%;
  }



.carousel-control.left, .carousel-control.right{ 
    background: none !important;
    filter: progid:none !important;>
}


</style>

<?php if (!empty($offers)) { ?>

<div id="myCarousel2" class="carousel slide" data-ride="carousel">

  <?php  
    $set1 = array_slice($offers, 0, 3);
    $set2 = array_slice($offers, 3, 3);
  ?>
  <div class="carousel-inner">

    <div class="item active">
      <div class="row" id="recent_packages">
        <div id="all_packages" class="col-md-12">    
          <!-- BEGIN PRODUCTS -->
          <?php foreach($set1 as $package): ?> 
            <div class="col-md-4 package" onclick="toPackage(<?php echo $package['package_id'];?>);">
              <span class="thumbnail">
                <div class="parent" style="position: relative;top: 0;left: 0;">
                  <a href="#">
                    <img id="pimg" class="img-responsive" src="http://gig4.opendata.lk/travel/admin/uploads/packages/<?php echo $package['thumb_image']; ?>" alt="..." style="height:200px;width:260px;position: relative;top: 0;left: 0;border-radius:10px;">
                    <img id="" class="img-responsive" src="<?php echo base_url(); ?>assets/uploads/images/offer.gif" alt="" style="height:100px;width:170px;position: absolute;top: 40px;left: 50px;">
                  </a>
                </div>
                <div style="height:60px">
                  <h4 id="ptitle" style="text-transform: uppercase !important;text-align: center;color: black;">
                    <?php echo $package['package_name']; ?>
                  </h4>
                </div>

                <hr class="line">

                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <p class="price" style="color: #db4a1a">
                      $<?php echo $package['price_adult']; ?>
                    </p>                                       
                  </div>
                </div>

              </span>
            </div>
          <?php endforeach; ?> 
          <!-- END PRODUCTS -->
        </div>
      </div>
    </div>

    <?php if (!empty($set2)) { ?>
      <div class="item">
        <div class="row" id="recent_packages">
          <div id="all_packages" class="col-md-12">    
            <!-- BEGIN PRODUCTS -->
            <?php foreach($set2 as $package): ?> 
              <div class="col-md-4">
                <span class="thumbnail">
                  <a href="#">
                    <img id="pimg" class="img-responsive" src="http://gig4.opendata.lk/travel/admin/uploads/packages/<?php echo $package['thumb_image']; ?>" alt="..." style="height:200px;width:260px">
                  </a>

                  <div style="height:60px">
                    <h4 id="ptitle" style="text-transform: uppercase !important;text-align: center;color: black;">
                      <?php echo $package['package_name']; ?>
                    </h4>
                  </div>

                  <hr class="line">

                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <p class="price">
                        $<?php echo $package['price_adult']; ?>
                      </p>                                       
                    </div>
                  </div>

                </span>
              </div>
            <?php endforeach; ?> 
            <!-- END PRODUCTS -->
          </div>
        </div>
      </div>
    <?php } ?>

  </div>

  <a class="left carousel-control" href="#myCarousel2" data-slide="prev" style="margin-left: -10%;">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel2" data-slide="next" style="margin-right: -10%;border-color: #ffff;">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php } ?>

<script type="text/javascript">
  function toPackage(id) {
    window.location = "<?php echo base_url() ?>packages/"+id;
  }
</script>