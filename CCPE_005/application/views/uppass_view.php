

<div class="container" style="margin-top: 10%;">
  <div class="row">
    <div class="col-md-4 col-md-offset-4 well">
      <?php $attributes = array("name" => "uppwdform");
      echo form_open("uppass/index", $attributes);?>
      <legend style="text-align: center;">Update Password</legend>
      <div class="form-group">
        <label for="subject">Current Password</label>
        <input class="form-control" name="curpassword" placeholder="Password" type="password" />
        <span class="text-danger"><?php echo form_error('curpassword'); ?></span>
      </div>
      <div class="form-group">
        <label for="subject">New Password</label>
        <input class="form-control" name="password" placeholder="New Password" type="password" />
        <span class="text-danger"><?php echo form_error('password'); ?></span>
      </div>
      <div class="form-group">
        <label for="subject">Confirm Password</label>
        <input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" />
        <span class="text-danger"><?php echo form_error('cpassword'); ?></span>
      </div>
      <div class="form-group">
        <a name="cancel" href="<?php echo base_url(); ?>index.php/home/index" class="btn btn-info" >Cancel</a>
        <button name="submit" type="submit" class="btn btn-info" >Update</button>
      </div>
      <?php echo form_close(); ?>
      <?php echo $this->session->flashdata('msg'); ?>
    </div>
  </div>
