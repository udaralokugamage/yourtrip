<html>
	<head>
		<title>ciBlog</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom_style.css">
              
                <script src="<?php echo base_url(); ?>dist/sweetalert.min.js"></script>
                <link rel="stylesheet" href="<?php echo base_url(); ?>dist/sweetalert.css">
                
                <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
    @import url('https://fonts.googleapis.com/css?family=Lora');
    @import url('https://fonts.googleapis.com/css?family=Marvel');
    </style>
    <script src="https://use.fontawesome.com/639f40bdc5.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/crawler.js" type="text/javascript" ></script>
       
	</head>
	<body>
	
            
            <br> <br> <br> <br>

<div class="container-fluid" style="background-color: #fff;margin-bottom: 60px">  
    
                  <div class="container" style="border: 1px solid #eee;padding: 10px;border-radius: 6px">
                            
                        <div class="row" style="margin: 20px">
                          
                            <h1>Your Trip Details</h1>
                                                            
                               <br>
                            
                                 <div class="row">
                                     <div class="col-md-6">
                                     <p>Trip Planner: <?php echo $book['cus_fname']; ?> <?php echo $book['cus_lname']; ?></p>
                                     <p>Total trip duration: <?php echo $book['no_of_dates']; ?> days</p>
                                     <p>Total No.of adults: <?php echo $book['no_of_adults']; ?></p>
                                     <p>Total No.of children: <?php echo $book['no_of_children']; ?></p>
                                     <p>Trip Date: <?php echo $book['start_date']; ?></p>
                                     <p>Booking ID: <?php echo $book['bid']; ?></p>
                                 </div>
                                     <div class="col-md-6">
                                     <p>Price for <?php echo $book['no_of_adults']; ?> persons: $<?php echo $book['adults_total']; ?></p>
                                     <p>Price for <?php echo $book['no_of_children']; ?> children: $<?php echo $book['child_total']; ?></p>
                                     <p>Sub Total: $<?php echo $book['sub_total']; ?></p>
                                     
                                 </div>                                    
                                 </div>
                                
                                <div class="row">
                                    <h3><?php echo $packages['package_name']; ?></h3>
                                    <p>Region: <?php echo $packages['region']; ?></p> <p>Country: <?php echo $packages['country']; ?></p>
                                    <p><?php echo $packages['description']; ?></p>
                                    <p>
                                       <?php echo $packages['detailed_itinerary']; ?>
                                    </p>
                                    
                                </div>
                               
                               
                        </div>
                      
                      
                  </div>
    
                  <br>    <br>
    