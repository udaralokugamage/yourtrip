
    <div style="background-image: url(<?php echo base_url(); ?>assets/uploads/images/bb3.png);
              background-size: cover;height: 475px;" class="jumbotron text-center">
     
    </div>
    <div class="container">

      <div class="row text-center" >
          <h4 style="margin-top:60px;margin-bottom:60px;">Shopping for Travelling Packages? You’re in the right place. With millions of reviews from travelers worldwide, we can help you find the ideal hotel, inn or bed ,breakfast. And when you’re ready to book, we check 200+ sites to find you the latest and lowest hotel prices. It’s all right here!.</h4>
      </div>
       <div class="row" style="margin-bottom:75px">
         <div class="col-md-4" style="border-radius: 20px;"><img src="<?php echo base_url(); ?>assets/uploads/images/a.png" alt="Paris" width="315" height="260" style="border-radius: 20px;"></div>
         <div class="col-md-4" ><img src="<?php echo base_url(); ?>assets/uploads/images/d.png" alt="Paris" width="315" height="260" style="border-radius: 20px;"></div>
         <div class="col-md-4" style="border-radius: 20px;"><img src="<?php echo base_url(); ?>assets/uploads/images/s.png" alt="Paris" width="315" height="260" style="border-radius: 20px;"></div>
      </div>

    </div>
    <hr>
    <div class="container">
      <?php $this->load->view('templates/_recent_packages', $packages); ?>
    </div>
    <div class="container">
      <?php $this->load->view('templates/_offers_packages', $offers); ?>
    </div>
    <hr>
    </div>
       <div class="container">

      </div>
    <div class="container-fluid">

          <div class="row" style="background-color:#4db1ea;height:60px;">
            <div class="container">
              <div class="col-md-3 text-center" style="margin-bottom:0px;padding-bottom:0px !important;"><a href="#" style="text-decoration:none"><p style="margin-bottom:0px;color:#fff"><i class="fa fa-facebook" aria-hidden="true" style="color:#fff;font-size:22px;margin-top:20px;margin-bottom:20px;margin-right:10px"></i>Facebook</P></a></div>
              <div class="col-md-3 text-center" style="margin-bottom:0px;padding-bottom:0px !important;"><a href="#" style="text-decoration:none"><p style="margin-bottom:0px;color:#fff"><i class="fa fa-instagram" aria-hidden="true" style="color:#fff;font-size:22px;margin:20px;margin-bottom:20px;margin-right:10px">  </i>Instagram</P></a></div>
              <div class="col-md-3 text-center" style="margin-bottom:0px;padding-bottom:0px !important;"><a href="#" style="text-decoration:none"><p style="margin-bottom:0px;color:#fff"><i class="fa fa-pinterest-p" aria-hidden="true" style="color:#fff;font-size:22px;margin:20px;margin-bottom:20px;margin-right:10px">  </i>Pinterest</P></a></div>
              <div class="col-md-3 text-center" style="margin-bottom:0px;padding-bottom:0px !important;"><a href="#" style="text-decoration:none"><p style="margin-bottom:0px;color:#fff"><i class="fa fa-twitter" aria-hidden="true" style="color:#fff;font-size:22px;margin:20px;margin-bottom:20px;margin-right:10px">  </i>Twitter</P></div>
            </div>
          </div>
          <div class="row" style="background-color:#fff;height:310px;">
              <marquee behavior="alternate" direction="left">
                <img src="<?php echo base_url(); ?>assets/uploads/images/a.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/s.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/a.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/d.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/a.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/s.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/a.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
                <img src="<?php echo base_url(); ?>assets/uploads/images/d.png" width="315" height="310" alt="Natural" style="margin-left:-5px;margin-right:-5px;"/>
            </marquee>
          </div>
       </div>   

<script type="text/javascript">
  function myMap() {
    var mapOptions = {
        center: new google.maps.LatLng(<?php echo $travel[0]['lat']; ?>, <?php echo $travel[0]['longt']; ?>),
        zoom: 10
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var previous_marker = new google.maps.Marker({position: {lat: <?php echo $travel[0]['lat']; ?>, lng: <?php echo $travel[0]['longt']; ?>}, map: map});
    previous_marker.setMap(map);

    google.maps.event.addListener(map, 'click', function( event ){
      previous_marker.setMap(null);
      marker = new google.maps.Marker({position: event.latLng, map: map});
      marker.setMap(map);
      previous_marker = marker;

      $("#lat").val(event.latLng.lat());
      $("#longt").val(event.latLng.lng());
    });
  }
</script> 