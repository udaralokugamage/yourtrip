
<div class="container" style="margin-top: 10%;">
  <div class="row">
    <div class="col-md-4 col-md-offset-4 well">
      <?php $attributes = array("name" => "profileform");
      echo form_open("profile/index", $attributes);?>
      <legend style="text-align: center;">Update Profile</legend>
      
      <div class="form-group">
        <label for="name" style="text-align: center;">First Name</label>
        <input class="form-control" name="fname" placeholder="<?php echo $this->session->userdata('uname');?>" type="text" value="<?php echo set_value('fname'); ?>" />
        <span class="text-danger"><?php echo form_error('fname'); ?></span>
      </div>      
    
      <div class="form-group">
        <label for="name" style="text-align: center;">Last Name</label>
        <input class="form-control" name="lname" placeholder="<?php echo $this->session->userdata('lname');?>" type="text" value="<?php echo set_value('lname'); ?>" />
        <span class="text-danger"><?php echo form_error('lname'); ?></span>
      </div>
    
      <div class="form-group">
        <label for="email" style="text-align: center;">Email ID</label>
        <input class="form-control" name="email" placeholder="<?php echo $this->session->userdata('email');?>" type="text" value="<?php echo set_value('email'); ?>" />
        <span class="text-danger"><?php echo form_error('email'); ?></span>
      </div>
	  
      <div class="form-group">
        <label for="subject">Password</label>
        <input class="form-control" name="password" placeholder="Password" type="password" />
        <span class="text-danger"><?php echo form_error('password'); ?></span>
      </div
	  
	  <!------------ Adding Contact Number field -------------->
      <div class="form-group">
        <label for="contactNumber" style="text-align: center;">Contact Number</label>
        <input class="form-control" name="contactNumber" placeholder="<?php echo $this->session->userdata('contactNumber');?>" type="tel" value="<?php echo set_value('contactNumber'); ?>" />
        <span class="text-danger"><?php echo form_error('contactNumber'); ?></span>
      </div>
    
	  <!------------ Closing Contact Number field -------------->

      <div class="form-group">
        <a name="cancel" href="<?php echo base_url(); ?>index.php/home/index" class="btn btn-info" >Cancel</a>
        <button name="submit" type="submit" class="btn btn-info" >Update</button>
        <a href="<?php echo base_url(); ?>index.php/uppass/index" class="btn btn-info" style="margin-top: 5%;">Update Password</a>
      </div>
      <?php echo form_close(); ?>
      <?php echo $this->session->flashdata('msg'); ?>
    </div>
  </div>
