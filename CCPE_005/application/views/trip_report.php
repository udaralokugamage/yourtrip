<?php if($this->session->flashdata('booked')): ?>
<script>
    swal("Successfully Booked!", "You have successfully planned your trip.!", "success");
</script>
<?php endif; ?> 
<?php if($this->session->flashdata('mail_send')): ?>
<script>
    swal("Successfully Send!", "You have successfully send your trip plan report.!", "success");
</script>
<?php endif; ?> 
 <br> <br> <br> <br>

<div class="container-fluid" style="background-color: #fff;margin-bottom: 60px">  
    
                  <div class="container" style="border: 1px solid #eee;padding: 10px;border-radius: 6px">
                            
                        <div class="row" style="margin: 20px">
                          
                            <h1>Your Trip Details</h1>
                                                            
                               <br>
                            
                                 <div class="row">
                                     <div class="col-md-6">
                                     <p>Trip Planner: <?php echo $book['cus_fname']; ?> <?php echo $book['cus_lname']; ?></p>
                                     <p>Total trip duration: <?php echo $book['no_of_dates']; ?> days</p>
                                     <p>Total No.of adults: <?php echo $book['no_of_adults']; ?></p>
                                     <p>Total No.of children: <?php echo $book['no_of_children']; ?></p>
                                     <p>Trip Date: <?php echo $book['start_date']; ?></p>
                                     <p>Booking ID: <?php echo $book['bid']; ?></p>
                                 </div>
                                     <div class="col-md-6">
                                     <p>Price for <?php echo $book['no_of_adults']; ?> persons: $<?php echo $book['adults_total']; ?></p>
                                     <p>Price for <?php echo $book['no_of_children']; ?> children: $<?php echo $book['child_total']; ?></p>
                                     <p>Sub Total: $<?php echo $book['sub_total']; ?></p>
                                     
                                 </div>                                    
                                 </div>
                                
                                <div class="row">
                                    <h3><?php echo $packages['package_name']; ?></h3>
                                    <p>Region: <?php echo $packages['region']; ?></p> <p>Country: <?php echo $packages['country']; ?></p>
                                    <p><?php echo $packages['description']; ?></p>
                                    <p>
                                       <?php echo $packages['detailed_itinerary']; ?>
                                    </p>
                                    
                                </div>
                               
                               
                        </div>
                      
                      
                  </div>
    
                  <br>    <br>
                  <div class="container">
                        <div class="row">
                                    <div class="col-md-6">
                             <?php echo form_open('packages/partner_email/'.$book['bid']); ?>           
                                   <div class="form-group">                               
                                       <input class="form-control" name="pemail" type="text" placeholder="Enter your travel partner email here" id="inputSmall">
                                       <p class="text-danger"><?php echo validation_errors(); ?></p>
                                   </div>
                                    </div>
                                    <div class="col-md-6">

                                    <button style="width: 225px" type="submit"  class="btn btn-danger">Send mail</button>
                             <?php echo form_close(); ?> 
                                    <a style="width: 225px" href='<?php echo base_url(); ?>index.php/packages/print_report/<?php echo $book['bid']; ?>' class="btn btn-primary" >Generate PDF</a>
                                    </div>
                           
                        </div>
                  </div>
</div>