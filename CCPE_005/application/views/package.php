<script src="https://use.fontawesome.com/dac2c348c4.js"></script>
<style type="text/css">
  /* Rating Star Widgets Style */
  .rating-stars ul {
    list-style-type:none;
    padding:0;
    margin-top: 20px;
    
    -moz-user-select:none;
    -webkit-user-select:none;
  }
  .rating-stars ul > li.star {
    display:inline-block;
    
  }

  /* Idle State of the stars */
  .rating-stars ul > li.star > i.fa {
    font-size:1.5em; /* Change the size of the stars */
    color:#ccc; /* Color on idle state */
  }

  /* Hover state of the stars */
  .rating-stars ul > li.star.hover > i.fa {
    color:#FFCC36;
  }

  /* Selected state of the stars */
  .rating-stars ul > li.star.selected > i.fa {
    color:#FF912C;
  }
</style>


<div class="container-fluid" style="height:65px;"></div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2 style="margin-bottom: 25px;">
        <?php echo $package[0]['package_name']; ?>
      </h2>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <img class="img-thumbnail" src="http://localhost/travel/admin/uploads/packages/<?php echo $package[0]['thumb_image']; ?>" style="height:240px;width:auto;">
    </div>
    <div class="col-md-8">
      <p>
        <?php echo substr($package[0]['description'], 0, 300); ?>
        <?php echo (strlen($package[0]['description']) > 300) ? "... <a>more</a>" : '' ; ?>
      </p>
      <h3>
        <span class="label label-primary">
          <?php echo $package[0]['fixed_no_of_days']; ?> Days Trip        
        </span>&nbsp;
        <?php if($package[0]['status']) { ?>
          <span class="label label-warning" style="font-size: 60%"> Booking In Progress .. </span>
        <?php } else { ?>
          <span class="label label-danger" style="font-size: 60%"> Closed </span>
        <?php } ?>
      </h3>
      <div>
        <h2>
          <span style="font-size: 60%;">$</span>
          <?php echo $package[0]['price_adult']; ?>
          <span style="font-size: 50%;">
            <?php echo '( $ '.$package[0]['price_child'].' child )'; ?>
          </span>
        </h2>
      </div>
      <div>
        <button onclick="location.href='<?php echo base_url(); ?>packages/booking_page/<?php echo $package[0]['package_id']; ?>'" class="btn btn-info right"> BOOK NOW</button>
      </div>
    </div>
  </div>
</div>

<div class="container">

  <h3>
    <u>Overview</u> 
  </h3>

  <div style="height: 50px;margin-top: 30px;">
    <span class="label label-default" style="font-size: 20px;">
      <span class="glyphicon glyphicon-globe"></span>
      <?php echo $package[0]['region']; ?>
    </span>&nbsp;
    <span class="label label-success" style="font-size: 20px;">
      <span class="glyphicon glyphicon-plane"></span>
      <?php echo $package[0]['country']; ?>
    </span>
  </div>

  <p>
    <?php echo $package[0]['detailed_itinerary']; ?>
  </p>
</div>


<div class="container" style="margin-top: 20px;margin-bottom: 20px;">
  <h3> WE GO</h3>
  <div id="map" style="width:auto;height:400px;background:yellow;margin-bottom:20px;"></div>
</div>

<div class="container">
  <div class="row col-md-12">

  <?php foreach ($images as $image) { ?>
    <div class="col-md-4">
      <?php if($image['image']) { ?>
        <img class="img-thumbnail" src="http://localhost/travel/admin/uploads/travel/<?php echo $image['name']; ?>" style="height:240px;width:auto;">
      <?php } else { ?>

      <video width="auto" height="240px" controls>
        <source src="http://localhost/travel/admin/uploads/travel/<?php echo $image['name']; ?>" type="video/mp4">
        Your browser does not support the video tag.
      </video>

      <?php } ?>
    </div>
  <?php } ?>

  </div>
</div>

<hr>

<div class="container">

  <div class="row" style="margin-bottom: 20px; margin-top: 30px;">
    <div class="col-md-5">
      <h3> Place Your Review </h3>

      <?php if ($user_id === NULL){ ?>

      <div> Please Login to add your review </div>

      <?php } else { ?>

      <?php echo form_open('reviews/create'); ?>

      <form>

        <div>
          <section class='rating-widget'>
          <!-- Rating Stars Box -->
            <div class='rating-stars'>
              <ul id='stars'>
                <li class='star' title='Poor' data-value='1'>
                  <i class='fa fa-star fa-fw'></i>
                </li>
                <li class='star' title='Fair' data-value='2'>
                  <i class='fa fa-star fa-fw'></i>
                </li>
                <li class='star' title='Good' data-value='3'>
                  <i class='fa fa-star fa-fw'></i>
                </li>
                <li class='star' title='Excellent' data-value='4'>
                  <i class='fa fa-star fa-fw'></i>
                </li>
                <li class='star' title='WOW!!!' data-value='5'>
                  <i class='fa fa-star fa-fw'></i>
                </li>
              </ul>
            </div>
          </section>
        </div>

        <div class="form-group">
          <label for="comment">Comment</label>
          <textarea class="form-control" name="comment"></textarea>
        </div>

        <!-- user id hidden value -->
        <input type="input" name="user_id" hidden="true" value="<?php echo $user_id; ?>" />
        <!-- package id hidden -->
        <input type="input" name="package_id" hidden="true" value="<?php echo $package[0]['package_id']; ?>" />
        <!-- stars values hidden -->
        <input type="input" name="stars" id="stars-hidden-input" hidden="true" />
        <!--  -->

        <button type="submit" name="submit" class="btn btn-info">Add Review</button>
      </form>

      <?php } ?>
    </div>

    <?php if (empty($reviews)) { ?>
      <div class="col-md-7">
        <h3 style=""> Reviews </h3>
        <div style=""> No Reviews </div>
      </div>
    <?php } else { ?>

    <div class="col-md-7" style="border-radius: 3px;border: 1px solid #eee">
      <h3 style="text-align: center;"> Reviews </h3>
      <div style="width: auto; height: 350px; overflow-y: scroll;">
        <?php foreach ($reviews as $review) { ?>
          <dl class="dl-horizontal">
            <dt>
              <strong><?php echo $review['fname'].' '.$review['lname']; ?></strong>
              <br>
              <span class="badge"><?php echo $review['stars']; ?></span>
            </dt>
            <dd>
              <div>
                <?php echo $review['comment']; ?>
              </div>
            </dd>
          </dl>
        <?php } ?>
      </div>
    </div>

    <?php } ?>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
  
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function(){
      var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
     
      // Now highlight all the stars that's not after the current hovered star
      $(this).parent().children('li.star').each(function(e){
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
      
    }).on('mouseout', function(){
      $(this).parent().children('li.star').each(function(e){
        $(this).removeClass('hover');
      });
    });
    
    
    /* 2. Action to perform on click */
    $('#stars li').on('click', function(){
      var onStar = parseInt($(this).data('value'), 10); // The star currently selected
      var stars = $(this).parent().children('li.star');
      
      for (i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      
      for (i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }

      var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
      responseMessage(ratingValue);
      
    });

  });


  function responseMessage(value) {
    $('#stars-hidden-input').val(value);
  }

  function myMap() {

    var mapOptions = {
        center: new google.maps.LatLng(0, 0),
        zoom: 2
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

      <?php foreach ($locations as $location) { ?>
        var marker = new google.maps.Marker({position: {lat: <?php echo $location['lat']; ?>, lng: <?php echo $location['longt']; ?>}, map: map});
        marker.setMap(map);
      <?php } ?>


  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGfL40sospUmLPHgT3GqxilpKtohlA5LY&callback=myMap"></script>