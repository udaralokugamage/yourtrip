
    <style>
    @import url('https://fonts.googleapis.com/css?family=Lora');
    @import url('https://fonts.googleapis.com/css?family=Marvel');
    
    @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&subset=latin-ext,vietnamese');   
    
body{font-family: 'Quicksand', sans-serif;}
        h4{
    	font-weight: 600;
	}
	p{
		font-size: 12px;
		margin-top: 5px;
	}
	.price{
		font-size: 30px;
    	margin: 0 auto;
    	color: #333;
	}

	.thumbnail{
		opacity:0.80;
		-webkit-transition: all 0.5s; 
		transition: all 0.5s;
	}
	.thumbnail:hover{
		opacity:1.00;
		box-shadow: 0px 0px 10px #4bc6ff;
	}
	.line{
		margin-bottom: 5px;
	}
	@media screen and (max-width: 770px) {
		.right{
			float:left;
			width: 100%;
		}
	}
	span.thumbnail {
        border: 1px solid #00c4ff !important;
        border-radius: 0px !important;
        -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
	padding: 10px;
    }
    .container h4{margin-top:30px; margin-bottom:15px;}
    button {    margin-top: 6px;
    }
    .right {
        float: right;
        border-bottom: 2px solid #0a5971;
    }
    .btn-info {
        color: #fff;
        background-color: #19b4e2;
        border-color: #19b4e2;
            font-size:13px;
            font-weight:600;
    }
    </style>
 
 
<div style="background-image: url(<?php echo base_url(); ?>assets/uploads/images/bb3.png);
    background-size: cover;height: 475px;" class="jumbotron text-center">
</div>
     
<?php if($this->session->flashdata('msg_send')): ?>
<script>
    swal("Successfully Send!", "You have successfully send your request.!", "success");
</script>
<?php endif; ?> 

<div class="container" style="width: 1170px !important">
                    
        <div class="row" style=" text-align: center;">
            <h4 class="package_heading">HOT DESTINATIONS</h4>
        </div><br>
        
    <div class="row">
	
            <?php 
            if (!empty($Sregions) && !empty($Scountry) && !empty($Smax_price)&& !empty($Sdays) ) {  ?>
               <h3>Your Searching Results:</h3>
               <p class="lead text-info">Destinations in <?php echo $Scountry ?>, <?php echo $Sregions ?> region with <?php echo $Smax_price ?> max price and <?php echo $Sdays ?> maximum days </p>
            <?php  }  ?>
            <?php 
             if (!count($packages)>0) { 
                  $empty_result = "Sorry, no results were found.!";?>
               
             <h3><?php echo $empty_result ?></h3>
            <?php  }  ?>
        
        <div id="all_packages" class="col-md-9 col-sm-12">    
                <!-- BEGIN PRODUCTS -->
                <?php foreach($packages as $package) : ?> 
                <div class="col-md-4 col-sm-6">
                                <span class="thumbnail">
                                    <a onclick="toPackage(<?php echo $package['package_id']; ?>);" href="#"><img id="pimg" class="img-responsive" src="http://gig4.opendata.lk/travel/admin/uploads/packages/<?php echo $package['thumb_image']; ?>" alt="..." style="height:200px;width:260px"></a>

                                    <div style="height:60px"><h4 id="ptitle" style="text-transform: uppercase !important;text-align: center"><?php echo $package['package_name']; ?></h4></div>
                                        <div>
                                             <p style="font-size: 14px;text-align: center"><?php echo $package['fixed_no_of_days']; ?> Days Trip.</p>
                                             <p style="display: block;background: #4db1ea;color: #fff;font-size: 18px;text-align: center;font-weight: 400">Region : <?php echo $package['region']; ?> Tour</p>                             
                                             <p style="display: block;background: #4db1ea;color: #fff;font-size: 18px;text-align: center;font-weight: 400">Country : <?php echo $package['country']; ?></p>
                                        </div>
                                        <hr class="line">
                                        <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <p class="price">$<?php echo $package['price_adult']; ?></p>                                       
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                      <?php if($this->session->userdata('login') == TRUE): ?>
                                                           <button  onclick="location.href='<?php echo base_url(); ?>packages/booking_page/<?php echo $package['package_id']; ?>'" class="btn btn-info right"> BOOK NOW</button>
                                                      <?php else: ?>
                                                         <button  onclick="location.href='<?php echo base_url(); ?>packages/not_login'" class="btn btn-info right"> BOOK NOW</button>
                                                      <?php endif; ?>    
                                                        
                                                </div>

                                        </div>
                                </span>
                </div>
                <?php endforeach; ?> 
                <!-- END PRODUCTS -->
        </div>
        <div class="col-md-3 col-sm-12">
                <div class="row" style=" text-align: center;">
                    <h4 style="margin-top:0px !important;font-size: 22px !important" class="search_tour">SEARCH TOURS</h4>
                </div>
            
            <?php echo form_open_multipart('home/search_package'); ?>
            
                <p>
                    <div class="form-group">
                        <label class="control-label" for="inputSmall">SELECT REGION :</label>
                        <select id="region" name="region"  style="border-color: #4dd6ff; color:#f6931f; font-weight:bold;position: relative; font-size: 14px; white-space: pre-wrap; line-height: 21px;" class="form-control">
                           
                        <option>- Select a Region -</option>                         
                        <?php foreach($regions as $region): ?>
                            <option><?php echo $region['r_name']; ?></option>
                        <?php endforeach; ?>                          
                
                        </select>
                        <?php if(form_error("region")){ ?>
                                        <span style="color: red;display: block;font-size: 14px;line-height: 26px" > <?php echo form_error("region"); ?></span>
                                        <script> 
                                        document.getElementById('region').style.border = "1.5px solid red";
                                        </script>
                        <?php } ?>    

                    </div>
                </p>
                <br>
                <p>
                    <div class="form-group">
                        <label class="control-label" for="inputSmall">SELECT COUNTRY :</label>
                        <select id="country" name="country"  style="border-color: #4dd6ff; color:#f6931f; font-weight:bold;position: relative; font-size: 14px; white-space: pre-wrap; line-height: 21px;" class="form-control">
                        <option>- Select a Country -</option>                         
                       
                        </select>
                        <?php if(form_error("country")){ ?>
                                        <span style="color: red;display: block;font-size: 14px;line-height: 26px" > <?php echo form_error("country"); ?></span>
                                        <script> 
                                        document.getElementById('country').style.border = "1.5px solid red";
                                        </script>
                        <?php } ?>   
                    </div>
                </p>
                <br>
                 <p>
                    <div class="form-group">
                        <label class="control-label" for="inputSmall" for="amount">MAX PRICE :</label>
                        <input type="text" id="amount" name="max_price" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        <?php if(form_error("max_price")){ ?>
                                        <span style="color: red;display: block;font-size: 14px;line-height: 26px" > <?php echo form_error("max_price"); ?></span>                           
                        <?php } ?>   
                    </div>
                </p>            
          
                <div id="slider-range"></div>
                <br><br>
                
                  <p>
                    <div class="form-group">
                        <label class="control-label" for="inputSmall">SELECT MAXIMUM DAYS :</label>
                        <select id="days" name="days"  style="border-color: #4dd6ff; color:#f6931f; font-weight:bold;position: relative; font-size: 14px; white-space: pre-wrap; line-height: 21px;" class="form-control" id="select">
                            <option>- Select a Days -</option> 
                            <option>2</option>
                            <option>4</option>                              
                            <option>6</option>
                            <option>10</option>
                            <option>14</option>
                            <option>20</option>
                            <option>20+</option>
                        </select>
                        <?php if(form_error("days")){ ?>
                                        <span style="color: red;display: block;font-size: 14px;line-height: 26px" > <?php echo form_error("days"); ?></span>
                                        <script> 
                                        document.getElementById('days').style.border = "1.5px solid red";
                                        </script>
                        <?php } ?>   
                       </div>
                </p>
                <br>
                <div class=" form-group">
                     <button class="btn btn-primary btn-sm" style="width: 100%" type="submit">SEARCH</button>                 
                </div>
               
        </div>
    <?php echo form_close(); ?>
        
    </div><br><br>
                    
</div>                    
                    
        	
<div class="container-fluid" style="background-color: #f1f4f5">  
    <div class="container">          
        <div class="row" >    
            
              <div class="row" style=" text-align: center;">
                    <h4 class="package_heading">Exclusive holidays for any travelers</h4>
              </div>
              <div class="row" style=" text-align: center;">
                  <p style="font-size: 16px !important">For more details, Please Contact Us..</p>
              </div><br> 
              <div class="row">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                      
                <?php echo form_open('packages/contact_us'); ?>    
                      
                <textarea class="form-control" style="border-radius: 0px !important;" id="message" name="message" placeholder="Message" rows="6"></textarea>
                <br />
                <div class="row">
                <div class="col-xs-5 col-md-5 form-group">
                <input class="form-control input-lg" style="border-radius: 0px !important;" id="name" name="name" placeholder="Name" type="text" required  />
                </div>
                <div class="col-xs-7 col-md-7 form-group">
                <input class="form-control input-lg" style="border-radius: 0px !important;" id="email" name="email" placeholder="Email" type="email" required />
                </div>
                </div>
               
             
                <div class="row">
                <div class="col-xs-12 col-md-12 form-group">
                          <button class="btn btn-primary pull-right" style="width:220px !important;" type="submit">Submit</button>              
                                                       
                <?php echo form_close(); ?>
                            
                          <p class="text-danger">   
                              <?php echo validation_errors(); ?>
                          </p>      
                </div>
                </div>
                  </div>
                  <div class="col-md-1"></div>
              
            </div>

		<!-- END PRODUCTS -->
	</div>
</div>

<script type="text/javascript">
  function toPackage(id) {
    window.location = "<?php echo base_url() ?>packages/"+id;
  }
</script>

<!-------container----->


 <script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min:0,
      max: 4000,
    
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val($( "#slider-range" ).slider( "values", 1 ));
  } );
  </script>
  
  <script>
    
$(function (){ //document is loaded so we can bind events

  $("#region").change(function (){ //change event for select

     var combo=document.getElementById('region').value;
//     alert(combo);
     $.ajax({  //ajax call
        type: "POST",      //method == POST 
        url: '<?php echo base_url(); ?>/home/region_country/'+combo //url to be called                    
     }).done(function(msg) {

        $('#country').empty();
        obj = JSON.parse(msg);
//        alert(msg);
        for (i = 0; i < obj.sub.length; i++) {
            $('#country').append($('<option/>').attr("value", obj.sub[i].name).text(obj.sub[i].name));
        }
     
     }); 
   });
   
 }); 
 
</script>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>