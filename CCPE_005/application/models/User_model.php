<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function get_user($email, $pwd)
	{
		$this->db->where('email', $email);
		$this->db->where('password', md5($pwd));
        $query = $this->db->get('customer');
		return $query->result();
	}

	function get_cuser($id, $pwd)
	{
		$this->db->where('id', $id);
		$this->db->where('password',($pwd));
        $query = $this->db->get('customer');
		return $query->result();
	}
	
	// get user
	function get_user_by_id($id)
	{
		$this->db->where('id', $id);
        $query = $this->db->get('customer');
		return $query->result();
	}
	
	// insert
	function insert_user($data)
    {
		return $this->db->insert('customer', $data);
	}

	function updateps($id,$password)
	{
		$data= array('password' =>$password );
		$this->db->where('id', $id);
		//$query=$this->db->update('customer',$data);
		return $this->db->update('customer',$data);
		//return $query->result();
	}

	function updateprop($id,$fname,$lname,$email,$contactNumber)
	{
		$data= array('fname' => $fname, 'lname' =>$lname,'email'=>$email,'contactNumber' => $contactNumber);
		$this->db->where('id',$id);
		return $this->db->update('customer',$data);
	}
	
	// Get booking history for the logged customer
	public function GetBookingHistory($id)
	{
		$this->db->where('cus_id', $id);
		$query = $this->db->get('booking');
		return $result=$query->result();
	}	
	
	// Email after registration
    public function registration_email($fname,$lname,$cus_email){
              

        $this->load->library('email');

        $this->email->set_mailtype('html');

        $this->email->set_newline("\r\n");
        $this->email->from('mail4jayanchinthaka@gmail.com','YourTrip.lk'); // change it to yours
        $this->email->to($email);// change it to yours

        $this->email->subject('Confirmation of Registration at YourTrip.lk');
		
			//insert user details into array to pass to email
			$data = array(
				'fname' => $this->input->post('fname'),
				'lname' => $this->input->post('lname'),
				'cus_email' => $this->input->post('cus_email')
			);
                
        $msg = $this->load->view('reg_email', $data, true);
                
        $this->email->message($msg);

        $this->email->send();
        
    } 	

}?>