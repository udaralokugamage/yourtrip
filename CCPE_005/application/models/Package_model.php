<?php

class Package_model extends CI_Model{
    
    
    public function get_hot_package($pid = FALSE){
        
	if($pid === FALSE){
            $this->db->order_by('package_id', 'DESC');
           
            $query = $this->db->get('package',6);
            return $query->result_array();
	}
                        
         
            $query = $this->db->get_where('package', array('package_id' => $pid));
            return $query->row_array();
            
    }
    
    
    public function get_region_package($region){
      
            $query = $this->db->get_where('package', array('region' => $region));
            return $query->row_array();
            
    }
    
    
     public function get_region(){
        
	$this->db->order_by('r_name');
	$query = $this->db->get('region');
	return $query->result_array();
                        
    }  
    
    
    public function search_packages(){
        
        $region = $this->input->post('region');
        $country = $this->input->post('country');
        $max_price = $this->input->post('max_price');
        $max_days = $this->input->post('days');
        
	$this->db->order_by('package_id');
        $this->db->where('region',$region);
        $this->db->where('country',$country);
        $this->db->where('price_adult <',$max_price);
        $this->db->where('fixed_no_of_days <',$max_days);
	$query = $this->db->get('package');
        
        $number_of_rows = $query->num_rows();
        
//        if($number_of_rows>0){
            return $query->result_array();
//        }  else {
//            return FALSE;
//        }
	
    }  
    
    public function get_current_user($uid){
		$this->db->where('id', $uid);
                $query = $this->db->get('customer');
		return $query->row_array();
	}
    
    public function get_country($rname){
        
            $this->db->select('country.name');
            $this->db->from('country');
            $this->db->join('region', 'region.id = country.region_id');
            $this->db->order_by('country.name');
            $this->db->where('region.r_name',$rname);
            $query = $this->db->get();
            
            return $query->result_array();

    } 
    
    public function booking(){
        
        $uf_name = $this->input->post('Bfname');
        $ul_name = $this->input->post('Blname');
        $uemail = $this->input->post('Bemail');
        $ucontact = $this->input->post('Bcontact');
        $start_date = $this->input->post('start_day');
        $adults = $this->input->post('adults');
        $Children = $this->input->post('Children');
        $adults_total= $this->input->post('adults_total');
        $child_total = $this->input->post('child_total');
        $sub_total = $this->input->post('sub_total');
        $user_id = $this->input->post('Buid');
        
        $pack_id = $this->input->post('Bpid');
        $pack_name = $this->input->post('Bpname');
        $region = $this->input->post('Bregion');
        $country = $this->input->post('Bcountry');
        $days = $this->input->post('Bdays');
        $priceC = $this->input->post('priceC');
        $priceP = $this->input->post('priceP');
        
        $data = array(  
            
            'cus_fname'     =>  $uf_name,
            'cus_lname'     =>  $ul_name,
            'email'         =>  $uemail,
            'contact'       =>  $ucontact,
            'package_name'  =>  $pack_name,
            'region'        =>  $region,
            'country'       =>  $country,
            'no_of_dates'   =>  $days,
            'price_p'       =>  $priceP,
            'adults_total'  =>  $adults_total,
            'price_c'       =>  $priceC,
            'child_total'   =>  $child_total,
            'sub_total'     =>  $sub_total,
            'start_date'    =>  $start_date,
            'no_of_adults'  =>  $adults,
            'no_of_children'=>  $Children,
            'cus_id'        =>  $user_id,
            'package_id'    =>  $pack_id
           
		
	);
        
        $this->db->insert('booking', $data);
        $insert_id = $this->db->insert_id();
//      $this->session->set_flashdata('booked', 'Successfully Booked');
        $this->get_booking_data($insert_id);
        
        return $this->get_booking_data($insert_id);

    } 
    
    public function get_booking_data($bid){
		$this->db->where('bid', $bid);
                $query = $this->db->get('booking');
		return $query->row_array();
	}
        
    public function booking_receipt_email($email,$packageID,$bookID){
              

                $this->load->library('email');

                $this->email->set_mailtype('html');

                $this->email->set_newline("\r\n");
                $this->email->from('petition@petition.lk','YourTrip.lk'); // change it to yours
                $this->email->to($email);// change it to yours

                $this->email->subject('Confirmation of your Booking at YourTrip.lk');

                $data['book']=$this->get_booking_data($bookID);
                $data['packages'] = $this->package_model->get_hot_package($packageID);
                
                $msg = $this->load->view('con_email', $data, true);
                
                $this->email->message($msg);

                $this->email->send();
        
    }

    public function recent_packages()
    {
        $this->db->order_by('created_date', 'ASC');
        $query = $this->db->get('package',6);
        $this->db->where('deleted', false);
        return $query->result_array();
    }

  public function offers_packages()
  {
    $this->db->where('offer', 1);
    $query = $this->db->get('package',6);
    $this->db->where('deleted', false);
    return $query->result_array();
  }

  public function get_package($pid)
  {
    // print_r($pid);
    $this->db->where('package_id', $pid);
    $query = $this->db->get('package',1);
    return $query->result_array();
  }

  public function get_locations($pid)
  {
    $this->db->select('travel.id as id ,travel.lat as lat, travel.longt as longt');
    $this->db->from('travel_package');
    $this->db->join('travel', 'travel.id = travel_package.travel_id');
    $this->db->where('package_id', $pid);

    $query = $this->db->get();
    $result = $query->result_array();

    if (empty($result)) {
        return [];
    }

    return $result;

  }

  public function get_locations_images($pid)
  {
    $this->db->select('travel.id as id ,travel.lat as lat, travel.longt as longt');
    $this->db->from('travel_package');
    $this->db->join('travel', 'travel.id = travel_package.travel_id');
    $this->db->where('package_id', $pid);

    $areas = $this->db->get()->result_array();
    // return $query->result_array();

    $ids = [];
    foreach ($areas as $area) {
      array_push($ids, $area['id']);
    }

    if (!empty($ids)) {
        $this->db->select('name, image');
        $this->db->from('travel_media');
        $this->db->where_in('travel_id', $ids);
        $query = $this->db->get();
        return $query->result_array();
    }


    return [];
  }
        
}