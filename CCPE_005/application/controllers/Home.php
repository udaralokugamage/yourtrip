<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('package_model');
    }
    
	public function index()
	{
        $data['packages'] = $this->package_model->recent_packages();
        $data['offers'] = $this->package_model->offers_packages();
            
            $this->load->view('templates/header');
            $this->load->view('main', $data);
            $this->load->view('templates/footer');
		
	}
        
        public function travel()
	{            
            $data['regions'] = $this->package_model->get_region();
            $data['packages'] = $this->package_model->get_hot_package();
            
                $this->load->view('templates/header');
		$this->load->view('all_packages',$data);
		$this->load->view('templates/footer');
	}
	
	public function history()
	{            
        $uid =$this->session->userdata('uid');
		//$this->data['historys'] = $this->user_model->GetBookingHistory($uid);

        $data = array();

        $data['result']=$this->user_model->GetBookingHistory($uid);
		
        $this->load->view('templates/header');
        $this->load->view('history_view',$data);
        $this->load->view('templates/footer');
	}

	
	public function about()
	{            
        $this->load->view('templates/header');
		$this->load->view('about_view');
		$this->load->view('templates/footer');
	}	
        
        public function search_package()
	{
            
            $this->form_validation->set_rules('region', 'Region', 'required|callback_region_check');           
            $this->form_validation->set_rules('country', 'Country', 'required|callback_country_check');
            $this->form_validation->set_rules('max_price', 'Max Price', 'required|callback_max_price_check');
            $this->form_validation->set_rules('days', 'Max Days', 'required|callback_days_check');           

            if($this->form_validation->run() === FALSE){  
                
            $data['regions'] = $this->package_model->get_region();
            $data['packages'] = $this->package_model->get_hot_package();
            
           
            $this->load->view('templates/header');
			$this->load->view('all_packages',$data);
			$this->load->view('templates/footer');
                
            } else{
            
                 $data['Sregions']= $this->input->post('region');
                 $data['Scountry']= $this->input->post('country');
                 $data['Smax_price'] = $this->input->post('max_price');
                 $data['Sdays'] = $this->input->post('days');
          
                
//                 if(!$this->package_model->search_packages()){
                     
                      $data['regions'] = $this->package_model->get_region();
                      $data['packages'] = $this->package_model->search_packages();
//                 }  else {
//                      $data['regions'] = $this->package_model->get_region();
//                      $data['packages'] = $this->package_model->search_packages();
//                 }
                 
            $this->load->view('templates/header');
			$this->load->view('all_packages',$data);
			$this->load->view('templates/footer');
               

            }
            
	}
        //callback functions
                public function region_check($str)
                {
                        if ($str == '- Select a Region -')
                        {
                                $this->form_validation->set_message('region_check', 'Please select a Region');
                                return FALSE;
                        }
                        else
                        {
                                return TRUE;
                        }
                }
                
                 public function country_check($str)
                {
                        if ($str == '- Select a Country -')
                        {
                                $this->form_validation->set_message('country_check', 'Please select a Country');
                                return FALSE;
                        }
                        else
                        {
                                return TRUE;
                        }
                }
                
                 public function max_price_check()
                {
                        if ($this->input->post('max_price')==0)
                        {
                                $this->form_validation->set_message('max_price_check', 'Please select max price');
                                return FALSE;
                        }
                        else
                        {
                                return TRUE;
                        }
                }
                
                 public function days_check($str)
                {
                        if ($str == '- Select a Days -')
                        {
                                $this->form_validation->set_message('days_check', 'Please select a maximum no of days');
                                return FALSE;
                        }
                        else
                        {
                                return TRUE;
                        }
                }
                
        public function region_country($rname){
           
            $data['sub'] = $this->package_model->get_country($rname);          
            $json = json_encode($data);
            echo $json;
 
	}
        
        
    function logout()
	{
	// destroy session
        $data = array('login' => '', 'uname' => '', 'lname' => '', 'email' => '', 'uid' => '');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
	redirect('home/index');
	}

}