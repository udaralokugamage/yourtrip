<?php
class uppass extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library(array('session', 'form_validation'));
		$this->load->database();
		$this->load->model('user_model');
	}
	
	function index()
	{
		//$details = $this->user_model->get_user_by_id($this->session->userdata('uid'));
		//$this->load->view('uppass_view');
		// set form validation rules;
		$this->form_validation->set_rules('curpassword', 'Currentpassword', 'trim|required|md5|min_length[8]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5|min_length[8]');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|md5|required|matches[password]');
		// submit
		$curpassword =$this->input->post('curpassword');
		$uid =$this->session->userdata('uid');

		if ($this->form_validation->run() == FALSE)
        {
			// fails
                                $this->load->view('templates/header');
                                $this->load->view('uppass_view');
//                                $this->load->view('templates/footer');
			
        }
        else{
        	if (count($this->user_model->get_cuser($uid, $curpassword))> 0) {

			//insert user details into db
				$password = $this->input->post('password');
			
			if ($this->user_model->updateps($uid,$password))
			{
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Password is Successfully Update</div>');
				$this->load->view('templates/header');
                                $this->load->view('uppass_view');
//                                $this->load->view('templates/footer');
			}
			else
			{
				// error
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				$this->load->view('templates/header');
                                $this->load->view('uppass_view');
//                                $this->load->view('templates/footer');
			}
		}
		else{	

				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Current Password is not valid!</div>');
				$this->load->view('templates/header');
                                $this->load->view('uppass_view');
//                                $this->load->view('templates/footer');
		}
        }

		
	}
	
}