<?php

class profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library(array('session', 'form_validation'));
		$this->load->database();
		$this->load->model('user_model');
	}
	
	function index()
	{
		//$this->load->view('profile_view');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[customer.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5|min_length[8]');
		$this->form_validation->set_rules('contactNumber', 'Contact Number', 'trim|required|regex_match[/^[0-9]{10}$/]');
		
		// submit
		if ($this->form_validation->run() == FALSE)
        {
			// fails
                        $this->load->view('templates/header');
                        $this->load->view('profile_view');
                        $this->load->view('templates/footer');
			
        }

        $uid =$this->session->userdata('uid');
		$curtpassword=$this->input->post('password');
		$fname=$this->input->post('fname');
		$lname=$this->input->post('lname');
		$email=$this->input->post('email');
		$contactNumber=$this->input->post('contactNumber');
        $uresult = $this->user_model->get_cuser($uid, $curtpassword);
		if (count($uresult)> 0) {

			//insert user details into db
			
			if ($this->user_model->updateprop($uid,$fname,$lname,$email,$contactNumber))
			{	

				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">User data is Successfully Update</div>');
				$sess_data = array('login' => TRUE, 'uname' => $fname,'lname' => $lname,'email' => $email,'contactNumber' => $contactNumber, 'uid' => $uid);
				$this->session->set_userdata($sess_data);

                                $this->load->view('templates/header');
                                $this->load->view('profile_view');
                                $this->load->view('templates/footer');
						}
			else
			{
				// error
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				$this->load->view('templates/header');
                                $this->load->view('profile_view');
                                $this->load->view('templates/footer');
			}
		}
		else{	
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Current Password is not valid!</div>');
				$this->load->view('templates/header');
                                $this->load->view('profile_view');
                                $this->load->view('templates/footer');
		}
	}

}