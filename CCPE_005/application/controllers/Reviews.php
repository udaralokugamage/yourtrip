<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('reviews_model','package_model'));
    $this->load->helper(array('form', 'url'));
    $this->load->library(array('form_validation', 'session'));
  }

  public function index($package_id)
  {
    $data['reviews'] = $this->reviews_model->reviews_get($package_id);
    $this->load->view('package', $data);
  }

  public function review_form($package_id, $user_id)
  {
    $data['package_id'] = $package_id;
    $this->load->view('reviews/review_form', $data);
  }

  public function create()
  {
    
    $this->form_validation->set_rules('comment', 'Please Enter Your Comment', 'required');
    $this->form_validation->set_rules('stars', 'Please Rate', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->load->view('reviews/review_form');
    }
    else
    {
      $package_id = $this->reviews_model->review_post();
      redirect('packages/show/'.$package_id);
    }
  }
}