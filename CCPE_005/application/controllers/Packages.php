<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('package_model', 'reviews_model', 'user_model'));
    $this->load->helper(array('form', 'url'));
    $this->load->library(array('form_validation', 'session'));
  }

	public function booking_page($pid)
	{
             $uid = $this->session->userdata('uid');
           
             $data['user'] = $this->package_model->get_current_user($uid);
             $data['packages'] = $this->package_model->get_hot_package($pid);
             
                        $this->load->view('templates/header');
			$this->load->view('booking',$data);
			$this->load->view('templates/footer');
		
	}
        
        
        
        public function cart()
	{
                        $this->load->view('templates/header');
			$this->load->view('shopping_cart');
			$this->load->view('templates/footer');
		
	}
        
        public function not_login()
	{
                        $this->session->set_flashdata('not_log', 'Sorry, you require to be logged in to book packages..!');
                        
                        $this->load->view('templates/header');
                        $this->load->view('login_view');
		
	}
        
        public function partner_email($bid)
	{
                         $p_email= $this->input->post('pemail');
                         $this->form_validation->set_rules('pemail', 'Email', 'trim|required|max_length[100]|min_length[5]|valid_email');
                         if($this->form_validation->run() === FALSE){
                             
                            $data['book']=$this->package_model->get_booking_data($bid);
                            $packageID = $data['book']['package_id'];
//                            $uemail = $data['book']['email'];
//                            $bookID = $data['book']['bid'];
//                            $this->package_model->booking_receipt_email($uemail,$packageID,$bookID);
                            $data['packages'] = $this->package_model->get_hot_package($packageID);
//                            $this->session->set_flashdata('booked', 'Successfully Booked');

                            $this->load->view('templates/header');
                            $this->load->view('trip_report',$data);
                            $this->load->view('templates/footer');
                            
                         }else {
                             
                             $data['book']=$this->package_model->get_booking_data($bid);
                             $packageID = $data['book']['package_id'];
//                            $uemail = $data['book']['email'];
//                            $bookID = $data['book']['bid'];
                            $this->package_model->booking_receipt_email($p_email,$packageID,$bid);
                            $data['packages'] = $this->package_model->get_hot_package($packageID);
                            $this->session->set_flashdata('mail_send', 'Successfully Send');

                            $this->load->view('templates/header');
                            $this->load->view('trip_report',$data);
                            $this->load->view('templates/footer');
                             
                         }
	}
        
        
        public function booking_pack()
	{
          
            $pid = $this->input->post('Bpid');
                    
            $this->form_validation->set_rules('Bfname', 'Customer First Name', 'required');
            $this->form_validation->set_rules('Blname', 'Customer Last Name', 'required');
            $this->form_validation->set_rules('Bemail', 'Customer Email', 'required');
            $this->form_validation->set_rules('Bcontact', 'Customer Contact', 'required');
            $this->form_validation->set_rules('start_day', 'Starting day', 'required');
            $this->form_validation->set_rules('adults', 'No of adults', 'required');
            $this->form_validation->set_rules('Children', 'No of Children', 'required');
          
            if($this->form_validation->run() === FALSE){
               
           $uid = $this->session->userdata('uid');
           
             $data['user'] = $this->package_model->get_current_user($uid);
             $data['packages'] = $this->package_model->get_hot_package($pid);
             
                        $this->load->view('templates/header');
			$this->load->view('booking',$data);
			$this->load->view('templates/footer');
                
            } else {
                
                $data['book']=$this->package_model->booking();
                $packageID = $data['book']['package_id'];
                $uemail = $data['book']['email'];
                $bookID = $data['book']['bid'];
                $this->package_model->booking_receipt_email($uemail,$packageID,$bookID);
                $data['packages'] = $this->package_model->get_hot_package($packageID);
                $this->session->set_flashdata('booked', 'Successfully Booked');

                        $this->load->view('templates/header');
			$this->load->view('trip_report',$data);
			$this->load->view('templates/footer');
                
            }
		
	}
        
          public function view_print_report($bid){
                 
                            $data['book']=$this->package_model->get_booking_data($bid);
                            $packageID = $data['book']['package_id'];
//                            $uemail = $data['book']['email'];
//                            $bookID = $data['book']['bid'];
//                            $this->package_model->booking_receipt_email($uemail,$packageID,$bookID);
                            $data['packages'] = $this->package_model->get_hot_package($packageID);
//                            $this->session->set_flashdata('booked', 'Successfully Booked');

                           // $this->load->view('templates/header');
                            $this->load->view('trip_plan',$data);
                           // $this->load->view('templates/footer');
                            
          }
          
          
        public function contact_us()
	{
            $cmsg = $this->input->post('message');
            $cname = $this->input->post('name');
            $cemail = $this->input->post('email');
            
            $this->form_validation->set_rules('message', 'Your Message', 'required');
            $this->form_validation->set_rules('name', 'Your Name', 'required');
            $this->form_validation->set_rules('email', 'Your Email', 'required');
            
            if($this->form_validation->run() === FALSE){
               
                $data['regions'] = $this->package_model->get_region();
                $data['packages'] = $this->package_model->get_hot_package();
                
                $this->load->view('templates/header');
		$this->load->view('all_packages',$data);
		$this->load->view('templates/footer');
                
            } else {
            
                $data['regions'] = $this->package_model->get_region();
                $data['packages'] = $this->package_model->get_hot_package();
           
                $this->contact_email($cemail,$cname,$cmsg);
                
                $this->session->set_flashdata('msg_send', 'Successfully Send');
                 
                $this->load->view('templates/header');
		$this->load->view('all_packages',$data);
		$this->load->view('templates/footer');
                        
            }
            
	}
        
         public function contact_email($email,$name,$msgg){
              

                $this->load->library('email');

                $this->email->set_mailtype('html');

                $this->email->set_newline("\r\n");
                $this->email->from($email,'YourTrip.lk'); // change it to yours
                $this->email->to('18886971@student.curtin.edu.au');// change it to yours

                $this->email->subject('Inquiring some more details about packages');
                
                $data['msg']= $msgg;
                $data['name'] = $name;
                $data['email']= $email;
               
                $msg = $this->load->view('contact_email', $data, true);
                
                $this->email->message($msg);

                $this->email->send();
                
        
         }    
        
         public function print_report($bid){

         require 'pdfmyurl.php';

            try {
                    // first fill in the license that you received upon sign-up
                    $pdf = new PDFmyURL ('bFGGIpQxouqD');

                    // now set the options, so for example when we want to have a page in A4 in orientation portrait
                    $pdf->SetPageSize('A4');
                    $pdf->SetPageOrientation('Portrait');

                    // then do the conversion - this is how you convert Google to PDF and display the PDF to the user
                    $pdf->CreateFromURL (base_url().'index.php/packages/view_print_report/'.$bid);
                    $pdf->Display();

            }  catch (Exception $error) {
                       echo $error->getMessage();
                       echo $error->getCode();
            }



         
            
    }
      public function show($pid)
    {
      $data['user_id'] = $this->session->userdata('uid');
      //---
      $data['package'] = $this->package_model->get_package($pid);
      $data['reviews'] = $this->reviews_model->reviews_get($pid);
      $data['locations'] = $this->package_model->get_locations($pid);
      $data['images'] = $this->package_model->get_locations_images($pid);
      //---

      $this->load->view('templates/header');
      $this->load->view('package', $data);
      $this->load->view('templates/footer');
    }

}