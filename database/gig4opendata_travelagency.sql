-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2017 at 07:39 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gig4opendata_travelagency`
--

-- --------------------------------------------------------

--
-- Table structure for table `airline`
--

CREATE TABLE `airline` (
  `airline_id` int(11) NOT NULL,
  `airline_name` varchar(50) NOT NULL,
  `airline_description` text NOT NULL,
  `thumb_image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airline`
--

INSERT INTO `airline` (`airline_id`, `airline_name`, `airline_description`, `thumb_image`) VALUES
(2, 'Cathay', 'Wonderful', 'thumb_1497171785_download.jpg'),
(3, 'Sri Lankan', 'Great Experience', 'thumb_1497171808_f582f242159707bd76cd2b781ce47775.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `bid` int(11) NOT NULL,
  `cus_fname` varchar(150) NOT NULL,
  `cus_lname` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(150) NOT NULL,
  `package_name` varchar(500) NOT NULL,
  `region` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `no_of_dates` int(11) NOT NULL,
  `price_p` float NOT NULL,
  `adults_total` float NOT NULL,
  `price_c` float NOT NULL,
  `child_total` float NOT NULL,
  `sub_total` float NOT NULL,
  `start_date` date NOT NULL,
  `no_of_adults` int(11) NOT NULL,
  `no_of_children` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cus_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`bid`, `cus_fname`, `cus_lname`, `email`, `contact`, `package_name`, `region`, `country`, `no_of_dates`, `price_p`, `adults_total`, `price_c`, `child_total`, `sub_total`, `start_date`, `no_of_adults`, `no_of_children`, `status`, `date_added`, `cus_id`, `package_id`) VALUES
(186, 'Jayan', 'Chinthaka', '18886971@student.curtin.edu.au', '1234567890', 'DISCOVER COLOMBO\r\n', 'ASIA', 'Sri Lanka', 12, 800, 1600, 400, 1200, 2800, '2017-02-25', 2, 3, 0, '2017-06-10 16:35:05', 12, 2),
(187, 'Buhuni', 'Piyarisi', 'dbpiyarisi@gmail.com', '1234567890', 'Romantic Greece Tour: Athens, Mykonos & Santorini', 'EUROPE', 'Greece ', 16, 2000, 8000, 1000, 2000, 10000, '2017-08-23', 4, 2, 0, '2017-06-10 21:22:27', 14, 9),
(188, 'Jayan', 'Chinthaka', '18886971@student.curtin.edu.au', '1234567890', 'DISCOVER COLOMBO\r\n', 'ASIA', 'Sri Lanka', 12, 800, 800, 400, 400, 1200, '2017-02-25', 1, 1, 0, '2017-06-10 23:10:38', 12, 2),
(189, 'Jayan', 'Chinthaka', '18886971@student.curtin.edu.au', '1234567890', 'DISCOVER COLOMBO\r\n', 'ASIA', 'Sri Lanka', 12, 800, 1600, 400, 400, 2000, '2017-02-25', 2, 1, 0, '2017-06-10 23:29:47', 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `region_id`) VALUES
(1, 'Japan ', 1),
(2, 'China ', 1),
(3, 'Turkey ', 1),
(4, 'Greece ', 5),
(5, 'Italy ', 5),
(6, 'Zambia ', 6),
(7, 'Africa', 6),
(8, 'America ', 4),
(9, 'Australia ', 2),
(10, 'Sri Lanka', 1),
(11, 'India', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(12) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `contactNumber` varchar(12) NOT NULL,
  `customer_status` tinyint(4) NOT NULL COMMENT 'enable - 1, disable - 0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `fname`, `lname`, `email`, `password`, `contactNumber`, `customer_status`) VALUES
(13, 'Abcd', 'sdff', 'ab@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1234567890', 0),
(12, 'Jayan', 'Chinthaka', '18886971@student.curtin.edu.au', '202cb962ac59075b964b07152d234b70', '147852369', 0),
(14, 'Buhuni', 'Piyarisi', 'bpiyarisi@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '0712295980', 0),
(15, 'fname', 'lname', 'email@email.com', 'e10adc3949ba59abbe56e057f20f883e', '0716677889', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `image_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `thumb_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`image_id`, `package_id`, `file_name`, `thumb_image`) VALUES
(1, 2, '1496252475_Koala.jpg', 'thumb_1496252475_Koala.jpg'),
(2, 2, '1496252475_Lighthouse.jpg', 'thumb_1496252475_Lighthouse.jpg'),
(3, 2, '1496252475_Penguins.jpg', 'thumb_1496252475_Penguins.jpg'),
(4, 2, '1496252475_pool.jpg', 'thumb_1496252475_pool.jpg'),
(5, 2, '1496252476_Tulips.jpg', 'thumb_1496252476_Tulips.jpg'),
(7, 14, '1497173186_greece_0.jpg', 'thumb_1497173186_greece_0.jpg'),
(8, 14, '1497173197_greece_island_sunset_0.jpg', 'thumb_1497173197_greece_island_sunset_0.jpg'),
(9, 14, '1497173209_greece_santorini.jpg', 'thumb_1497173209_greece_santorini.jpg'),
(10, 14, '1497173218_greece_santorini_tour_santorini_sunset_oia.jpg', 'thumb_1497173218_greece_santorini_tour_santorini_sunset_oia.jpg'),
(11, 14, '1497173229_Tsoliades.jpg', 'thumb_1497173229_Tsoliades.jpg'),
(12, 14, '1497173237_Mykonos_Grand_-_pool_massage.jpg', 'thumb_1497173237_Mykonos_Grand_-_pool_massage.jpg'),
(14, 15, '1497173731_beijing_temple_of_heaven_felix_willeke_-_x.jpg', 'thumb_1497173731_beijing_temple_of_heaven_felix_willeke_-_x.jpg'),
(15, 15, '1497173740_moscow_gum_department_store_yury_gubin_fotolia_-_x.jpg', 'thumb_1497173740_moscow_gum_department_store_yury_gubin_fotolia_-_x.jpg'),
(16, 15, '1497173750_zaisan_memorial_ulan_bator_mongolia.jpg', 'thumb_1497173750_zaisan_memorial_ulan_bator_mongolia.jpg'),
(17, 15, '1497173759_zarengold_-_lernidee_0115_-_ross_hillier_x.jpg', 'thumb_1497173759_zarengold_-_lernidee_0115_-_ross_hillier_x.jpg'),
(18, 15, '1497173769_zarengold_-_lernidee_1724_-_ross_hillier_x_2.jpg', 'thumb_1497173769_zarengold_-_lernidee_1724_-_ross_hillier_x_2.jpg'),
(19, 15, '1497173777_zarengold_-_lernidee_8119_-_ross_hillier_x.jpg', 'thumb_1497173777_zarengold_-_lernidee_8119_-_ross_hillier_x.jpg'),
(20, 15, '1497173788_zarengold_-_lernidee_9460_-_ross_hillier_x_0.jpg', 'thumb_1497173788_zarengold_-_lernidee_9460_-_ross_hillier_x_0.jpg'),
(21, 16, '1497174179_australia_great_barrier_reef_clear_fishes.jpg', 'thumb_1497174179_australia_great_barrier_reef_clear_fishes.jpg'),
(22, 16, '1497174187_australia_great_barrier_reef_panorama_view_h1.jpg', 'thumb_1497174187_australia_great_barrier_reef_panorama_view_h1.jpg'),
(24, 16, '1497174207_australia_sydney_bondi_beach_beautiful_sunset_and_waves_0.jpg', 'thumb_1497174207_australia_sydney_bondi_beach_beautiful_sunset_and_waves_0.jpg'),
(25, 16, '1497174222_australia_sydney_opera_house_0.jpg', 'thumb_1497174222_australia_sydney_opera_house_0.jpg'),
(26, 16, '1497174230_australia-gold-coast-hamilton-island1.jpg', 'thumb_1497174230_australia-gold-coast-hamilton-island1.jpg'),
(27, 16, '1497174308_australia-melbourne-flinders-street-station-entrance12.jpg', 'thumb_1497174308_australia-melbourne-flinders-street-station-entrance12.jpg'),
(31, 17, '1497174637_india_agra_taj_mahal_unidentified_tourists_visit_indias_most_famous_icon-e.jpg', 'thumb_1497174637_india_agra_taj_mahal_unidentified_tourists_visit_indias_most_famous_icon-e.jpg'),
(32, 17, '1497174643_india_agra_view_from_agra_fort_on_the_taj_mahal-e.jpg', 'thumb_1497174643_india_agra_view_from_agra_fort_on_the_taj_mahal-e.jpg'),
(33, 17, '1497174649_india_delhi_ruins_of_tughlaqabad_fort_in_delhi.jpg', 'thumb_1497174649_india_delhi_ruins_of_tughlaqabad_fort_in_delhi.jpg'),
(35, 17, '1497174664_india_delhi_view_of_the_jama_masjid_mosque_-e.jpg', 'thumb_1497174664_india_delhi_view_of_the_jama_masjid_mosque_-e.jpg'),
(36, 17, '1497174674_india_jaipur_tour_monkey_temple_galwar_bagh.jpg', 'thumb_1497174674_india_jaipur_tour_monkey_temple_galwar_bagh.jpg'),
(37, 17, '1497174681_india_new_delhi_tour_inside_of_meenakshi_hindu_temple_0.jpg', 'thumb_1497174681_india_new_delhi_tour_inside_of_meenakshi_hindu_temple_0.jpg'),
(38, 17, '1497174688_india_new_delhi_tour_statue_of_garlanded_hindu_god_krishna_0.jpg', 'thumb_1497174688_india_new_delhi_tour_statue_of_garlanded_hindu_god_krishna_0.jpg'),
(39, 17, '1497174694_india_taj_mahal_in_india_0.jpg', 'thumb_1497174694_india_taj_mahal_in_india_0.jpg'),
(40, 18, '1497175019_elephant_on_sri_lanka.jpg', 'thumb_1497175019_elephant_on_sri_lanka.jpg'),
(43, 18, '1497175041_sri_lanka_elephants_in_water_0.jpg', 'thumb_1497175041_sri_lanka_elephants_in_water_0.jpg'),
(44, 18, '1497175049_sri_lanka_leopard_in_the_wild_on_the_island_of_sri_lanka.jpg', 'thumb_1497175049_sri_lanka_leopard_in_the_wild_on_the_island_of_sri_lanka.jpg'),
(45, 18, '1497175057_sri_lanka_tropical_beach_with_palm_trees.jpg', 'thumb_1497175057_sri_lanka_tropical_beach_with_palm_trees.jpg'),
(46, 18, '1497175063_tea_plantations_in_munnar_kerala_india.jpg', 'thumb_1497175063_tea_plantations_in_munnar_kerala_india.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `hotel_id` int(11) NOT NULL,
  `hotel_name` varchar(200) NOT NULL,
  `hotel_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotel_id`, `hotel_name`, `hotel_description`) VALUES
(3, 'Araliya', 'Amazing'),
(4, 'Sunset', 'Beautiful View'),
(5, 'Cinnamon Bay', 'Great Place to be'),
(6, 'Kanneliya Hotel', 'A dreamy room for you and your family');

-- --------------------------------------------------------

--
-- Table structure for table `hotels_gallery`
--

CREATE TABLE `hotels_gallery` (
  `image_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `thumb_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotels_gallery`
--

INSERT INTO `hotels_gallery` (`image_id`, `hotel_id`, `file_name`, `thumb_image`) VALUES
(11, 3, '1495971529_Gullfoss,_an_iconic_waterfall_of_Iceland.jpg', 'thumb_1495971529_Gullfoss,_an_iconic_waterfall_of_Iceland.jpg'),
(22, 4, '1497171958_genoa-italy-500px.jpg', 'thumb_1497171958_genoa-italy-500px.jpg'),
(23, 5, '1497171975_images.jpg', 'thumb_1497171975_images.jpg'),
(30, 6, '1497172223_genoa-italy-500px.jpg', 'thumb_1497172223_genoa-italy-500px.jpg'),
(31, 6, '1497172253_187288a_hb_a_001.jpg', 'thumb_1497172253_187288a_hb_a_001.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `price_adult` decimal(10,2) NOT NULL,
  `price_child` decimal(10,2) NOT NULL,
  `detailed_itinerary` text NOT NULL,
  `thumb_image` varchar(200) NOT NULL,
  `banner_image` varchar(200) NOT NULL,
  `fixed_no_of_days` varchar(5) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - enable, 0 - disable',
  `status_change_track` tinyint(4) NOT NULL COMMENT '1 - change status once',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL COMMENT '1 - delete',
  `region` varchar(150) NOT NULL,
  `country` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `offer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`package_id`, `package_name`, `description`, `price_adult`, `price_child`, `detailed_itinerary`, `thumb_image`, `banner_image`, `fixed_no_of_days`, `status`, `status_change_track`, `created_by`, `created_date`, `updated_date`, `deleted`, `region`, `country`, `country_id`, `region_id`, `offer`) VALUES
(1, 'CYCLING ADVENTURES\n', 'A feeling of majesty overtakes you when you see historic palaces or view unencumbered wildlife in its natural habitat. Experience the nature and history of India on this custom tailored tour. Find the grandeur of sandstone castles casting a pink glow upon a city.', '200.00', '100.00', '(Day 1): Begin your Adventure with a Tour through Delhi, Indiaâ€™s Capital\n<br>\n(Day 2): Make your Way to the Pink City of Jaipur for a Half-Day Exploration\n<br>\n(Day 3): Spend the Day Enjoying the Impressive History and Architecture of Jaipur\n<br>\n(Day 4): Transfer from Jaipur to Agra to View the Famous Taj Mahal before Making your way Onward Towards Umaryia\n<br>\n(Day 5): Venture through the Bandhavgarh National Park in Search of Wildlife\n<br>\n(Day 6): Explore Bandhavgarh National Park to See Tigers in their Natural Habitat\n<br>\n(Day 7): Depart for Kanha National Park Arriving in Time for an Evening Game Drive\n<br>\n(Day 8): Scour the Wilderness in Search of Stunning Wildlife at Kanha National Park<br>', 'Bicycle-Tour-560x460.jpg', 'banner_1493995382_AmarSagar.jpg', '11', 1, 0, 1, '2017-05-05 16:25:54', '2017-05-05 16:43:02', 1, 'ASIA', 'Sri Lanka', 10, 1, 0),
(2, 'DISCOVER COLOMBO', 'Italy is for lovers and historians, foodies and hikers, art enthusiasts and fashionistas. In other words, Italy is for everyone.', '800.00', '400.00', 'TRIP AT A GLANCE<br> \r\n(Day 1): Venice â€“ Arrive in the World Renowned City of Venice\r\n<br>\r\n(Day 2): Venice â€“ Enjoy the Charm of Venice at your Leisure\r\n<br>\r\n(Day 3): Venice â€“ Explore Venice and the Surrounding Lagoon\r\n<br>\r\n(Day 4): Milan â€“ Journey to the Fashionable City of Milan\r\n<br>\r\n(Day 5): Milan â€“ Discover the Beauty of Milan and its Lush Countryside\r\n<br>\r\n(Day 6): Florence â€“ Venture to the Captivating Renaissance City of Florence\r\n<br>\r\n(Day 7): Florence â€“ Half-Day Tour of the Historic and Captivating City Center\r\n<br>\r\n(Day 8): Florence â€“ Relish an Excursion into the Tuscan Countryside\r\n<br>\r\n(Day 9): Rome â€“ Transfer to the Treasured Eternal City of Rome\r\n<br>\r\n(Day 10): Rome â€“ Enjoy a Day at your Leisure and a Dinner Cruise on the Tiber\r\n<br>\r\n(Day 11): Rome â€“ Continue your Roman Experience or Enjoy a Day Tour to Naples\r\n<br>\r\n(Day 12): Rome â€“ Depart for Home<br>', 'the-benjamin-shopping-560x460.jpg', 'banner_1493994871_genoa-italy-500px.jpg', '12', 1, 0, 1, '2017-05-05 16:29:13', '2017-06-11 14:45:08', 1, 'ASIA', 'Sri Lanka', 10, 1, 1),
(3, 'ELEPHANT TOUR', 'Welcome to Elephant Country â€“ a land that treats gentle giants as royalty! As the sun sets in the horizon, elevate yourself in the midst of the forest to witness these tree-twisting jumbos, stroll across the wide open plains beneath your feet, or simply gaze upon the shallow waters of Gal Oya National Park and be amused as these giants swim calmly from one island to another. If getting in closer range with the â€˜tuskedâ€™ is your pick, then the Millennium Elephant Foundation is your bliss, as it lets you have a memorable elephant ride, feed and even bathe its inmates. Meanwhile, enhance your experience as you reach Udawalawe National Park which boasts of its own Elephant Transit Home. Do not be surprised to hear trumpets across a land, haunted by the big foot herding across its vast plains. After all, the â€˜Elephant Tourâ€™ is nothing less than an experience, waiting to be cherished for a lifetime!', '500.00', '250.00', 'TRIP AT A GLANCE \r\n(Day 1): Venice â€“ Arrive in the World Renowned City of Venice\r\n\r\n(Day 2): Venice â€“ Enjoy the Charm of Venice at your Leisure\r\n\r\n(Day 3): Venice â€“ Explore Venice and the Surrounding Lagoon\r\n\r\n(Day 4): Milan â€“ Journey to the Fashionable City of Milan\r\n\r\n(Day 5): Milan â€“ Discover the Beauty of Milan and its Lush Countryside\r\n\r\n(Day 6): Florence â€“ Venture to the Captivating Renaissance City of Florence\r\n\r\n(Day 7): Florence â€“ Half-Day Tour of the Historic and Captivating City Center\r\n\r\n(Day 8): Florence â€“ Relish an Excursion into the Tuscan Countryside\r\n\r\n(Day 9): Rome â€“ Transfer to the Treasured Eternal City of Rome\r\n\r\n(Day 10): Rome â€“ Enjoy a Day at your Leisure and a Dinner Cruise on the Tiber\r\n\r\n(Day 11): Rome â€“ Continue your Roman Experience or Enjoy a Day Tour to Naples\r\n\r\n(Day 12): Rome â€“ Depart for Home', 'Elephant-Tour.jpg', 'Elephant-Tour', '11', 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'ASIA', 'Sri Lanka', 10, 1, 0),
(4, 'Sydney, Great Barrier Reef, Melbourne', 'Step into the image of picture-perfect Australia set inside the grandeur of the Sydney Opera House and the majesty of the Great Barrier Reef. Follow kangaroos as they hop across island plains, and bask in the powdery-soft white sands of a secluded island surrounded by turquoise waters. From charming cafes decorated with captivating art to enticing restaurants filled with seductive aromas, whether lounging on a sailboat or sipping wine with a view of spreading vineyards, ', '700.00', '350.00', 'Step into the image of picture-perfect Australia set inside the grandeur of the Sydney Opera House and the majesty of the Great Barrier Reef. Follow kangaroos as they hop across island plains, and bask in the powdery-soft white sands of a secluded island surrounded by turquoise waters. From charming cafes decorated with captivating art to enticing restaurants filled with seductive aromas, whether lounging on a sailboat or sipping wine with a view of spreading vineyards, ', 'australia_sydney.jpg', 'australia_sydney.jpg', '14', 0, 0, 8, '2017-05-01 00:00:00', '2017-05-23 00:00:00', 1, 'Australia ', 'Australia ', 9, 2, 0),
(5, 'America Tours', 'South America has inspired travel for millennia. A land rich with the cultural history of ancient civilizations, it is also home to two of the New Seven Wonders of the World, Machu Picchu in Peru and Christ the Redeemer in Brazil. The natural splendor alone makes the adventure worthwhile, whether itâ€™s the stunning peaks of Torres del Paine of Patagonia or the largest waterfall systems in the world, Iguazu Falls.', '2000.00', '1000.00', 'South America has inspired travel for millennia. A land rich with the cultural history of ancient civilizations, it is also home to two of the New Seven Wonders of the World, Machu Picchu in Peru and Christ the Redeemer in Brazil. The natural splendor alone makes the adventure worthwhile, whether itâ€™s the stunning peaks of Torres del Paine of Patagonia or the largest waterfall systems in the world, Iguazu Falls.', 'chile_pataganiapatagonia_0.jpg', 'chile_pataganiapatagonia_0.jpg', '20', 0, 0, 8, '2017-05-10 00:00:00', '2017-05-16 00:00:00', 1, 'America ', 'America ', 8, 4, 0),
(6, 'Maasai Mara, Lake Nakuru, Lake Naivasha', 'An African safari isnâ€™t a passive experience. It isnâ€™t just about glimpsing wildlife. Youâ€™re enveloped in their untamed world, absolutely surrounded by the drama and charm of the planetâ€™s greatest theater; every angle is unique and each moment personalized. Allow Zicasso''s safari experts to handcraft your perfect immersion, bringing harmonic luxury to the natural wonder, and tailoring your dream African safari.', '1000.00', '500.00', 'An African safari isnâ€™t a passive experience. It isnâ€™t just about glimpsing wildlife. Youâ€™re enveloped in their untamed world, absolutely surrounded by the drama and charm of the planetâ€™s greatest theater; every angle is unique and each moment personalized. Allow Zicasso''s safari experts to handcraft your perfect immersion, bringing harmonic luxury to the natural wonder, and tailoring your dream African safari.', 'africa_kenya_masai_mara_national_reserve_safari_lions.jpg', 'africa_kenya_masai_mara_national_reserve_safari_lions.jpg', '9', 0, 0, 1, '2017-05-17 00:00:00', '2017-05-29 00:00:00', 1, 'AFRICA', 'Africa', 7, 6, 0),
(7, 'Intimate Zambia and Expressive Kenya Safari Itinerary', 'Stimulate your senses with a visit to The Smoke That Thunders, otherwise known as Victoria Falls, one of the Seven Natural Wonders of the World. Although many tourists flock to its more famous neighbors, Zambia has continued to maintain its off the beaten path charm. With its abundant diversity of wildlife, untouched natural landscapes, and welcoming people, experience real Africa.    ', '1000.00', '500.00', '(Day 1): Begin your Adventure with a Tour through Delhi, Indiaâ€™s Capital\n<br>\n(Day 2): Make your Way to the Pink City of Jaipur for a Half-Day Exploration\n<br>\n(Day 3): Spend the Day Enjoying the Impressive History and Architecture of Jaipur\n<br>\n(Day 4): Transfer from Jaipur to Agra to View the Famous Taj Mahal before Making your way Onward Towards Umaryia\n<br>\n(Day 5): Venture through the Bandhavgarh National Park in Search of Wildlife\n<br>\n(Day 6): Explore Bandhavgarh National Park to See Tigers in their Natural Habitat\n<br>\n(Day 7): Depart for Kanha National Park Arriving in Time for an Evening Game Drive\n<br>\n(Day 8): Scour the Wilderness in Search of Stunning Wildlife at Kanha National Park<br>', 'spotted_hyenas_and_wild_dogs_fight_h1_0.jpg', 'spotted_hyenas_and_wild_dogs_fight_h1_0.jpg', '12', 1, 0, 8, '2017-05-29 00:00:00', '2017-05-10 00:00:00', 1, 'AFRICA', 'Zambia ', 6, 6, 0),
(8, 'Italy Tours & Vacations', 'Italy is a country thatâ€™s as varied as the palettes of its great Renaissance artists, awash with color and culture. Lose yourself in the green rolling hills of Tuscany, bathed in the orange glow of the setting sun, and sip a succulent limoncello while taking in the sapphire surf of the sea. Marvel at the fantastical cream-colored figures of the Trevi Fountain, and relax at a neighborhood trattoria with a ruby-red glass of the countryâ€™s best Chianti. This isnâ€™t a place where you go to vacation â€“ this is a place where you go to live.', '1300.00', '650.00', 'Italy is a country thatâ€™s as varied as the palettes of its great Renaissance artists, awash with color and culture. Lose yourself in the green rolling hills of Tuscany, bathed in the orange glow of the setting sun, and sip a succulent limoncello while taking in the sapphire surf of the sea. Marvel at the fantastical cream-colored figures of the Trevi Fountain, and relax at a neighborhood trattoria with a ruby-red glass of the countryâ€™s best Chianti. This isnâ€™t a place where you go to vacation â€“ this is a place where you go to live.', 'ferrari_museum_italy_e_h1_1.jpg', 'ferrari_museum_italy_e_h1_1.jpg', '15', 1, 1, 8, '2017-05-15 00:00:00', '2017-05-13 00:00:00', 1, 'EUROPE', 'Italy ', 5, 5, 1),
(9, 'Romantic Greece Tour: Athens, Mykonos & Santorini', 'Crafted for 2017-2018, enjoy the relaxing atmosphere of Athens and the Greek Islands, combining remarkable archaeological sites, magnificent beaches, white washed villages, scrumptious food and great shopping! In Athens - pay tribute to the Acropolis. In Mykonos - stroll through the whitewashed houses and the narrow alleyways and beautiful paradise beaches. In Santorini - enjoy magnificent views and white washed houses perched on the edge of the cliff overlooking the Aegean sea. We also', '2000.00', '1000.00', 'Crafted for 2017-2018, enjoy the relaxing atmosphere of Athens and the Greek Islands, combining remarkable archaeological sites, magnificent beaches, white washed villages, scrumptious food and great shopping! In Athens - pay tribute to the Acropolis. In Mykonos - stroll through the whitewashed houses and the narrow alleyways and beautiful paradise beaches. In Santorini - enjoy magnificent views and white washed houses perched on the edge of the cliff overlooking the Aegean sea. We also', 'greece-santorini_view_of_caldera_with_domes_header.jpg', 'greece-santorini_view_of_caldera_with_domes_header.jpg', '16', 1, 0, 8, '2017-05-08 00:00:00', '2017-05-17 00:00:00', 1, 'EUROPE', 'Greece ', 4, 5, 0),
(10, 'A Perfect Romantic Getaway Vacation to Turkey', 'Turkey revels in its role as continental uniter, defying traditional fault lines as it blends the very best of Europe and Asia. While iconic East and West captivation is an omnipresent undercurrent, Turkey has always exuded a distinct identity. Whether experiencing its ancient architectural riches, beguiling traditions, or mystifying landscapes, few countries offer such inimitability.', '900.00', '450.00', 'Turkey revels in its role as continental uniter, defying traditional fault lines as it blends the very best of Europe and Asia. While iconic East and West captivation is an omnipresent undercurrent, Turkey has always exuded a distinct identity. Whether experiencing its ancient architectural riches, beguiling traditions, or mystifying landscapes, few countries offer such inimitability.', 'Turkey-Ephesus-Library-Ruins-with-People-LT-Header.jpg', 'Turkey-Ephesus-Library-Ruins-with-People-LT-Header.jpg', '15', 1, 0, 8, '2017-05-15 00:00:00', '2017-05-17 00:00:00', 1, 'Middle East', 'Turkey ', 3, 1, 0),
(11, 'Luxury Vacation in China', 'What were the most enjoyable or memorable parts of your trip? \r\nThe places our travel agency recommended to us that were not on our scope.  We were also blown away by the accommodations -- we were on budget yet the rooms were uniformly wonderful (sometimes we hated to leave, but then the next place was just as terrific).   We also requested restaurant recommendations that were not touristy and ideally local and authentic -- our guides took us to places that were precisely what we had in mind.', '800.00', '400.00', 'What were the most enjoyable or memorable parts of your trip? \r\nThe places our travel agency recommended to us that were not on our scope.  We were also blown away by the accommodations -- we were on budget yet the rooms were uniformly wonderful (sometimes we hated to leave, but then the next place was just as terrific).   We also requested restaurant recommendations that were not touristy and ideally local and authentic -- our guides took us to places that were precisely what we had in mind.', 'chinagreatwallofchinainsummerwithbeautifulsky.jpg', 'chinagreatwallofchinainsummerwithbeautifulsky.jpg', '12', 1, 0, 8, '2017-05-29 00:00:00', '2017-05-20 00:00:00', 1, 'ASIA', 'China ', 2, 1, 0),
(12, 'Kyoto, Tokyo, Naoshima', 'Your overall comments on the trip and the travel company: \r\nThe Japan tour company met and exceeded all our expectations. The agent who planned our trip, was very responsive to our concerns, and to planning a trip that met our needs and wants.  All of the hotels we stayed at were great (with the exception of one in Kyoto that had some heating issues and even it was great except our room was too hot), the directions were clear yet still allowed (required) you to plan the day you wanted. It was not so regimented so we were able to be spontaneous on any given day. We did use guides two days and they were exceptional!', '900.00', '450.00', 'Your overall comments on the trip and the travel company: \r\nThe Japan tour company met and exceeded all our expectations. The agent who planned our trip, was very responsive to our concerns, and to planning a trip that met our needs and wants.  All of the hotels we stayed at were great (with the exception of one in Kyoto that had some heating issues and even it was great except our room was too hot), the directions were clear yet still allowed (required) you to plan the day you wanted. It was not so regimented so we were able to be spontaneous on any given day. We did use guides two days and they were exceptional!', 'japan.jpg', 'japan.jpg', '12', 1, 0, 1, '2017-05-30 00:00:00', '2017-05-19 00:00:00', 1, 'ASIA', 'Japan ', 1, 1, 0),
(13, 'test test', 'sdfsd sd fsdf', '1234.00', '12.00', 'sd fsdf sdfsdf 222', 'chinagreatwallofchinainsummerwithbeautifulsky.jpg', 'banner_1496252424_Lighthouse.jpg', '12', 1, 0, 1, '2017-05-31 23:10:24', '2017-05-31 23:10:51', 1, 'ASIA', 'Turkey ', 3, 1, 0),
(14, 'A Perfect Greece Vacation Package 2017-2018: Mykonos, Santorini, & More', 'TRIP HIGHLIGHTS \r\nExperience the majesty of the Acropolis filled with millennia of history including the famous Parthenon\r\nStroll along the Plaka, the classic and continuously used walkway of ancient Athens\r\nStand on the shores of flawless beaches in Mykonos\r\nEnjoy the confluence of cosmopolitan shops within antique buildings\r\nWitness the painting-like beauty of the quintessential Cyclades architecture on the cliffs of Santorini\r\nWatch the sun dip behind the horizon on a secluded sunset cruise\r\nRevel in the mythical realm of the Palace of Knossos (if you extend to Crete)\r\nMeander through the stunning Venetian style streets of Chania (if you extend to Crete)', '1000.00', '500.00', 'TRIP AT A GLANCE <br>\r\n(Day 1): Arrive in Athens and be taken to the old city center<br>\r\n(Day 2): Tour the mesmerizing antiquity of the Acropolis and Athenian history<br>\r\n(Day 3): Transfer to the stunning island of Mykonos<br>\r\n(Day 4): Explore the sensational beaches Mykonos<br>\r\n(Day 5): Traverse the meandering classic streets of Mykonos Town<br>\r\n(Day 6): Hydrofoil to the famous Santorini<br>\r\n(Day 7): Experience the majesty of a sunset cruise<br>\r\n(Day 8): Spend the day enjoying the Santorini at your leisure<br>\r\n(Day 9): Depart for home (unless you extend your trip)<br>\r\n(Day 9 Trip Extension): Cruise to Crete<br>\r\n(Day 10): Discover the ruins of the Knossos Minoan Palace<br>\r\n(Day 11): Revel in the Venetian city of Chania<br>\r\n(Day 12): Luxuriate in the seclusion of Elounda on Eastern Crete<br>\r\n(Day 13): Depart for home', 'thumb_1497173069_Girl_in_Santorini.jpg', 'banner_1497173069_00-Great-Small-Hotels-in-Greece-Kinsterna.jpg', '9', 1, 0, 1, '2017-06-11 14:54:29', '0000-00-00 00:00:00', 0, 'Europe', 'Greece ', 4, 5, 0),
(15, 'Tsar''s Gold Trans-Siberian Railway Tour by Private Train: Beijing to Moscow', 'TRIP HIGHLIGHTS \r\nIndulge in a private train experience, with attentive service, fine dining, and elegant luxury within your private compartment on the Trans-Siberian train\r\nWander along the ancient stone walkway of the Great Wall of China\r\nDiscover the breathtaking landscape of the Mongolian Alps and the unique culture of the nomads living along the steppe\r\nView the heart of Imperial Russia and the seat of the country’s power during a private tour of the Kremlin complex\r\nEnjoy the captivating scenery of the Siberian countryside on a panoramic train ride along the shores of the world’s oldest and deepest freshwater lake\r\nWitness the distinctive combination of Muslim and European culture in the former Tartar capital of Kazan\r\nRelish a view of Bactrian camels while riding through the Gobi Desert, the largest desert on the Asian continent \r\nExplore the imperial splendor of Beijing’s Forbidden City to find jade artifacts and the marvelous Dragon Throne\r\nTrace the history of Buddhism in Mongolia while visiting the architecturally stunning Choijin Lama Temple in the capital of Ulan Bator\r\nVisit the Church upon Blood in Yekaterinburg, erected over the site at which the Russian Revolution claimed the lives of the Romanov Family', '5500.00', '2750.00', 'TRIP AT A GLANCE <br>\r\n(Day 1): Beijing – Arrive the Capital of China with an Introductory Tour of the City\r\n<br>\r\n(Day 2): Beijing – Discover the Treasured Great Wall of China and Ming Tombs\r\n<br>\r\n(Day 3): Beijing – Visit the Forbidden City and Depart Beijing Heading for Mongolia\r\n<br>\r\n(Day 4): Gobi Desert – Transfer Trains at the Border and Explore the Town of Erlian\r\n<br>\r\n(Day 5): Ulan Bator – Dock in the Capital of Mongolia to Tour Choijin Lama Temple\r\n<br>\r\n(Day 6): Mongolian Alps – Explore Gandan Monastery and the Mongolian Alps\r\n<br>\r\n(Day 7): Ulan-Ude – Delight in the Scenery Passing into Russia to Visit Ulan-Ude\r\n<br>\r\n(Day 8): Lake Baikal – Traverse the Blissful Shores of Lake Baikal on a Scenic Train\r\n<br>\r\n(Day 9): Irkutsk – Arrive in Irkutsk to Stroll along the Market and History Museums\r\n<br>\r\n(Day 10): Siberia – Full Day Scenic Ride through Siberia with Entertaining Classes\r\n<br>\r\n(Day 11): Novosibirsk – Enjoy Novosibirsk Hospitality and Explore the Large City\r\n<br>\r\n(Day 12): Yekaterinburg – Relish a Cultural Tour of the border of Europe and Asia\r\n<br>\r\n(Day 13): Kazan – Wander along Traditional Russian Architecture and a Unique Past\r\n<br>\r\n(Day 14): Moscow – Arrive in Moscow with a Tour of the Kremlin and Red Square\r\n<br>\r\n(Day 15): Moscow – Delight in a Full Day Tour of Moscow Before your Flight Home', 'thumb_1497173674_zarengold_-_lernidee_9460_-_ross_hillier_x_0.jpg', 'banner_1497173619_zarengold_-_lernidee_1724_-_ross_hillier_x_2.jpg', '15', 1, 0, 1, '2017-06-11 15:03:39', '2017-06-11 15:04:34', 0, 'Asia', 'China ', 2, 1, 0),
(16, 'Best of Australia Tour: Sydney, Great Barrier Reef, Melbourne', 'TRIP HIGHLIGHTS \r\nDive into the warm waters of the South Pacific to snorkel or scuba dive along prime locations of the Great Barrier Reef\r\nDelight in a scenic cruise to Whitehaven Island and traverse the nature trails to view the idyllic turquoise waters set against white sand beach\r\nWitness koalas lounging in their natural habitats of the eucalyptus canopy while wandering the aerial boardwalk of the Koala Conservation Centre\r\nSpend time on the beach of Phillip Island to watch the little penguins return from the Tasman Sea\r\nTraverse the historic lanes and arcades of Melbourne to settle into the inviting, vivacious culture of shops, cafes, and art\r\nEnjoy a private tour of the iconic Sydney Opera House for a deeper understanding of the architectural style and detail\r\nVisit the famous sands of Bondi Beach for an immersive look at the beach culture of Sydney\r\nExplore the Rocks district in Sydney to view the oldest European neighborhood in all of Australia\r\nTraverse the trails of the Blue Mountains to find waterfalls, verdant forests, and deep gorges\r\nIndulge in luxurious accommodations across Australia, including a lavish resort overlooking the white shores of Hamilton Island  ', '300.00', '150.00', 'TRIP AT A GLANCE<br>\r\n \r\n(Day 1): Sydney – Arrive in the Exciting and Energetic Beachside City of Sydney\r\n<br>\r\n(Day 2): Sydney – Enjoy a Half-Day Tour to Explore the Highlights of Sydney\r\n<br>\r\n(Day 3): Sydney – Cruise along Sydney Harbour and Tour the Sydney Opera House\r\n<br>\r\n(Day 4): Sydney – Discover the Majesty In and Around Sydney at your Leisure\r\n<br>\r\n(Day 5): Hamilton Island – Transfer North to the Pristine Shores of Hamilton Island\r\n<br>\r\n(Day 6): Hamilton Island – Witness the Life and Colors of the Great Barrier Reef\r\n<br>\r\n(Day 7): Hamilton Island – Indulge in Luxury and Nature with a Day at your Leisure\r\n<br>\r\n(Day 8): Hamilton Island – Explore your Surroundings at your Preferred Pace\r\n<br>\r\n(Day 9): Melbourne – Fly to Australia’s Bustling Cultural Capital of Melbourne\r\n<br>\r\n(Day 10): Melbourne – Traverse the Historic Lanes and Arcades of Melbourne\r\n<br>\r\n(Day 11): Melbourne – Delight in a Full Day Excursion to Phillip Island\r\n<br>\r\n(Day 12): Melbourne – Depart for Home ', 'thumb_1497174146_australia_great_barrier_reef_clear_fishes.jpg', 'banner_1497174146_australia_great_barrier_reef_panorama_view_h1.jpg', '12', 1, 0, 1, '2017-06-11 15:12:26', '0000-00-00 00:00:00', 0, 'Australia', 'Australia ', 9, 2, 0),
(17, 'Best of India Tour 2017-2018', 'TRIP HIGHLIGHTS \r\nWitness the impeccable architecture of the Taj Mahal, resplendent in immaculate marble and built with remarkable symmetry\r\nExplore the eclectic history of Delhi, within the old and new reaches of the city\r\nExperience the ghost city of Fatehpur Sikri where the sandstone structures date back to the 16th century\r\nVenture through Ranthambore National Park in search of the elusive Royal Bengal Tiger Panthera tigris tigris\r\nDiscover the grandeur of Jaipur, the Pink City, whose historic architecture is as magnificent as the city’s colorful streets', '200.00', '100.00', 'TRIP AT A GLANCE<br> \r\n(Day 1): Your Plane Departs for Delhi, India\r\n<br>\r\n(Day 2): Welcome to Delhi, the Capital City of India\r\n<br>\r\n(Day 3): Tour through the Remarkable Mixture of History and Culture of Delhi\r\n<br>\r\n(Day 4): Make your Way to Agra, via the Train to Experience the Lovely Architecture\r\n<br>\r\n(Day 5): Transfer to Ranthambore National Park with a Stop in Fatepur Sikri\r\n<br>\r\n(Day 6): Explore Ranthambore National Park and the 8th Century Ganesh Temple\r\n<br>\r\n(Day 7): Spend the Morning on a Game Drive before Coming to Alwar\r\n<br>\r\n(Day 8): Find yourself in Jaipur on a Full Day Tour of the Pink City\r\n<br>\r\n(Day 9): Explore Alwar and the Cultural Lives of Nearby Villagers\r\n<br>\r\n(Day 10): Return to Delhi to Make your Return Flight Home', 'thumb_1497174604_india_agra_taj_mahal_unidentified_tourists_visit_indias_most_famous_icon-e.jpg', 'banner_1497174604_india_agra_view_from_agra_fort_on_the_taj_mahal-e.jpg', '10', 1, 0, 1, '2017-06-11 15:20:04', '0000-00-00 00:00:00', 0, 'Asia', 'India', 11, 1, 0),
(18, 'VIP Tour of Sri Lanka: A Signature Experience', 'TRIP HIGHLIGHTS \r\nWander through a collection of ancient cave temples to view stunning frescoes and statues dating back more than 2,000 years\r\nRelish an in-depth tour of a historic tea plantation and factory in the tea-growing region for extensive insight into tradition, cultivation, and flavor\r\nDelight in a private cooking class to learn the art of layered flavors in traditional Sri Lankan cuisine\r\nPartake in the imaginative skill and unique heritage of carving a religious mask during a privately taught class\r\nView one of the oldest known trees in the world while touring the ancient city of Anuradhapura, which legend states has a direct connection to Buddha\r\nVenture to the top of Lion Rock to explore the tremendous grounds of Sigiriya Fortress, home to gardens, reservoirs, caves, and a palace\r\nTraverse the protected grounds of Yala Wildlife Sanctuary in search of Asian elephants, crocodiles, and leopards on thrilling game drives\r\nEnjoy a guided tour of a private spice farm for better understanding of the tropical flavors and lavish arrays of spices thriving in the climate of Sri Lanka\r\nDiscover the unique blend of continued culture and colonial architecture while touring the Fortress of Galle with a local private guide\r\nTravel to the base of Ritigala Mountain Range to visit the grounds of a historical Monastery ', '100.00', '50.00', 'TRIP AT A GLANCE<br> \r\n(Day 1): Colombo – Arrive in Bustling Colombo with an Introductory Tour of the City\r\n<br>\r\n(Day 2): Kotugoda – Enjoy a Scenic Drive to Kotugoda and Ancient Anuradhapura\r\n<br>\r\n(Day 3): Kotugoda – Visit Sigiriya Rock Fortress and the Ruins of Ritigala Monastery\r\n<br>\r\n(Day 4): Kandy – Traverse Dambulla Cave Temples and a Matale Spice Farm\r\n<br>\r\n(Day 5): Kandy – Tour Peradeniya Gardens, Kandy, and Temple of the Tooth\r\n<br>\r\n(Day 6): Hatton – Relish a Scenic Train to Hatton with the Remaining Day at Leisure\r\n<br>\r\n(Day 7): Hatton – Delight in a Half-Day Tea Factory Tour and Time at your Leisure \r\n<br>\r\n(Day 8): Yala – Take a Scenic Ride to Yala National Park for an Evening Game Drive\r\n<br>\r\n(Day 9): Yala – Partake in a Morning and a Night Game Drive in Yala National Park\r\n<br>\r\n(Day 10): Galle – Enjoy a Coastal Drive to Galle to Tour the Historic Dutch Fortress\r\n<br>\r\n(Day 11): Galle – Indulge in a Traditional Cooking Class and Mask Making Excursion\r\n<br>\r\n(Day 12): Colombo – Return to the Capital City for your Evening Flight Home <br>', 'thumb_1497174954_elephant_on_sri_lanka.jpg', 'banner_1497174979_silhouettes_of_the_traditional_stilt_fishermen_at_the_sunset_near_galle_in_sri_lanka.jpg', '12', 1, 0, 1, '2017-06-11 15:25:54', '2017-06-11 15:26:20', 0, 'Asia', 'Sri Lanka', 10, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `r_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `r_name`) VALUES
(1, 'Asia'),
(2, 'Australia'),
(4, 'America '),
(5, 'Europe'),
(6, 'Africa');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `request_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `date_planned` datetime NOT NULL,
  `requested_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requested_airlines`
--

CREATE TABLE `requested_airlines` (
  `req_airline_id` int(11) NOT NULL,
  `airline_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requested_hotels`
--

CREATE TABLE `requested_hotels` (
  `req_hotel_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stars` float NOT NULL,
  `comment` text NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `package_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `stars`, `comment`, `visible`, `package_id`) VALUES
(19, 15, 4, 'this is a test', 1, 4),
(20, 15, 4, 'TRIP HIGHLIGHTS Dive into the warm waters of the South Pacific to snorkel or scuba dive along prime locations of the Great Barrier Reef Delight in a scenic cruise to Whitehaven Island and traverse the nature trails to view the idyllic t', 1, 16),
(21, 15, 4, 'A feeling of majesty overtakes you when you see historic palaces or view unencumbered wildlife in its natural habitat. Experience the na', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `longt` float NOT NULL,
  `description` text NOT NULL,
  `title` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id`, `lat`, `longt`, `description`, `title`) VALUES
(2, 46.4076, 18.0176, 'sdfsdf', 'sdfsad');

-- --------------------------------------------------------

--
-- Table structure for table `travel_media`
--

CREATE TABLE `travel_media` (
  `id` int(11) NOT NULL,
  `travel_id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `image` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel_media`
--

INSERT INTO `travel_media` (`id`, `travel_id`, `name`, `image`) VALUES
(18, 2, '7016879_orig.jpg', 1),
(19, 2, 'mov_bbb1.mp4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `travel_package`
--

CREATE TABLE `travel_package` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `travel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel_package`
--

INSERT INTO `travel_package` (`id`, `package_id`, `travel_id`) VALUES
(9, 16, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_type` tinytext NOT NULL COMMENT '1- super admin, 2 - agent',
  `user_email` varchar(120) NOT NULL,
  `user_password` varchar(150) NOT NULL,
  `user_status` tinyint(1) NOT NULL COMMENT 'enable - 1,  disable - 0',
  `change_password_code` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `user_type`, `user_email`, `user_password`, `user_status`, `change_password_code`, `created_date`, `updated_date`) VALUES
(1, 'John', 'Smith', '1', 'anitakosh2017@gmail.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', 1, '', '2017-04-08 20:02:27', '2017-05-31 23:15:15'),
(8, 'Joe', 'Smith', '2', 'joe@gmail.com', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 1, '', '2017-04-20 18:58:31', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airline`
--
ALTER TABLE `airline`
  ADD PRIMARY KEY (`airline_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `hotels_gallery`
--
ALTER TABLE `hotels_gallery`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `requested_airlines`
--
ALTER TABLE `requested_airlines`
  ADD PRIMARY KEY (`req_airline_id`);

--
-- Indexes for table `requested_hotels`
--
ALTER TABLE `requested_hotels`
  ADD PRIMARY KEY (`req_hotel_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `travel_media`
--
ALTER TABLE `travel_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `travel_package`
--
ALTER TABLE `travel_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airline`
--
ALTER TABLE `airline`
  MODIFY `airline_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `hotel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hotels_gallery`
--
ALTER TABLE `hotels_gallery`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requested_airlines`
--
ALTER TABLE `requested_airlines`
  MODIFY `req_airline_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requested_hotels`
--
ALTER TABLE `requested_hotels`
  MODIFY `req_hotel_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `travel`
--
ALTER TABLE `travel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `travel_media`
--
ALTER TABLE `travel_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `travel_package`
--
ALTER TABLE `travel_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
